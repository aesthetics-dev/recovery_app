import * as express from 'express';
import * as bodyParser from 'body-parser';
import RequestLogger from './src/middleware/request_logger';
import 'express-async-errors';
import { randomFact } from './src/other/facts';
import DI from './src/utils/dependency_injector';
import OrganizationRouter from './src/routers/organization_router';
import AccountRouter from './src/routers/account_router';
import TaskRouter from './src/routers/task_router';
import AuthMiddleware from './src/middleware/auth_middleware';

class App {
  inst: express.Express
  readonly BASE = '/api';
  private readonly logger: RequestLogger = new RequestLogger();

  constructor(
    private readonly organizationRouter: OrganizationRouter,
    private readonly accountRouter: AccountRouter,
    private readonly taskRouter: TaskRouter,
    private readonly authMiddleware: AuthMiddleware,
  ) {
    this.inst = express();
    this.inst.use(bodyParser.json());
    this.inst.use((req, res, next) => this.logger.log(req, res, next));

    this.inst.use(`${this.BASE}/org/:orgId`, (req, res, next) => authMiddleware.authenticateWithOrg(req, res, next));
    this.inst.use(this.BASE, this.organizationRouter.getRouter());
    this.inst.use(this.BASE, this.accountRouter.getRouter());
    this.inst.use(this.BASE, this.taskRouter.getRouter());

    this.inst.use((req, res, next) => new RequestLogger().log(req, res, next));
    this.inst.get('*', (req, res) => {
      res.send(`404 Page Not Found. Here's a random fact: ${randomFact()}`).status(404);
    });
    this.inst.use((err: any, req: any, res: any, next: any) => DI.get().exceptionHandler.catchAll(err, req, res, next));
  }
}

export default App;
