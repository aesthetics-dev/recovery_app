// @ts-ignore
import * as env from 'dotenv';

env.config();

export const DB_USER = process.env.db_user;
export const DB_HOST = process.env.db_host;
export const DB_NAME = process.env.db_name;
export const DB_PASS = process.env.db_pass;
export const DB_PORT = parseInt(typeof process.env.db_port === 'string' ? process.env.db_port : '');

export const PORT = process.env.port;
