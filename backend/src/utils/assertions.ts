import Failure from '../models/utility/failure';
import Failures from '../routers/failures';

export function assertDefined<T>(val: T | undefined, failure: Failure = Failures.badFormat): T {
  if (val === undefined) throw failure;
  return val;
}

export function assertEqual(val1: any, val2: any, failure: Failure): void {
  if (val1 !== val2) throw failure;
}

export function assertTrue(val: boolean, failure: Failure = Failures.badFormat) {
  assertEqual(val, true, failure);
}
