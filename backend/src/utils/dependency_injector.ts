// @ts-ignore
import admin from 'firebase-admin';
import * as http from 'http';
import { Express } from 'express';
import AccountService from '../services/account_service';
import Postgres from '../repos/postgres';
import OrganizationService from '../services/organization_service';
import OrganizationRouter from '../routers/organization_router';
import AccountRouter from '../routers/account_router';
import RelationService from '../services/relation_service';
import AuthMiddleware from '../middleware/auth_middleware';
import ExceptionHandler from '../middleware/exception_handler';
import Firebase from '../repos/firebase';
import TaskRouter from '../routers/task_router';
import TaskService from '../services/task_service';
import App from '../../app';
import AuthService from '../services/auth_service';
import AppNotificationService from '../services/app_notification_service';

class DI {
  private static inst: DI = new DI();

  static get() {
    return DI.inst;
  }

  server: http.Server;
  app: Express;

  postgres: Postgres;
  messaging: admin.messaging.Messaging;

  accountService: AccountService;
  taskService: TaskService;
  organizationService: OrganizationService;
  accLocService: RelationService
  authService: AuthService;
  notificationService: AppNotificationService;

  authMiddleware: AuthMiddleware;
  organizationRouter: OrganizationRouter;
  accountRouter: AccountRouter;
  taskRouter: TaskRouter;

  exceptionHandler: ExceptionHandler

  private constructor() {
    Firebase.initialize();

    this.postgres = new Postgres();
    this.messaging = admin.messaging();

    this.accLocService = new RelationService(this.postgres, 'acc_loc', 'accId', 'locId');

    this.notificationService = new AppNotificationService(this.messaging, this.postgres);
    this.accountService = new AccountService(this.postgres, this.accLocService);
    this.taskService = new TaskService(this.postgres, this.notificationService);
    this.organizationService = new OrganizationService(this.postgres, this.taskService, this.notificationService);
    this.authService = new AuthService(this.organizationService, admin.auth());

    this.authMiddleware = new AuthMiddleware(this.authService);
    this.accountRouter = new AccountRouter(this.accountService);
    this.organizationRouter = new OrganizationRouter(this.organizationService, this.accountService);

    this.exceptionHandler = new ExceptionHandler();
    this.taskRouter = new TaskRouter(this.taskService, this.accountService);

    this.app = new App(this.organizationRouter, this.accountRouter, this.taskRouter, this.authMiddleware).inst;
    this.server = http.createServer(this.app);
  }
}

export default DI;
