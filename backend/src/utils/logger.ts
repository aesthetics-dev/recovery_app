// @ts-ignore
import * as moment from 'moment';
// @ts-ignore
import * as chalk from 'chalk';

class Logger {
  readonly id: string;
  constructor(id: string = '') {
    this.id = id;
  }

  signature(type: string) {
    return `-- ${type} | ${this.id} | ${moment().format('YYYY-MM-DD HH:mm:ss')}\n`;
  }

  info(...args: any[]) {
    // eslint-disable-next-line no-console
    console.log(chalk.green(this.signature('💡 INFO')), ...args);
  }

  error(...args: any[]) {
    // eslint-disable-next-line no-console
    console.log(chalk.red(this.signature('❗ ERROR')), ...args);
  }

  debug(...args: any[]) {
    // eslint-disable-next-line no-console
    console.log(chalk.blue(this.signature('🐛 DEBUG')), ...args);
  }

  warning(...args: any[]) {
    // eslint-disable-next-line no-console
    console.log(chalk.yellow(this.signature('⚠️ WARNING')), ...args);
  }

  wtf(...args: any[]) {
    // eslint-disable-next-line no-console
    console.log(chalk.cyan(this.signature('👽 WTF')), ...args);
  }
}

export default Logger;
