export default abstract class NumberUtils {
  static clamp(val: number, min: number, max: number) {
    return Math.max(Math.min(val, max), min);
  }

  static roundTo(val: number, dPlaces: number) {
    const power = 10 ** dPlaces;
    return Math.round(val * power) / power;
  }
}
