import ServerFailure from '../models/utility/server_failure';
import Failures from '../routers/failures';

export function traceFailure(failure: ServerFailure = Failures.unknown, replace: boolean = false) {
  // eslint-disable-next-line func-names
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const original = descriptor.value;
    // eslint-disable-next-line no-param-reassign,func-names
    descriptor.value = async function (...args: any[]) {
      try {
        return await original.call(this, ...args);
      } catch (e) {
        const error = ServerFailure.fromError(e).addTrace(original.name);

        if (replace) {
          error.code = failure.code;
          error.message = failure.message;
        }
        throw error;
      }
    };
  };
}
