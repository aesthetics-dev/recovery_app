// @ts-ignore

import * as moment from 'moment';

export function queryForRef(tableName: string, id: string) {
  // noinspection SqlResolve
  return `(SELECT json_build_object('id', ${tableName}.id, 'name', ${tableName}.name)
    FROM ${tableName}
    WHERE id = ${id})`;
}

export function queryForAccountRef(id: string) {
  return `(
    SELECT json_build_object('name', name, 'id', id, 'secondaryId', email)
    FROM acc
    WHERE id = ${id}
    )
  `;
}

export function getTimestamp(): string {
  return moment().format();
}
