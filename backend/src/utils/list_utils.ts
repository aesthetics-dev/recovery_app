import { SafeCaster } from '../models/utility/utility_types';

class ListUtils {
  static leftOuterJoin<T>(a1: T[], a2: T[], compOn: SafeCaster<T, any>) {
    return a1.filter((o1) => !a2.some((o2) => compOn(o1) === compOn(o2)));
  }

  static innerJoin<T>(a1: T[], a2: T[], compOn: SafeCaster<T, any>) {
    return a1.filter((o1) => a2.some((o2) => compOn(o1) === compOn(o2)));
  }
}

export default ListUtils;
