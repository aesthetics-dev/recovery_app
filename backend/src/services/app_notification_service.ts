// @ts-ignore
import * as lodash from 'lodash';
// @ts-ignore
import admin from 'firebase-admin';
import Task from '../models/data/task';
import Postgres from '../repos/postgres';
import { extractFcmTokens } from '../models/utility/model_utils';
import { Roles, TaskUpdateType, UpdateType } from '../models/utility/constants';
import { traceFailure } from '../utils/wrappers';
import AppNotificationQueries from '../queries/app_notification_queries';
import MessagingOptions = admin.messaging.MessagingOptions;

function extractTaskBody(taskUpdateType: TaskUpdateType, task: Task) {
  if (taskUpdateType === TaskUpdateType.inserted) {
    return `Task created: ${[task.customTag, ...task.tags].filter((t) => !!t).join(', ')}`;
  }
  if (taskUpdateType === TaskUpdateType.completed) {
    return 'Task completed!';
  }
  return `Task acknowledged: ${[task.customMDTag, ...task.mdTags].filter((t) => !!t).join(', ')}`;
}

function constructMessagingOptions(): MessagingOptions {
  return { priority: 'high' };
}

function extractTaskTitle(task: Task): string {
  return `${task.locRef?.name} ${task.bedRef?.name}`;
}

class AppNotificationService {
  constructor(
    private messages: admin.messaging.Messaging,
    private postgres: Postgres,
  ) {
  }

  @traceFailure()
  private async sendBannerNotification(
    devices: string | string[], title: string, body: string,
  ): Promise<void> {
    if (!devices?.length) {
      return;
    }

    await this.messages.sendToDevice(
      devices,
      { notification: { title, body, sound: 'default' } },
      constructMessagingOptions(),
    );
  }

  @traceFailure()
  private async sendUpdateNotification(
    devices: string | string[], updateValue: UpdateType,
  ): Promise<void> {
    if (!devices?.length) {
      return;
    }

    await this.messages.sendToDevice(
      devices,
      { data: { shouldRefresh: updateValue } },
      constructMessagingOptions(),
    );
  }

  async taskChange(taskUpdateType: TaskUpdateType, task: Task): Promise<void> {
    const [updateTokens, notificationTokens] = await Promise.all([
      this.postgres.queryArray(
        AppNotificationQueries.locationFollowers,
        { locId: task.locRef?.id },
        extractFcmTokens,
      ).then(lodash.flatten),

      this.getNotificationTokens(taskUpdateType, task),
    ]);

    await Promise.all([
      this.sendBannerNotification(
        notificationTokens,
        extractTaskTitle(task),
        extractTaskBody(taskUpdateType, task),
      ),
      this.sendUpdateNotification(updateTokens, UpdateType.tasks),
    ]);
  }

  async getNotificationTokens(taskUpdateType: TaskUpdateType, task: Task): Promise<string[]> {
    if (taskUpdateType === TaskUpdateType.inserted) {
      return this.postgres.queryArray(
        AppNotificationQueries.notificationLocationFollowersByRole,
        { locId: task.locRef?.id, roleType: Roles.physician },
        extractFcmTokens,
      ).then(lodash.flatten);
    }
    if ([TaskUpdateType.acknowledged, TaskUpdateType.completed].includes(taskUpdateType)) {
      return this.postgres.queryArray(
        AppNotificationQueries.notificationLocationFollowersByAccId,
        { accId: task.created?.ref?.id },
        extractFcmTokens,
      ).then(lodash.flatten);
    }
    return [];
  }

  async organizationUpdate(orgId: string): Promise<void> {
    const tokens = await this.postgres.queryArray(
      AppNotificationQueries.organizationTokens,
      { orgId }, extractFcmTokens,
    ).then(lodash.flatten);

    await this.sendUpdateNotification(tokens, UpdateType.organization);
  }
}

export default AppNotificationService;
