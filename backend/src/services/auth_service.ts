// @ts-ignore
import Admin from 'firebase-admin';
import OrganizationService from './organization_service';
import Failures from '../routers/failures';
import AuthenticatedUser from '../models/data/authenticated_user';
import { traceFailure } from '../utils/wrappers';

class AuthService {
  private readonly default = 'default'

  constructor(
    private organizationService: OrganizationService,
    private auth: Admin.auth.Auth,
  ) {
  }

  @traceFailure(Failures.unauthorized)
  async authenticateToken(token?: string, orgId: string = this.default) {
    if (!orgId) {
      throw Failures.unauthorized.addInfoTrace('orgId is undefined');
    }

    const tokenId = AuthService.extractTokenId(token);

    const authUser = await this.auth.verifyIdToken(tokenId).catch(() => {
      throw Failures.unauthorized.addInfoTrace('Id verification failed');
    });

    if (!authUser?.email) {
      throw Failures.unauthorized.addInfoTrace('No email found after authentication');
    }

    const user = new AuthenticatedUser({ email: authUser.email });

    if (orgId === this.default) {
      return user;
    }

    const valid = await this.organizationService.isEmailKnown(orgId, authUser.email);
    if (!valid) {
      throw Failures.unauthorized.addInfoTrace('Invalid email domain');
    }

    return user;
  }

  static extractTokenId(token?: string) {
    if (!token?.startsWith('Bearer ')) {
      throw Failures.unauthorized.addInfoTrace('Bad token format');
    }

    const tokenId = token.split('Bearer ')[1];
    if (!tokenId) {
      throw Failures.unauthorized.addInfoTrace('No token Bearer');
    }

    return tokenId;
  }
}

export default AuthService;
