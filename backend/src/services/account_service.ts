// @ts-ignore
import { isDefined } from 'ts-is-present';
import Postgres from '../repos/postgres';
import Account from '../models/data/account';
import { accLocRelations } from '../models/data/relation_types';
import RelationService from './relation_service';
import { asAccount, asString, asStringArray } from '../models/utility/model_utils';
import Failures from '../routers/failures';
import ServerFailure from '../models/utility/server_failure';
import Logger from '../utils/logger';
import { traceFailure } from '../utils/wrappers';
import AccountQueries from '../queries/account_queries';
import Heartbeat from '../models/data/heartbeat';
import LogoutTrace from '../models/data/logout_trace';

class AccountService {
  logger: Logger = new Logger('Account Service')

  constructor(
    private readonly postgres: Postgres,
    private readonly accLocService: RelationService,
  ) {

  }

  @traceFailure(Failures.accGet)
  async getByOrgAndEmail(
    orgId: string, email: string,
    createIfDoesntExist: boolean = false,
  ): Promise<Account | undefined> {
    const accResponse = await this.postgres.query(AccountQueries.getAccount, { orgId, email });

    const acc = asAccount(accResponse.rows[0]);

    if (!acc && createIfDoesntExist) {
      await this.createAccount(orgId, email);
      return this.getByOrgAndEmail(orgId, email, false);
    }

    return acc;
  }

  @traceFailure(Failures.accInsert)
  async createAccount(orgId: string, email: string): Promise<true> {
    return this.postgres
      .g('acc')
      .insert({ email, orgId })
      .then(() => true);
  }

  @traceFailure()
  async saveHeartbeat(accId: string, heartbeat: Heartbeat) {
    await this.postgres.query(AccountQueries.heartbeat, {
      accId,
      clientOs: heartbeat.clientOs,
      clientVersion: heartbeat.clientVersion,
      clientBuildNumber: heartbeat.clientBuildNumber,
    });
  }

  @traceFailure()
  async saveLogoutTrace(accId: string, logoutTrace: LogoutTrace) {
    await this.postgres.query(AccountQueries.logoutTrace, {
      accId,
      logoutMethod: logoutTrace.logoutMethod,
    });
  }

  @traceFailure()
  async registerLogout(accId: string) {
    await this.postgres.query(AccountQueries.logout, { accId });
  }

  @traceFailure(Failures.accInsert)
  async insertFcmToken(accId: string, tokenId: string): Promise<void> {
    const tokens = await this.postgres
      .querySingle(
        AccountQueries.getFcmTokens,
        { accId },
        (v) => asStringArray(v?.fcmTokens),
      ) || [];

    const outTokens = [tokenId, ...tokens.filter((t) => t !== tokenId)].slice(0, 3);
    await this.postgres.query(AccountQueries.setFcmTokens, { tokens: outTokens, accId });
  }

  @traceFailure(Failures.accInsert)
  async deleteFcmToken(accId: string, tokenId: string): Promise<void> {
    await this.postgres.querySingle(AccountQueries.deleteFcmToken, { accId, tokenId });
  }

  @traceFailure()
  async getIdByOrgIdAndEmail(orgId: string, email: string): Promise<string | undefined> {
    return this.postgres.querySingle(
      AccountQueries.getAccIdByOrgAndEmail,
      { orgId, email },
      (v) => asString(v?.id),
    );
  }

  @traceFailure()
  async update(acc: Account): Promise<boolean> {
    if (!acc.ref?.id || !acc.orgRef?.id || !acc.locRefs) {
      throw Failures.badFormat;
    }

    try {
      await this.postgres
        .g('acc')
        .update({ name: acc.ref.name, role: acc.role?.ref?.id, notificationsOn: acc.notificationsOn })
        .where({ id: acc.ref.id, orgId: acc.orgRef.id })
        .then(() => true);
      const locIds = acc.locRefs.map((l) => l.id).filter(isDefined);
      return this.accLocService.updateTo(acc.ref.id, <any>locIds, accLocRelations.follower);
    } catch (error) {
      this.logger.error(error);
      throw ServerFailure.fromError(Failures.accUpdate);
    }
  }

  async isAdmin(orgId: string, email: string): Promise<boolean> {
    return this.postgres.queryIsNotEmpty(AccountQueries.isAdminEmail, { orgId, email });
  }
}

export default AccountService;
