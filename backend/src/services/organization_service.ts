// @ts-ignore
import { isDefined } from 'ts-is-present';
import Postgres from '../repos/postgres';
import Organization from '../models/data/organization';
import {
  asAbridgedOrganization,
  asObjectArray,
  asOrganization,
  asReference, asStringArray,
  constructLast,
} from '../models/utility/model_utils';
import Failures from '../routers/failures';
import Reference from '../models/data/reference';
import { traceFailure } from '../utils/wrappers';
import TaskService from './task_service';
import Bed from '../models/data/bed';
import ListUtils from '../utils/list_utils';
import Loc from '../models/data/loc';
import AppNotificationService from './app_notification_service';
import OrganizationQueries from '../queries/organization_queries';
import Role from '../models/data/role';
import AbridgedOrganization from '../models/data/abridged_organization';

class OrganizationService {
  constructor(
    readonly postgres: Postgres,
    readonly taskService: TaskService,
    readonly notificationService: AppNotificationService,
  ) {
  }

  @traceFailure(Failures.orgGet)
  async getFullById(orgId: string, { adminQuery = false } = {}): Promise<Organization | undefined> {
    return this.postgres.querySingle(OrganizationQueries.getFullOrganization, { orgId, adminQuery }, asOrganization);
  }

  @traceFailure(Failures.orgGet)
  async getById(orgId: string): Promise<Organization | undefined> {
    return this.postgres.g('org')
      .where({ id: orgId })
      .select()
      .then((d: any[]) => constructLast(d, asOrganization));
  }

  @traceFailure(Failures.orgsGet)
  async getAll(): Promise<AbridgedOrganization[]> {
    const orgResponse = await this.postgres.g.raw(`
      SELECT "authType",
             json_build_object('name', name, 'id', id) AS ref
      FROM org
    `);

    const orgs = asObjectArray(orgResponse.rows, asAbridgedOrganization);
    if (!orgs) {
      throw Failures.orgsGet.addInfoTrace("Couldn't cast organizations to array");
    }

    return orgs;
  }

  @traceFailure(Failures.orgInsert)
  async populateLocationIds(orgId: string, locations: Loc[]): Promise<void> {
    const dbLocs = await this.postgres.query(OrganizationQueries.getActiveLocationNameId, { orgId });

    locations.forEach((l) => {
      const match = dbLocs.rows.find((dl) => dl.name === l.ref?.name);
      if (!l.ref) {
        return;
      }
      // eslint-disable-next-line no-param-reassign
      l.ref.id = match.id;
    });
  }

  @traceFailure(Failures.orgInsert)
  async updateOrg(orgId: string, accId: string, org: Organization): Promise<void> {
    // update organization
    if (org.ref?.name) {
      await this.postgres
        .g('org')
        .where({ id: orgId })
        .update({ name: org.ref?.name, emails: org.emails });
    }

    const currOrg = await this.getFullById(orgId);
    if (!currOrg) throw Failures.orgInsert.addInfoTrace('Couldn\'t find current organization');

    await Promise.all([
      this.updateLocations(orgId, accId, currOrg.locations, org.locations),
      this.updateRoles(orgId, currOrg.roles, org.roles),
    ]);
    await this.updateBeds(orgId, accId, currOrg.locations, org.locations);
    await this.notificationService.organizationUpdate(orgId);
  }

  @traceFailure()
  async updateBeds(orgId: string, accId: string, currLocs: Loc[] = [], updatedLocs: Loc[] = []) {
    const promises: Promise<void>[] = [];
    updatedLocs.forEach((l1) => {
      const l2 = currLocs.find((l) => l.ref?.name === l1.ref?.name);
      const newBeds: Bed[] = !l2?.beds
        ? l1.beds || []
        : ListUtils.leftOuterJoin(l1.beds || [], l2.beds, (b) => b.ref?.name);

      const bedsToRemove: Bed[] = !l2?.beds
        ? []
        : ListUtils.leftOuterJoin(l2.beds, l1.beds || [], (b) => b.ref?.name);

      promises.push(...bedsToRemove.map((b) => this.removeBed(orgId, accId, b.ref?.id)));
      promises.push(...newBeds.map((b) => this.insertBed(orgId, l1.ref?.id, b.ref?.name)));
    });

    await Promise.all(promises);
  }

  @traceFailure()
  async updateRoles(orgId: string, currentRoles: Role[] = [], updatedRoles: Role[] = []) {
    const rolesToRemove = ListUtils.leftOuterJoin(
      currentRoles,
      updatedRoles,
      (l) => l.ref?.name,
    );

    const promises: Promise<any>[] = [];

    if (rolesToRemove.length) {
      promises.push(this.postgres
        .g('role')
        .delete()
        .whereIn('id', rolesToRemove.map((role) => role.ref?.id).filter(isDefined))
        .where({ orgId, editable: true }));
    }

    if (currentRoles.length) {
      promises.push(
        ...updatedRoles.map((r) => this.postgres.query(OrganizationQueries.upsertRole, {
          name: r.ref?.name,
          orgId,
          editable: r.editable,
          type: r.type,
        })),
      );
    }

    await Promise.all(promises);
  }

  @traceFailure()
  async updateLocations(
    orgId: string, accId: string,
    currentLocations: Loc[] = [], updatedLocations: Loc[] = [],
  ) {
    const newLocations = ListUtils.leftOuterJoin(
      updatedLocations,
      currentLocations,
      (l) => l.ref?.name,
    );

    const locationsToRemove = ListUtils.leftOuterJoin(
      currentLocations,
      updatedLocations,
      (l) => l.ref?.name,
    );

    //* insert & delete locations
    await Promise.all(
      [
        ...newLocations.map((l) => this.insertLoc(orgId, l.ref?.name)),
        ...locationsToRemove.map((l) => this.removeLocation(orgId, accId, l.ref?.id)),
      ],
    );

    //* update locations
    await this.populateLocationIds(orgId, updatedLocations);
    await Promise.all(updatedLocations.map((l) => this.updateLocation(orgId, l)));
  }

  @traceFailure(Failures.orgDomainValidate)
  async isEmailKnown(orgId: string, email: string): Promise<boolean> {
    const known = await this.postgres.querySingle(
      OrganizationQueries.emailKnown,
      { orgId, email },
      (v) => Boolean(v.known),
    );
    return Boolean(known);
  }

  @traceFailure(Failures.bedInsert)
  async insertBed(orgId: string, locId: string | undefined, bedName: string | undefined): Promise<void> {
    if (!orgId || !locId || !bedName) {
      return;
    }
    await this.postgres.query(OrganizationQueries.insertBed, { locId, name: bedName, custom: false });
  }

  @traceFailure(Failures.locInsert)
  async insertLoc(orgId: string, locName: string | undefined): Promise<void> {
    if (!locName) {
      return;
    }
    await this.postgres.g('loc').insert({ orgId, name: locName });
  }

  @traceFailure(Failures.orgInsert)
  async insertOrg(orgId: string, name: string): Promise<void> {
    await this.postgres.g('org').insert({ name });
  }

  @traceFailure(Failures.bedRemove)
  async removeBed(orgId: string, accId: string | undefined, bedId: string | undefined): Promise<void> {
    if (!orgId || !accId || !bedId) {
      return;
    }
    await this.postgres.query(`
      UPDATE bed
      SET active = FALSE
      WHERE id = :bedId
    `, { bedId });

    await this.taskService.archiveTasks(accId);
  }

  @traceFailure(Failures.bedRemove)
  async updateLocation(orgId: string, loc: Loc): Promise<void> {
    if (!orgId || !loc || !loc.ref?.name || !loc.tags || !loc.mdTags) {
      return;
    }

    await this.postgres.query(`
      UPDATE loc
      SET tags     = :tags,
          "mdTags" = :mdTags
      WHERE "orgId" = :orgId
        AND "name" = :name
    `, {
      tags: loc.tags, mdTags: loc.mdTags, orgId, name: loc.ref.name,
    });
  }

  @traceFailure(Failures.locRemove)
  async removeLocation(orgId: string, accId: string, locId: string | undefined): Promise<void> {
    if (!locId) {
      return;
    }

    const p1 = this.postgres.query(`
      UPDATE bed
      SET active = FALSE
      WHERE "locId" = :locId;`, { locId });

    const p2 = this.postgres.query(`
      UPDATE loc
      SET active = FALSE
      WHERE id = :locId
    `, { locId });

    await Promise.all([p1, p2]);

    await this.taskService.archiveTasks(accId);
  }

  @traceFailure(Failures.orgRemove)
  async removeOrganization(accId: string, orgId: string): Promise<void> {
    const p1 = this.postgres.query(`
      UPDATE bed
      SET bed.active = FALSE
      WHERE EXISTS(
              SELECT
              FROM loc
              WHERE loc."orgId" = :orgId
                AND loc."id" = bed."locId");`, { orgId });

    const p2 = this.postgres.query(
      `UPDATE loc
         SET loc.active = FALSE
         WHERE loc."orgId" = :orgId;`, { orgId },
    );

    const p3 = this.postgres.query(`
      UPDATE org
      SET org.active = FALSE
      WHERE id = :orgId;
    `, { orgId });

    await Promise.all([p1, p2, p3]);

    await this.taskService.archiveTasks(accId);
  }
}

export default OrganizationService;
