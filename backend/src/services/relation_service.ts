import Postgres from '../repos/postgres';
import Logger from '../utils/logger';

class RelationService {
    logger: Logger

    constructor(
        private postgres: Postgres,
        private table: string,
        private fromCol: string,
        private toCol: string,
    ) {
      this.logger = new Logger(`${table} Service`);
    }

    // - inserts relations that do not exist
    // - removes relations that should not exist
    // - ignores everything else
    async updateTo(fromVal: string, toValues: string[], type: string): Promise<boolean> {
      await this.postgres.g(this.table)
        .delete()
        .where({ [this.fromCol]: fromVal, type })
        .whereNotIn(this.toCol, toValues);

      const objects = toValues.map((val) => ({ [this.fromCol]: fromVal, [this.toCol]: val, type }));
      const query = this.postgres.g(this.table)
        .insert(objects)
        .toString();

      if (query) await this.postgres.query(`${query} ON CONFLICT DO NOTHING`);
      return true;
    }
}

export default RelationService;
