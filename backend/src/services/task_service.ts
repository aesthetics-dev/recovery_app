// @ts-ignore
import * as _ from 'lodash';
import Task from '../models/data/task';
import Postgres from '../repos/postgres';
import { getTimestamp } from '../utils/query_utils';
import { traceFailure } from '../utils/wrappers';
import Failures from '../routers/failures';
import AppNotificationService from './app_notification_service';
import { TaskUpdateType } from '../models/utility/constants';
import TaskQueries from '../queries/task_queries';
import {
  asLocationTaskAverage, asString, castSafe,
} from '../models/utility/model_utils';
import LocationTaskAverage from '../models/data/location_task_average';
import OrganizationQueries from '../queries/organization_queries';

class TaskService {
  constructor(
    private postgres: Postgres,
    private notificationService: AppNotificationService,
  ) {

  }

  @traceFailure(Failures.taskSubmit)
  async submitTask(accId: string, task: Task) {
    if (!task.orgRef?.id || !task.bedRef?.id || !task.locRef?.id) {
      throw Failures.taskPut.addInfoTrace('Information missing');
    }

    const oldTsk = await this.getActiveTaskForBed(task.bedRef.id);
    const tagsDifferent = oldTsk?.customTag !== task.customTag || !_.isEqual(oldTsk.tags, task.tags);
    const mdTagsDifferent = oldTsk?.customMDTag !== task.customMDTag || !_.isEqual(oldTsk.mdTags, task.mdTags);

    // empty task is same as removed task
    if (task.isEmpty()) {
      if (!task.bedRef.id) throw Failures.taskSubmit.addInfoTrace('tried to remove without bed id');
      await this.removeTask(accId, task.bedRef.id);
      await this.notificationService.taskChange(TaskUpdateType.completed, task);
    } else if (!tagsDifferent && mdTagsDifferent) { // if md tags are different -- just update them
      if (!task.bedRef.id) throw Failures.taskPut.addInfoTrace('tried to update with null id');
      await this.updateMdTags(task.bedRef.id, task);
      await this.notificationService.taskChange(TaskUpdateType.acknowledged, task);
    } else {
      if (task.bedRef.id) await this.removeTask(accId, task.bedRef.id);
      await this.insertTask(accId, task);
      await this.notificationService.taskChange(TaskUpdateType.inserted, task);
    }
  }

  @traceFailure()
  async getActiveTaskForBed(bedId: string): Promise<Task | undefined> {
    return this.postgres.querySingle(
      TaskQueries.getActiveTaskByBedId,
      { bedId },
      castSafe(Task),
    );
  }

  @traceFailure(Failures.taskRemove)
  async removeTask(accId: string, bedId: string) {
    await this.postgres.g.raw(TaskQueries.removeTask, { bedId, removedBy: accId });
  }

  @traceFailure(Failures.taskPut)
  async updateMdTags(bedId: string, task: Task) {
    await this.postgres.g.raw(TaskQueries.updateTagsForBed, {
      bedId,
      mdTags: task.mdTags,
      customMDTag: task.customMDTag,
    });
  }

  @traceFailure(Failures.taskPut)
  async insertTask(accId: string, task: Task) {
    if (!(task.orgRef?.id && task.locRef?.id && task.bedRef)) {
      throw Failures.taskPut.addInfoTrace('missing org, loc, or bed ref ids while trying to insert');
    }

    if (task.isEmpty()) {
      return;
    }

    const validLoc = await this.validLocAndOrg(
      task.orgRef.id,
      task.locRef.id,
    );

    if (!validLoc || !task.bedRef?.name) {
      throw Failures.taskSubmit;
    }

    let bedId = task.bedRef.id;
    if (!bedId) {
      bedId = await this.postgres.querySingle(OrganizationQueries.insertBed, {
        name: task.bedRef.name,
        locId: task.locRef.id,
        custom: true,
      }, (v) => asString(v?.id));
    }

    await this.postgres.query(`
      INSERT INTO task
      ("orgId", "locId", "bedId", tags, "mdTags", "customTag", "customMDTag", "createdBy",
       "createdOn")
      VALUES (:orgId, :locId, :bedId, :tags, :mdTags, :customTag, :customMDTag, :createdBy, now())
    `, {
      orgId: task.orgRef.id,
      locId: task.locRef.id,
      bedId,
      tags: task.tags,
      mdTags: task.mdTags,
      customTag: task.customTag,
      customMDTag: task.customMDTag,
      createdBy: accId,
    });
  }

  async archiveTasks(accId: string) {
    return this.postgres.query(TaskQueries.archiveTasks, { accId, ts: getTimestamp() });
  }

  async validLocAndOrg(orgId: string, locId: string): Promise<boolean> {
    return this.postgres.query(OrganizationQueries.existsActiveLocAndOrg,
      { orgId, locId }).then((r) => r.rows.length > 0);
  }

  @traceFailure(Failures.taskGet)
  async getForAccount(accId: string): Promise<Task[]> {
    const tasks = await this.postgres.g.raw(TaskQueries.getTasks(false), { accId });

    return tasks.rows;
  }

  @traceFailure(Failures.taskGet)
  async getHistoricForAccount(accId: string, limit: number = 15): Promise<Task[]> {
    const tasks = await this.postgres.g.raw(TaskQueries.getTasks(true, limit), { accId });

    return tasks.rows;
  }

  async getLocationTaskStats(accId: string): Promise<LocationTaskAverage[]> {
    const ans = await this.postgres.queryArray(
      TaskQueries.taskStatistics,
      { accId },
      asLocationTaskAverage,
    ) || [];
    return <any>ans;
  }
}

export default TaskService;
