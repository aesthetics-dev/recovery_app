export default abstract class AppNotificationQueries {
  // @args locId
  static locationFollowers = `
    SELECT "fcmTokens"
    FROM acc
    WHERE EXISTS(
            SELECT
            FROM acc_loc
            WHERE "accId" = acc.id
              AND "locId" = :locId
            )
  `
  // @args orgId
  static organizationTokens = `
    SELECT "fcmTokens"
    FROM acc
    WHERE acc."orgId" = :orgId
  `

  // @args locId, roleType
  static notificationLocationFollowersByRole = `
    SELECT "fcmTokens"
    FROM acc
    WHERE acc."notificationsOn" = TRUE
      AND exists(
      SELECT
      FROM role
      WHERE role.type = :roleType
        AND role.id = acc.role
      )
      AND EXISTS(
      SELECT
      FROM acc_loc
      WHERE "accId" = acc.id
        AND "locId" = :locId
      )`;

  // @args  accId
  static notificationLocationFollowersByAccId = `
    SELECT "fcmTokens"
    FROM acc
    WHERE acc."notificationsOn" = TRUE
      AND acc.id = :accId
  `
}
