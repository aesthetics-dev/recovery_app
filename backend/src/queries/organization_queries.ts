import Subqueries from './subqueries';

export default abstract class OrganizationQueries {
  // @arg orgId
  // @arg :adminQuery
  static getFullOrganization = `
    SELECT "authType",
    
      -- ref
      (json_build_object('id', org.id, 'name', org.name)) AS ref,
      
      ( -- role subquery
        SELECT json_agg(${Subqueries.roleBuild})
        FROM role
        WHERE role."orgId" = org.id 
      ) AS roles,
      
      ( -- location subquery
        SELECT json_agg(
          json_build_object(
            'ref', json_build_object('id', loc.id, 'name', loc.name),
            'tags', loc.tags,
            'mdTags', loc."mdTags",
            -- bed subquery
            'beds', 
            (-- construct bed ref
              SELECT json_agg(
                json_build_object(
                  'ref', json_build_object('name', bed.name, 'id', bed.id)
                )
              ) AS ref
              FROM bed
              WHERE bed.active = TRUE
              AND bed.custom = FALSE
              AND bed."locId" = loc.id
            )
          )
        ) 
        FROM loc
        WHERE loc."orgId" = org.id
        AND loc.active = TRUE
      ) AS locations,
      (CASE WHEN :adminQuery::bool THEN emails::text[] END) as emails
      
    FROM org
    WHERE org.id = :orgId
  `;

  // @arg :orgId
  // @arg :email
  static emailKnown = `
    SELECT (
      EXISTS(
        SELECT 
        FROM org
        WHERE id = :orgId
          AND :email = ANY(emails)   
      )
    ) as known
  `

  static getActiveLocationNameId = `
    SELECT name, id
    FROM loc
    WHERE "orgId" = :orgId
      AND active = TRUE;
  `;

  static existsActiveLocAndOrg = `
    SELECT 1
    WHERE EXISTS(SELECT FROM loc WHERE loc.id = :locId AND loc.active = TRUE)
      AND EXISTS(SELECT FROM org WHERE org.id = :orgId AND org.active = TRUE)`;

  // @args name, locId, custom
  static insertBed = `
    INSERT INTO bed (name, "locId", custom)
    VALUES (:name, :locId, :custom)
    RETURNING id
  `;

  // @args orgId, name, editable, type
  static upsertRole = `
    INSERT INTO role ("orgId", name, editable, type)
    VALUES (:orgId, :name, :editable, :type)
    ON CONFLICT (name, "orgId") DO UPDATE SET editable = :editable,
                                              type     = :type
  `
}
