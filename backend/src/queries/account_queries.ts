import { queryForRef } from '../utils/query_utils';
import Subqueries from './subqueries';

export default abstract class AccountQueries {
  static getAccount = `
SELECT
 "notificationsOn",
  admin,

  -- ref
   jsonb_build_object('id', id, 'secondaryId', email, 'name', name) AS ref,

  -- followed locations
  (
    SELECT json_agg(${Subqueries.refBuild})
    FROM loc
    WHERE EXISTS( SELECT FROM acc_loc WHERE "accId" = acc.id AND "locId" = loc.id )
  ) AS "locRefs",

   -- organization
  ${queryForRef('org', '"orgId"')} AS "orgRef",

  -- role
  (SELECT ${Subqueries.roleBuild} FROM role WHERE role.id = acc.role) AS role

  FROM acc
  WHERE acc.email = :email
  AND acc."orgId" = :orgId
    `;

  // @args: :accId, :tokenId
  static deleteFcmToken = `
    UPDATE acc
    SET "fcmTokens" = array_remove("fcmTokens", :tokenId)
    WHERE id = :accId
  `;

  // @args: :accId, :tokens
  static setFcmTokens = `
    UPDATE acc
      SET "fcmTokens" = :tokens
    WHERE id = :accId
  `;

  // @args: :accId,
  static getFcmTokens = `
    SELECT "fcmTokens"
    FROM acc
    WHERE id = :accId
  `;

  // @args: email, orgId
  static getAccIdByOrgAndEmail = `
    SELECT id
    FROM acc
    WHERE "orgId" = :orgId
      AND email = :email
  `

  // @args: :accId, :clientVersion, clientBuildNumber, :clientOs
  static heartbeat = `
    UPDATE acc SET
      "lastActiveTs" = now(),
      "clientVersion" = :clientVersion,
      "clientOs" = :clientOs,
      "clientBuildNumber" = :clientBuildNumber
    WHERE id = :accId
  `

  // @args: :accId, :logoutMethod
  static logoutTrace = `
    UPDATE acc SET
      "logoutMethod" = :logoutMethod,
      "logoutTs" = now()
    WHERE id = :accId
  `

  // @args: accId, logoutMethod
  static logout = `
    UPDATE acc SET
      "logoutMethod" = :logoutMethod,
      "logoutTs" = now()
    WHERE id = :accId 
  `

  // arg :email
  // arg :orgId
  static isAdminEmail = 'SELECT is_admin_email(:email, :orgId)'

  // @arg :orgId
  // @arg :email
  static isAdminEmailDeclaration = `
      CREATE OR REPLACE FUNCTION is_admin_email(p_acc_email varchar(255), p_org_id uuid)
    RETURNS bool AS
    $$
    BEGIN
      RETURN EXISTS(
        SELECT 
        FROM acc 
        WHERE email = p_acc_email 
          AND "orgId" = p_org_id
      );
    END
    $$ LANGUAGE plpgsql STABLE;
  `
}
