import { queryForAccountRef, queryForRef } from '../utils/query_utils';

export default abstract class TaskQueries {
  // @args accId
  static taskStatistics = `
    SELECT ${queryForRef('loc', '"locId"')} AS "locRef",
           extract(EPOCH FROM avg(age("removedOn", "createdOn"))) AS "secondsToComplete"
    FROM task
    WHERE "removedOn" > current_timestamp - INTERVAL '7 days'
      AND EXISTS(
        SELECT
        FROM acc_loc
        WHERE "accId" = :accId
          AND acc_loc."locId" = task."locId"
      )
      AND EXISTS(
        SELECT
        FROM loc
        WHERE loc.active = TRUE AND loc.id = task."locId"
      )
    GROUP BY task."locId"
  `

  static getTasks(historic: boolean, limit?: number): string {
    return `
      SELECT tags,
             "mdTags",
             "customTag",
             "customMDTag",
             ${queryForRef('org', 'task."orgId"')} AS "orgRef",
             ${queryForRef('loc', 'task."locId"')} AS "locRef",
             ${queryForRef('bed', 'task."bedId"')} AS "bedRef",
             (SELECT json_build_object(
                 'ref', ${queryForAccountRef('task."createdBy"')},
                 'timestamp', task."createdOn")) AS "created",
             (SELECT json_build_object(
                 'ref', ${queryForAccountRef('task."removedBy"')},
                 'timestamp', task."removedOn")) AS "removed",
             (SELECT json_build_object(
                 'ref', ${queryForAccountRef('task."mdETA_id"')},
                 'timestamp', task."mdETA_ts")) AS "mdETA"
      FROM task
      WHERE "removedOn" IS ${historic ? 'NOT NULL' : 'NULL'}
        AND "locId" IN (
          SELECT "locId" FROM acc_loc
          WHERE "accId" = :accId
    ) AND EXISTS (SELECT FROM loc WHERE loc.id = "locId" AND loc.active = TRUE)
    ORDER BY ${historic ? '"removedOn"' : '"createdOn"'} DESC
    ${limit ? `LIMIT ${limit}` : ''}`;
  }

  static archiveTasks = `
    UPDATE task
    SET "removedBy" = :accId,
        "removedOn" = :ts
    WHERE EXISTS(SELECT FROM bed WHERE bed.id = task."bedId" AND bed.active = FALSE)
       OR EXISTS(SELECT FROM loc WHERE loc.id = task."locId" AND loc.active = FALSE)
       OR EXISTS(SELECT FROM org WHERE org.id = task."orgId" AND org.active = FALSE)
  `;

  static updateTagsForBed = `
    UPDATE task
    SET "mdTags"      = :mdTags,
        "customMDTag" = :customMDTag
    WHERE "bedId" = :bedId
      AND "removedBy" IS NULL
  `

  static removeTask = `
    UPDATE task
    SET "removedBy" = :removedBy,
        "removedOn" = now()
    WHERE "bedId" = :bedId
      AND "removedBy" IS NULL
  `

  // @args :bedId
  static getActiveTaskByBedId = `
    SELECT *
    FROM task
    WHERE "bedId" = :bedId
      AND "removedOn" IS NULL
  `
}
