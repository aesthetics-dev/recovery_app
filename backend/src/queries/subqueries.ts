export default abstract class Subqueries {
  static refBuild = `
    jsonb_build_object('name', name, 'id', id)
  `;

  static roleBuild = `
    json_build_object(
      'ref', ${Subqueries.refBuild},
      'type', type,
      'editable', editable
    )
  `;
}
