declare namespace Express {
  import AuthenticatedUser from '../../models/data/authenticated_user';

  interface Request {
    user: AuthenticatedUser
  }
}
