import { NextFunction, Request, Response } from 'express';
import Logger from '../utils/logger';
import ServerFailure from '../models/utility/server_failure';

class ExceptionHandler {
  private logger: Logger = new Logger('Exception Handler');

  catchAll(error: any, req: Request, res: Response, next: NextFunction) {
    this.logger.error(error);
    const failure = ServerFailure.fromError(error);
    return res.status(failure.code).send(failure.message);
  }
}

export default ExceptionHandler;
