// @ts-ignore
import { NextFunction, Request, Response } from 'express';
// @ts-ignore
import Failures from '../routers/failures';
import { asString } from '../models/utility/model_utils';
import AuthService from '../services/auth_service';

class AuthMiddleware {
  constructor(
    private authService: AuthService,
  ) {
  }

  async authenticateWithOrg(req: Request, res: Response, next: NextFunction): Promise<void> {
    const orgId = asString(req.params.orgId);
    if (!orgId) {
      throw Failures.badFormat;
    }
    const token = req.headers?.authorization;
    req.user = await this.authService.authenticateToken(token, orgId);
    next();
  }
}

export default AuthMiddleware;
