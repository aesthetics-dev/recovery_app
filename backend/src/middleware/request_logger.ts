import { NextFunction, Request, Response } from 'express';
import Logger from '../utils/logger';

class RequestLogger {
  logger = new Logger('RequestLogger')

  log(request: Request, response: Response, next: NextFunction) {
    this.logger.info(
      'Method:', request.method,
      '\nPath:', request.path,
      '\nAuth:', `${request.headers.authorization?.slice(0, 20)}...`,
      '\nBody:', request.body,
      '\nType:', request.headers['content-type'],
    );
    next();
  }
}

export default RequestLogger;
