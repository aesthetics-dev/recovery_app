import { Request, Response, Router } from 'express';
import OrganizationService from '../services/organization_service';
import Logger from '../utils/logger';
import { decideStatus } from './router_utils';
import { asOrganization, asString } from '../models/utility/model_utils';
import Failures from './failures';
import AccountService from '../services/account_service';
import { assertDefined } from '../utils/assertions';

class OrganizationRouter {
  readonly logger = new Logger('Organization Router')

  constructor(
    readonly organizationService: OrganizationService,
    readonly accountService: AccountService,
  ) {
  }

  getRouter() {
    const router = Router();
    router.get('/orgs',
      async (req, res) => this.getAll(req, res));

    router.route('/org/:orgId')
      .put((req, res) => this.updateOrg(req, res))
      .get((req, res) => this.getById(req, res));

    router.put('/email-in-domain/:orgId', (req, res) => this.checkEmailDomain(req, res));

    return router;
  }

  async getAll(req: Request, res: Response) {
    const response = await this.organizationService.getAll();
    return res.status(decideStatus(response)).send({ data: response });
  }

  async getById(req: Request, res: Response) {
    const orgId = assertDefined(asString(req.params.orgId));
    const admin = await this.accountService.isAdmin(orgId, req.user.email);
    const response = await this.organizationService.getFullById(orgId, { adminQuery: admin });
    return res.status(decideStatus(response)).send(response);
  }

  async updateOrg(req: Request, res: Response) {
    const { orgId } = req.params;
    const org = asOrganization(req.body);
    const acc = await this.accountService.getByOrgAndEmail(orgId, req.user.email);

    if (!acc?.ref?.id || !acc.admin) {
      throw Failures.unauthorized;
    }

    if (!org || org.ref?.id !== orgId) {
      throw Failures.orgUpdate;
    }

    await this.organizationService.updateOrg(orgId, acc.ref.id, org);
    return res.status(200).send();
  }

  async checkEmailDomain(req: Request, res: Response) {
    const orgId = assertDefined(asString(req.params.orgId));
    const email = assertDefined(asString(req.body.email));

    const response = await this.organizationService.isEmailKnown(orgId, email);
    return res.status(decideStatus(response)).send({ data: response });
  }
}

export default OrganizationRouter;
