import ServerFailure from '../models/utility/server_failure';

class Failures {
  static unauthorized = new ServerFailure('Unauthorized', 401)
  static badFormat = new ServerFailure('Bad Format', 400)
  static unknown = new ServerFailure('Unknown Error', 400)

  private static couldNot(action: string, code = 400) {
    return new ServerFailure(`Could not ${action}.`, code);
  }

  static taskGet = Failures.couldNot('get tasks')
  static taskPut = Failures.couldNot('put task')
  static taskSubmit = Failures.couldNot('submit task')
  static taskRemove = Failures.couldNot('remove task')

  static orgGet = Failures.couldNot('get organization')
  static orgsGet = Failures.couldNot('get organizations')
  static orgInsert = Failures.couldNot('insert organization')
  static orgDomainValidate = Failures.couldNot('validate user')
  static orgRemove = Failures.couldNot('remove organization')
  static orgUpdate = Failures.couldNot('update organization')

  static accGet = Failures.couldNot('get account')
  static accUpdate = Failures.couldNot('update account')
  static accInsert = Failures.couldNot('insert account')

  static bedRemove = Failures.couldNot('remove bed')
  static bedInsert = Failures.couldNot('insert bed')

  static tagRemove = Failures.couldNot('remove tag')
  static tagInsert = Failures.couldNot('insert tag')

  static locRemove = Failures.couldNot('remove location')
  static locInsert = Failures.couldNot('insert location')
}

export default Failures;
