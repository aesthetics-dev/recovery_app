import { Request, Response, Router } from 'express';

import AccountService from '../services/account_service';
import {
  asString, cast,
} from '../models/utility/model_utils';
import Failures from './failures';
import Heartbeat from '../models/data/heartbeat';
import Account from '../models/data/account';
import { assertDefined } from '../utils/assertions';
import LogoutTrace from '../models/data/logout_trace';

class AccountRouter {
  constructor(
    readonly accountService: AccountService,
  ) {
  }

  getRouter() {
    const router = Router();

    router.route('/org/:orgId/acc/token/:token?')
      .put((req, res) => this.putFcmToken(req, res))
      .delete((req, res) => this.deleteFcmToken(req, res));

    router.route('/org/:orgId/acc')
      .put((req, res) => this.updateAccountRoute(req, res))
      .get((req, res) => this.getByAuthentication(req, res));

    router.route('/org/:orgId/acc/heartbeat')
      .post((req, res) => this.postHeartbeat(req, res));

    router.route('/org/:orgId/acc/logout')
      .post((req, res) => this.postLogoutTrace(req, res));

    return router;
  }

  async getAccId(req: Request): Promise<string> {
    return assertDefined(
      await this.accountService.getIdByOrgIdAndEmail(req.params.orgId, req.user.email),
      Failures.unauthorized,
    );
  }

  async putFcmToken(req: Request, res: Response) {
    const accId = await this.getAccId(req);
    const fcmToken = assertDefined(asString(req.body.token));
    await this.accountService.insertFcmToken(accId, fcmToken);
    return res.status(200).send();
  }

  async deleteFcmToken(req: Request, res: Response) {
    const accId = await this.getAccId(req);

    // todo remove when more people update app
    const fcmToken = assertDefined(
      asString(req.params.token)
      || asString(req.body.token),
    );
    await this.accountService.deleteFcmToken(accId, fcmToken);
    return res.status(200).send();
  }

  async updateAccountRoute(req: Request, res: Response) {
    const acc = cast(Account)(req.body);
    const accId = await this.getAccId(req);
    if (acc?.ref?.id !== accId) {
      throw Failures.badFormat;
    }
    const result = await this.accountService.update(acc);
    return res.status(200).send(result);
  }

  async getByAuthentication(req: Request, res: Response) {
    const acc = await this.accountService.getByOrgAndEmail(
      req.params.orgId,
      req.user.email,
      true,
    );
    return res.status(200).send(acc);
  }

  async postHeartbeat(req: Request, res: Response) {
    const hb = cast(Heartbeat)(req.body.heartbeat);
    const accId = await this.getAccId(req);
    await this.accountService.saveHeartbeat(accId, hb);
    return res.status(200).send();
  }

  async postLogoutTrace(req: Request, res: Response) {
    const logoutTrace = cast(LogoutTrace)(req.body.trace);
    const accId = await this.getAccId(req);
    await this.accountService.saveLogoutTrace(accId, logoutTrace);
    return res.status(200).send();
  }
}

export default AccountRouter;
