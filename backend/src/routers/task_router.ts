import { Request, Response, Router } from 'express';
import TaskService from '../services/task_service';
import Logger from '../utils/logger';
import Failures from './failures';
import { asTask } from '../models/utility/model_utils';
import AccountService from '../services/account_service';

class TaskRouter {
  readonly logger = new Logger('Organization Router')

  constructor(
    private readonly taskService: TaskService,
    private readonly accService: AccountService,
  ) {
  }

  getRouter(): Router {
    const router = Router();
    router.route('/org/:orgId/tasks')
      .get(async (req, res) => this.getAllRoute(req, res))
      .put(async (req, res) => this.insertRoute(req, res));

    router.route('/org/:orgId/tasks/historic')
      .get(async (req, res) => this.getHistoricRoute(req, res));

    router.route('/org/:orgId/tasks/stats')
      .get(async (req, res) => this.getLocationTaskStats(req, res));

    return router;
  }

  async getAllRoute(req: Request, res: Response) {
    const acc = await this.accService.getByOrgAndEmail(req.params.orgId, req.user.email);

    if (!acc?.ref?.id) {
      return Failures.unauthorized;
    }

    const tasks = await this.taskService.getForAccount(acc.ref.id);
    return res.status(200).send({ data: tasks });
  }

  async getHistoricRoute(req: Request, res: Response) {
    const acc = await this.accService.getByOrgAndEmail(req.params.orgId, req.user.email);
    if (!acc?.ref?.id) return Failures.unauthorized;

    const tasks = await this.taskService.getHistoricForAccount(acc.ref.id);
    return res.status(200).send({ data: tasks });
  }

  async getLocationTaskStats(req: Request, res: Response) {
    const acc = await this.accService.getByOrgAndEmail(req.params.orgId, req.user.email);
    if (!acc?.ref?.id) return Failures.unauthorized;
    const stats = await this.taskService.getLocationTaskStats(acc.ref.id);
    return res.status(200).send({ data: stats });
  }

  async insertRoute(req: Request, res: Response) {
    const { orgId } = req.params;
    const task = asTask(req.body);

    if (!task) {
      throw Failures.badFormat.addInfoTrace('Task validation failed');
    }

    const accId = await this.accService.getIdByOrgIdAndEmail(req.params.orgId, req.user.email);
    if (!accId || task.orgRef?.id !== orgId) {
      return Failures.unauthorized.addInfoTrace('no account id or doesnt match the authenticated organization');
    }

    await this.taskService.submitTask(accId, task);
    return res.status(200).send();
  }
}

export default TaskRouter;
