import Failure from '../models/utility/failure';

export function decideStatus(val: any) : number {
  if ([null, undefined].includes(val)) {
    return 404;
  } if (val instanceof Failure) {
    return 400;
  }
  return 200;
}
