import Loc from './loc';
import {
  asArray, asLocation, asObjectArray, asReference, asRole, asString, asStringArray,
} from '../utility/model_utils';
import Reference from './reference';
import Role from './role';

class Organization {
  ref: Reference | undefined
  domains: string[] | undefined
  locations: Loc[] | undefined
  roles: Role[] | undefined
  authType: string | undefined
  emails: string[] | undefined

  constructor({
  // @ts-ignore
    ref, domains, locations, roles, authType, emails,
  }) {
    this.authType = asString(authType);
    this.ref = asReference(ref);
    this.domains = asArray(domains, asString);
    this.locations = asObjectArray(locations, asLocation);
    this.roles = asObjectArray(roles, asRole);
    this.emails = asStringArray(emails);
  }
}

export default Organization;
