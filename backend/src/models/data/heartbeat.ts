import { asString } from '../utility/model_utils';

export default class Heartbeat {
  clientVersion: string | null
  clientOs: string | null
  clientBuildNumber: string | null

  constructor(props: any) {
    this.clientVersion = asString(props.clientVersion) ?? null;
    this.clientOs = asString(props.clientOs) ?? null;
    this.clientBuildNumber = asString(props.clientBuildNumber) ?? null;
  }
}
