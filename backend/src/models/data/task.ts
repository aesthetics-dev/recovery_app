import {
  asFootprint,
  asReference,
  asString, asStringArray,
} from '../utility/model_utils';
import Reference from './reference';
import Footprint from './footprint';

class Task {
  orgRef?: Reference;
  bedRef?: Reference;
  locRef?: Reference;
  tags: string[];
  mdTags: string[];
  customTag: string;
  customMDTag: string;
  mdETA?: Footprint;
  created?: Footprint;
  removed?: Footprint;

  constructor({
    orgRef, bedRef, locRef, tags, mdTags, customTag, customMDTag, mdETA, created, removed,
  }: {
    orgRef?: Reference, bedRef?: Reference, locRef?: Reference, tags?: string[],
    mdTags?: string[], customTag?: string, customMDTag?: string, mdETA?: Footprint,
    created?: Footprint, removed?: Footprint,
  }) {
    this.orgRef = asReference(orgRef);
    this.bedRef = asReference(bedRef);
    this.locRef = asReference(locRef);
    this.tags = asStringArray(tags) || [];
    this.mdTags = asStringArray(mdTags) || [];
    this.customTag = asString(customTag)?.trim() || '';
    this.customMDTag = asString(customMDTag)?.trim() || '';
    this.mdETA = asFootprint(mdETA);
    this.created = asFootprint(created);
    this.removed = asFootprint(removed);
  }

  isEmpty() {
    return !(this.tags?.length || this.customTag?.trim());
  }
}

export default Task;
