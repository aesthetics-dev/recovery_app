import Reference from './reference';
import { asNumber, asReference } from '../utility/model_utils';

export default class Role {
  ref: Reference | undefined;
  type: number | undefined;
  editable: boolean;

  // @ts-ignore
  constructor({ ref, type, editable }) {
    this.ref = asReference(ref);
    this.type = asNumber(type);
    this.editable = editable === true;
  }
}
