import {
  asObjectArray, asReference, asRole,
} from '../utility/model_utils';
import Reference from './reference';
import Role from './role';

class Account {
  readonly ref: Reference | undefined;
  readonly orgRef: Reference | undefined;
  readonly role: Role | undefined;
  readonly notificationsOn: boolean;
  readonly locRefs: Reference[] | undefined;
  readonly admin: boolean | undefined;

  constructor({
    ref, orgRef, role, notificationsOn, locRefs, admin,
  }: {
    ref?: unknown, orgRef?: unknown, name?: unknown,
    role?: unknown, notificationsOn?: unknown, locRefs?: unknown, admin?: unknown
  }) {
    this.ref = asReference(ref);
    this.orgRef = asReference(orgRef);
    this.role = asRole(role);
    this.notificationsOn = !!notificationsOn;
    this.locRefs = asObjectArray(locRefs, asReference);
    this.admin = admin === true ? true : undefined;
  }
}

export default Account;
