class Reference {
  name: string | undefined
  id: string | undefined
  secondaryId: string | undefined

  constructor({ id, name, secondaryId }: { id?: string, name?: string, secondaryId?: string } = {}) {
    this.id = id;
    this.secondaryId = secondaryId;
    this.name = name;
  }

  static isSameName(r1: Reference, r2: Reference): boolean {
    return r1.name === r2.name;
  }
}

export default Reference;
