import { asString } from '../utility/model_utils';

export default class LogoutTrace {
  logoutMethod: string

  constructor(props: any = {}) {
    this.logoutMethod = asString(props.logoutMethod) ?? 'unknown';
  }
}
