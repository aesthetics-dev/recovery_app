import { asNumber, asReference } from '../utility/model_utils';
import Reference from './reference';

export default class LocationTaskAverage {
  locRef: Reference
  secondsToComplete: number

  // @ts-ignore
  constructor({ locRef, secondsToComplete }) {
    this.locRef = <any>asReference(locRef);
    this.secondsToComplete = asNumber(secondsToComplete, { roundTo: 0 }) || -1;
  }
}
