import Reference from './reference';
import { asReference, asString } from '../utility/model_utils';

class Footprint {
  ref?: Reference;
  timestamp?: string;

  constructor({ ref, timestamp }: { ref?: Reference, timestamp?: string } = {}) {
    this.ref = asReference(ref);
    this.timestamp = asString(timestamp);
  }
}

export default Footprint;
