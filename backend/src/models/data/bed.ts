import { asReference } from '../utility/model_utils';
import Reference from './reference';

class Bed {
  ref: Reference | undefined

  constructor({ ref }: { ref?: Reference }) {
    this.ref = asReference(ref);
  }
}

export default Bed;
