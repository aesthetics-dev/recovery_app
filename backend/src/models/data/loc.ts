import Bed from './bed';
import {
  asArray, asBed, asObjectArray, asReference, asString,
} from '../utility/model_utils';
import Reference from './reference';

class Loc {
  ref: Reference | undefined
  orgId: string | undefined
  tags: string[] | undefined
  mdTags: string[] | undefined
  beds: Bed[] | undefined

  constructor({
    ref, tags, mdTags, orgId, beds,
  }: { ref?: unknown, tags?: unknown[], mdTags?: unknown, orgId?: unknown, beds?: unknown }) {
    this.ref = asReference(ref);
    this.orgId = asString(orgId);
    this.tags = asArray(tags, asString);
    this.mdTags = asArray(mdTags, asString);
    this.beds = asObjectArray(beds, asBed);
  }
}

export default Loc;
