import Reference from './reference';

export default class AbridgedOrganization {
  ref: Reference | undefined;
  authType: string | undefined;

  // @ts-ignore
  constructor({ ref, authType }) {
    this.ref = ref;
    this.authType = authType;
  }
}
