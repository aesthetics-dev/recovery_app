class AuthenticatedUser {
  email: string;
  authenticated: boolean;

  constructor({ email } : { email: string } = { email: '' }) {
    this.email = email;
    this.authenticated = !!this.email;
  }
}

export default AuthenticatedUser;
