class Failure {
    message: string;

    constructor(message: string = 'Unspecified error.') {
      this.message = message;
    }
}

export default Failure;
