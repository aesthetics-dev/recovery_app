// @ts-ignore
import { isDefined } from 'ts-is-present';
import * as _ from 'lodash';
import {
  SafeCaster, Formatter, Validator, RFunction,
} from './utility_types';
import Organization from '../data/organization';
import Account from '../data/account';
import Bed from '../data/bed';
import Loc from '../data/loc';
import Reference from '../data/reference';
import Task from '../data/task';
import Footprint from '../data/footprint';
import LocationTaskAverage from '../data/location_task_average';
import NumberUtils from '../../utils/number_utils';
import Role from '../data/role';
import AbridgedOrganization from '../data/abridged_organization';
import Failure from './failure';
import Failures from '../../routers/failures';
import { assertDefined } from '../../utils/assertions';

export function asObject<T>(val: any, constructor: SafeCaster<any, T>): T | undefined {
  return typeof val === 'object' && val !== null ? constructor(val) : undefined;
}

export function asNumber(
  val: any, {
    roundTo: decimalPlaces = -1,
    min = Number.NEGATIVE_INFINITY,
    max = Number.POSITIVE_INFINITY,
  } = {},
): number | undefined {
  if (typeof val !== 'number') {
    return undefined;
  }
  let out = val;
  if (decimalPlaces >= 0) {
    out = NumberUtils.roundTo(out, decimalPlaces);
  }
  return NumberUtils.clamp(out, min, max);
}

export function asString(val: any, { validator, formatter }:
  { validator?: Validator<string>, formatter?: Formatter<string> } = {}) {
  if (typeof val !== 'string') {
    return undefined;
  }

  return !validator || validator(val)
    ? formatter
      ? formatter(val)
      : val
    : undefined;
}

export function asStringArray(data: unknown): string[] | undefined {
  return Array.isArray(data)
    ? data.map((v) => asString(v)).filter(isDefined)
    : undefined;
}

export function asObjectArray<T>(val: unknown, constructor: SafeCaster<any, T>): T[] | undefined {
  return Array.isArray(val)
    ? val.map<T | undefined>((v) => asObject(v, constructor)).filter(isDefined)
    : undefined;
}

export function castSafe<T>(Constructor: { new(args: any): T }): SafeCaster<any, T> {
  return (val: any) => asObject(val, (o) => new Constructor(o));
}

export function cast<T>(Constructor: { new(args: any): T }, failure: Failure = Failures.badFormat): RFunction<any, T> {
  return (val: any) => assertDefined(
    asObject(val, (o) => new Constructor(o)), failure,
  );
}

export function asArray<T>(val: unknown, constructor: SafeCaster<any, T>): T[] | undefined {
  return Array.isArray(val)
    ? val.map<T | undefined>(constructor).filter(isDefined)
    : undefined;
}

export function asOrganization(org: unknown): Organization | undefined {
  return asObject(org, (o) => new Organization(o));
}

export function asAbridgedOrganization(org: unknown): AbridgedOrganization | undefined {
  return asObject(org, (o) => new AbridgedOrganization(o));
}

export function asAccount(acc: unknown): Account | undefined {
  return asObject(acc, (o) => new Account(o));
}

export function extractFcmTokens(acc: any): string[] | undefined {
  return asStringArray(acc?.fcmTokens);
}

export function asTask(obj: unknown): Task | undefined {
  return asObject(obj, (o) => new Task(o));
}

export function asFootprint(obj: unknown): Footprint | undefined {
  return asObject(obj, (o) => new Footprint(o));
}

export function asLocation(loc: unknown): Loc | undefined {
  return asObject(loc, (o) => new Loc(o));
}

export function asBed(bed: unknown): Bed | undefined {
  return asObject(bed, (o) => new Bed(o));
}

export function asRole(role: unknown): Role | undefined {
  return asObject(role, (o) => new Role(o));
}

export function asReference(ref: unknown): Reference | undefined {
  return asObject(ref, (o) => new Reference(o));
}

export function asLocationTaskAverage(ref: unknown): LocationTaskAverage | undefined {
  return asObject(ref, (o) => new LocationTaskAverage(o));
}

export function constructLast<T>(data: any[], constructor: SafeCaster<unknown, T>): T | undefined {
  return constructor(_.last(data));
}
