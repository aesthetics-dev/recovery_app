export type SafeCaster<F, T> = (obj: F) => T | undefined;
export type RFunction<R, A> = (obj: A) => R;
export type Validator<T> = (obj: T) => boolean;
export type Formatter<T> = (obj: T) => T;
