import * as _ from 'lodash';
import Failure from './failure';
import { asNumber, asString, asStringArray } from './model_utils';
import Failures from '../../routers/failures';
import Logger from '../../utils/logger';

class ServerFailure extends Failure {
  code: number;
  trace: string[];
  infoTrace: string[];
  private static readonly logger: Logger = new Logger('ServerFailure')

  constructor(message: string, code: number, trace: string[] = [], infoTrace: string[] = []) {
    super(message);
    this.code = code;
    this.trace = trace;
    this.infoTrace = infoTrace;
  }

  addTrace(message: string) : ServerFailure {
    const newFail = _.cloneDeep(this);
    newFail.trace.push(message);
    return newFail;
  }

  addInfoTrace(message: string) : ServerFailure {
    const newFail = _.cloneDeep(this);
    newFail.infoTrace.push(message);
    return newFail;
  }

  static fromError(error: any, fallBack = Failures.unknown) : ServerFailure {
    try {
      const message = asString(error.message);
      const code = asNumber(error.code);

      if (!message || !code) {
        this.logger.error(error);

        return fallBack;
      }

      const trace = asStringArray(error.trace);
      const infoTrace = asStringArray(error.infoTrace);
      return new ServerFailure(message, code, trace, infoTrace);
    } catch (e) {
      return fallBack;
    }
  }
}

export default ServerFailure;
