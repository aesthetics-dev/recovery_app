export enum Roles {
  physician,
  nurse
}

export enum UpdateType {
  tasks = 'tasks',
  organization = 'organization',
}

export enum TaskUpdateType {
  inserted,
  completed,
  acknowledged
}
