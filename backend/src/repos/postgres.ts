import { QueryResult } from 'pg';
// @ts-ignore
import * as Knex from 'knex';
// @ts-ignore
import { isDefined } from 'ts-is-present';
import {
  DB_HOST, DB_NAME, DB_PASS, DB_PORT, DB_USER,
} from '../utils/env';
import Logger from '../utils/logger';
import { traceFailure } from '../utils/wrappers';
import { SafeCaster } from '../models/utility/utility_types';
import AccountQueries from '../queries/account_queries';

export const config: Knex.Config = {
  client: 'pg',
  version: '12.3',
  connection: {
    user: DB_USER,
    host: DB_HOST,
    database: DB_NAME,
    password: DB_PASS,
    port: DB_PORT,
  },
};

class Postgres {
  g: Knex
  logger = new Logger('Postgres')

  constructor() {
    this.g = Knex(config);
    this.declareFunctions().then(() => this.logger.info('Functions declared!'));
  }

  async declareFunctions() {
    await Promise.all([
      this.query(AccountQueries.isAdminEmailDeclaration),
    ]);
  }

  @traceFailure()
  async query(queryString: string, values?: any): Promise<QueryResult> {
    this.logger.debug(queryString);
    return this.g.raw(queryString, values);
  }

  @traceFailure()
  async queryArray<T>(queryString: string, values?: any, caster: SafeCaster<any, T> = (v) => v): Promise<T[] | undefined> {
    return this
      .query(queryString, values)
      .then((r) => r.rows.map(caster).filter(isDefined));
  }

  @traceFailure()
  async querySingle<T>(queryString: string, values?: any, caster: SafeCaster<any, T> = (v) => v): Promise<T | undefined> {
    return this
      .queryArray(queryString, values, caster)
      .then((arr) => (arr ? arr[0] : undefined));
  }

  @traceFailure()
  async queryIsNotEmpty(queryString: string, values?: any): Promise<boolean> {
    return this.queryArray(queryString, values).then((rows) => (rows?.length ?? 0) > 0);
  }
}

export default Postgres;
