// @ts-ignore
import admin from 'firebase-admin';

const firebaseConfig: admin.AppOptions = ({
  databaseURL: 'https://md-aware.firebaseio.com',
  projectId: 'md-aware',
  storageBucket: 'md-aware.appspot.com',
  credential: admin.credential.cert({
    projectId: 'md-aware',
    // eslint-disable-next-line max-len
    privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDeUjyWz+I7BSzD\njvW5ioIPgZT7r9fE02bMUbhrydIbLJIzdNshAurRS7nJq6bLJqXTlH4kjETzpn5c\nrkXD8eTFvEpZZuYrl++DTk3y0cD6c7QLDmfIVS9G7iUWH1H+QMBUtC7wq6+WjQWm\nj0xTTJ0gdwnGYeB1ZVkSkomzdZP11qgrNnmsVDp1olFNTlWzPgAUNOTCPEp2B55A\n6g/cVaXRInJH1BiLr9c4IEnm0SfTgnq84CJclfHyBX9o5pb6nsJHGQfOaK0NAVOf\nMCf8hokLR5NhL/T+RVGJt3aJ0D0vlkQxRBMd1dKpgsShJBbxEjLGCq11YSP7HWO+\nwXkfdWX7AgMBAAECggEADPM6X0iNs4VqJZwKuTuVtuSD8qM8DNWJm+Y4uWKgr/+J\nJqEscIBUgEuKb3JlvVireSchr+xRyeWNX8Ww/6hz/IaqyAlj5+8qPvpNNRM9y/j4\naFp29ibsIZQWqfK6GnbYHTb/svo1OK5PQXQAbKu+TVlRwvIJmp0EMr5Bcigrrbqu\n9KIqqWFJykoqj0Hyx/rQb6DPyWK1BbwBt6BmbYjoylXHyp6FfW+lOrFfXLj1D+Td\nV5Kuq7yi2XhAq2OSmbEg+OFNMQHEjS7FF9cp31oYkfmnOH3My+BwtoNosB6wXrP8\nJXjjqqc0oSkHWu33+FJKcCL7hBYPxIWKMWFZQ9AV5QKBgQD0yigBM/Lkcuiv29Zl\nVKX5Jg4O9VLfG6RDGp54abIfz7fltfimDOQActpgerPCxQsSciERlrRXFIfnZU9D\n5aegnbpF2hhD8Gj/0CCdykw1rrGg9ZERaw/0NI1CDDToEq1cuC8T7js8d3Ro56y5\nozgiPLr+/HWrepOMR1GkfXnOFQKBgQDogKq7scDIrWULiV/t03jqxOS4WRmsxgzi\nzV3LbGEuAcfA4godB9tivBzViGR+C9KPH6ivTqmJjoVlm/fBx5AvG0CkoO2gva8k\nhkATokDEwTGUQk+BL89tkG/3kKjyPaUENfyLVnCfOVrfTFat/aoiE8tuBgILyvzN\nC0zqlXx3zwKBgQDHzrL0uGiMnFbeJL1NnNiVE8Dp6MkqH6/mrjWn5+5jFPPwZqZ9\nM4xv6U/i7IUKeDffaJUhfDfb1IBSGlmm/C/lZJmFJs4jtnqeRE47PcMTFnMbBRbS\ndudg1/xtavVMcOJdCmIzDIQBiSfV8TUFmfrZx2SDeaK6w6ToHdbAQ0NHOQKBgQCz\nWDTa7aVw5tfPy1r1ADAAoHpxfUFh1a+w74tPVD2uGG92aKY2eI4RHlv7luc1l3cH\nMfuTptD+K7LYizPVBVRCEfqOQxip6keEnA4nVvwxCXluDKx5UWDm+gh0ZeD8YvK8\nScyRys6686oWHzOpqMco3C4Ko5XGdQEGRZZCSacmXwKBgQDwh6aFNadRrdbXOBdT\nQHoxVLup+LHkbh/f0GsRYXqrS9kHFJfyGH/T4v3W+ZSgcO16eF36LQXsBZ+AwSfU\nqq5vT60FYX3tBs6CkSILT5RPvngMTkBMGVa1FK2LZ3NJRtvpF0jjOrwU6ElzmqAC\nNGMobz4ESn0i2nmoimaya4lOfA==\n-----END PRIVATE KEY-----\n',
    clientEmail: 'firebase-adminsdk-qgwq8@md-aware.iam.gserviceaccount.com',
  }),
});

class Firebase {
  static initialize() {
    // Initialize Firebase
    admin.initializeApp(firebaseConfig);
  }
}

export default Firebase;
