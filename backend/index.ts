// @ts-ignore
import * as env from 'dotenv';
import DI from './src/utils/dependency_injector';
import { PORT } from './src/utils/env';
import { saitama } from './src/other/facts';

env.config();

const { server } = DI.get();

server.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(saitama(PORT));
});
