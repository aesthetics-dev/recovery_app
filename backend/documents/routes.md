
## Routes

| call | path                   | auth  | description          |
| ---- | ---------------------- | :---: | -------------------- |
| PUT  | `/orgs/:orgName/tasks` |   ✓   | create/update a task |


## Organizations 

| call | path    | auth  | description                      |
| ---- | ------- | :---: | -------------------------------- |
| GET  | `/orgs` |       | list of public organization data |

- returns:
```js
[
  {
    name: string
  }
]
```

| call | path             | auth  | description       |
| ---- | ---------------- | :---: | ----------------- |
| GET  | `/orgs/:orgName` |   ✓   | organization info |

  - returns:
```js
{
  name: string,
  utcOffset: int,
}
```

| call | path                               | auth  | description                 |
| ---- | ---------------------------------- | :---: | --------------------------- |
| GET  | `/orgs/:orgName/acc/:email/exists` |   ✓   | bool whether account exists |

- returns:
```js
{
  exists: bool,
}
```

| call | path                        | auth  | description          |
| ---- | --------------------------- | :---: | -------------------- |
| PUT  | `/orgs/:orgName/acc/:email` |   ✓   | get account by email |

- returns:
```js
{
  name: string,
  email: string,
  role: "nurse" | "md",
  orgName: string
}
```

| call | path                        | auth  | description         |
| ---- | --------------------------- | :---: | ------------------- |
| PUT  | `/orgs/:orgName/acc/:email` |   ✓   | update account info |

- accepts: 
```js
{
  name: string,
  role: "nurse" | "md",
  followedLocations: [locationName: string],
  notificationsOn: bool
}
```


## Locations

| call | path                             | auth  | description                                        |
| ---- | -------------------------------- | :---: | -------------------------------------------------- |
| GET  | `/orgs/:orgName/tasks/following` |   ✓   | get tasks for locations that the user is following |

- returns:
```js
[
  {
    bed_name: string,
    loc_name: string,
    tags: [string],
    mdTags: [string],
    customTag: string,
    customMdTag: string,
    createdTs: string,
    authorName: string,
    authorEmail: string,
  }
]
```