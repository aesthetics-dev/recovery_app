
### Account

| column    | type         |
| --------- | ------------ |
| id        | serial       |
| orgId    | int          |
| name      | varchar(64)  |
| email     | varchar(64)  |
| role      | varchar(32)  |
| active_ts | varchar(32s) |


| roles |
| ----- |
| nurse |
| md    |


### `org`

| column     | type          |
| ---------- | ------------- |
| id         | serial        |
| name       | varchar(64)   |
| domains    | varchar(32)[] |
| utcOffset | int           |


### `loc`

| column      | type          |
| ----------- | ------------- |
| id          | serial        |
| orgId (fk) | int           |
| name        | varchar(64)   |
| tags        | varchar(64)[] |
| md_tags     | varchar(64)[] |

### `bed`

| column      | type   |
| ----------- | ------ |
| id          | serial |
| string      | name   |
| loc_id (fk) | int    |

### `acc_rel`

| column      | type        |
| ----------- | ----------- |
| id          | bigserial   |
| acc_id (fk) | int         |
| entity_id   | int         |
| rel_type    | varchar(16) |

Relational table for accounts, where `rel_type` specified the type of the relation (one of the relations below).

| relation   | entity | description               |
| ---------- | ------ | ------------------------- |
| org_admin  | org    | admin for an organization |
| loc_follow | loc    | following a location      |

### `task`

| column         | type          |
| -------------- | ------------- |
| id             | bigserial     |
| bed_id (fk)    | int           |
| tags           | varchar(64)[] |
| mdTags         | varchar(64)[] |
| custom_tag     | varchar(256)  |
| custom_md_tag  | varchar(256)  |
| created_ts     | varchar(32)   |
| removed_ts     | varchar(32)   |
| created_acc_id | int           |
| removed_acc_id | int           |