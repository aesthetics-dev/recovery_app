# MDAware

## What is it

MDAware is an advanced to-do list app that helps doctors and nurses communicate patient information with each other more efficiently.

### Features

#### Task list

The task list allows doctors and nurses see all active tasks for the floors / departments they *follow*. 
Tasks are created by medical staff; they consist of a location, bed number, and a list of symptoms that the patient currently occupying that bed is experiencing.

In the screenshot below you can see the account of a hypothetical user - physician Daniel Williams. Their name is displayed at the top, followed by locations they follow, and a list of tasks for all of those locations.

<img src="https://play-lh.googleusercontent.com/w6LUWEwAa58WHz9FyL_gpJo_oT-_F6_yeBaRLtdwv04jlkP6EkAKF_JXtL61xpx2aFU=w1477-h1095" alt="Screenshot Image" style="width: 200px;" />

<div style="page-break-after: always; break-after: page;"></div>

#### Task creation

The app allows users to create new tasks quickly, as it features a list of beds and a set of predefined symptoms they can choose from. If they wish to, however, they can always add custom notes.

<img src="https://play-lh.googleusercontent.com/QssH2vzbFYF4j5th8C-RDzqg4BCpp6K5yx2aKohgj0nBH2cxjSekr1khMaB_9D1tmsU=w1477-h1095" alt="Screenshot Image" style="width: 200px;" />

<div style="page-break-after: always; break-after: page;"></div>

#### Account Edit

Naturally the app allows users to change their name, choose their role, *follow* locations, and edit a few additional settings.

<img src="https://play-lh.googleusercontent.com/VQFwhp0ZAOFhANi6Ciu4tzAJgU3YMMPKZfXm_tnSx-hHg5wS8GCjucIxXIhcUj8Hel4=w1477-h1095" alt="Screenshot Image" style="width:200px;" />

<div style="page-break-after: always; break-after: page;"></div>

#### Login

The app is set up to support multiple organizations (usually hospitals), which means a single app can be sold to and used by multiple hospitals at once. Only users whose emails have been added by the organization admins can log in, which guarantees that the admins are aware of everyone who has access to patient information.

<img src="https://play-lh.googleusercontent.com/Y3TtuvBWfghHJ44UmrCh0M4go7vZYvXdN5TxJy6n0GLk1Gk8HszkZm2bB3SGaLEN1DQ=w1477-h1095" alt="Screenshot Image" style="width: 200px;" />

<div style="page-break-after: always; break-after: page;"></div>

#### Admin Panel

Each organization can add admins who can:

1. Edit the organization
2. Add/Remove users from the organization
3. Add/remove/edit locations
4. Add/remove/edit beds for each of the location
5. Add/remove/edit predefined symptoms for each of the locations

#### Statistics

The app also allows hospital staff to track statistics about their work, which was requested as a fun way for departments to compete among each other on how long it takes them to complete tasks. We also expect this would be a valuable tool for hospital management to track worker productivity.

<div style="page-break-after: always; break-after: page;"></div>

## Validation

The app launched at Brigham and Women's hospital in Boston in early January of 2020 on 2 floors. These 2 floors have recovering patients in non-critical condition, which is why we thought it was safe to field test there. Since then (11 months later), over 40,000 tasks were created and completed (average 900/week) by the 60 doctors and nurses in those departments.

One of my co-founders, an anesthesiologist at Brigham and Women's, claimed that the app has become an integral part of their workflow, and that the few times that it has been down (never for more than 10 minutes) -- they have really felt its absence. We have plans to conduct extensive interviews of MDAware users to help us shape the next iterations of the app.

## Business Model

For the past 11 months, MDAware has been completely free to Brigham and Women's staff -- mostly because the cost of running our service are extremely low ($3/month) and because we wanted to get feedback from on of the leading hospitals in the world.

We're planning to move into other hospitals -- this time with a tiered paid model. The subscription model will likely be an annual subscription based on the number of users the organization has so that hospitals can field test the app on a few of their floors and move to others by upgrading their subscription. This way the barrier to entry is low, and the pricing model will incentivize to buy higher tiers.

## Next Steps

- Conduct interviews with the users of MDAware to guide the release of the non-beta version of the app
- As we are operating in the medical space, we think it is wise to either start an LLC or incorporate soon to avoid legal personal liabilities
- Find out how other hospitals operate to make sure other hospitals needs are similar to Brigham and Women's
- Solidify a business model and settle on initial pricing
- Build a professional landing page