abstract class DBCollections {
  static const organizations = "Organizations";
  static const users = "Users";
  static const locations = "Locations";
  static const tasks = "Tasks";
  static const publicData = "PublicData";
}

abstract class DBDocuments {
  static const dataSummary = "SignInData";
}
