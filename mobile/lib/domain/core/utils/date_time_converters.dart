import 'package:intl/intl.dart';

class DateTimeConverters {
  static String dateToJson(DateTime date) => date?.toUtc()?.toIso8601String();

  static DateTime dateFromJson(String utc8601String) {
    return utc8601String != null
        ? DateTime.parse(utc8601String).toLocal()
        : null;
  }

  static String dateToJm(DateTime dt) => DateFormat.jm().format(dt);
}
