import 'package:logger/logger.dart';
import 'package:uuid/uuid.dart';

final Logger log = Logger();
final Uuid uuid = Uuid();
String uniqid([List<String> props = const []]) =>
    (props + [uuid.v4()]).join(' | ');
