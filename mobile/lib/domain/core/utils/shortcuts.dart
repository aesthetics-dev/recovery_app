import 'package:recovery_app/domain/core/utils/static_objects.dart';
import 'package:recovery_app/domain/models/core/response.dart';

Response<T> logAndFail<T>(dynamic error, {Response<T> response}) {
  log.e(error);
  return response ?? Response<T>.error();
}

typedef Mutator<T> = T Function(T val);
typedef Mapper<T> = List<T> Function(List<T> list);

List<T> removeListItem<T>(List<T> list, int index) {
  return [...list.sublist(0, index), ...list.sublist(index + 1)];
}

List<T> updateListItem<T>(List<T> list, int index, Mutator<T> mutator) {
  return [
    ...list.sublist(0, index),
    mutator(list[index]),
    ...list.sublist(index + 1)
  ];
}

List<T> addListItem<T>(List<T> list, T newValue) {
  return [newValue, ...list];
}
