extension IterableExtension<E> on Iterable<E> {
  Iterable<T> indexedMap<T>(T Function(E, int) f) sync* {
    int i = 0;
    for (final e in this) {
      yield f(e, i++);
    }
  }
}
