import 'package:flutter/material.dart';

extension NavigatorStateExtension on NavigatorState {
  void popAllAndPushNamed(String routeName) {
    popUntil((route) => route.isFirst);
    pushReplacementNamed(routeName);
  }
}
