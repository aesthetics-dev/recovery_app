import 'dart:convert';

import 'package:http/http.dart' as http;

extension HttpUtility on http.Response {
  http.Response verify<T>() {
    // log.i({"status": statusCode, "body": body});
    if (statusCode < 200 || statusCode > 299) {
      throw body;
    }

    return this;
  }

  T parse<T>({T Function(dynamic data) constructor}) {
    final decoded = jsonDecode(body);
    return constructor == null ? decoded as T : constructor(decoded);
  }

  List<T> parseObjectArray<T>(
      {T Function(Map<String, dynamic> json) constructor}) {
    final decoded = jsonDecode(body) as List<dynamic>;
    return decoded.map((e) => constructor(e as Map<String, dynamic>)).toList();
  }

  T parseObject<T>({T Function(Map<String, dynamic> json) constructor}) {
    final decoded = jsonDecode(body) as Map<String, dynamic>;
    return constructor(decoded);
  }
}
