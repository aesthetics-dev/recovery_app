import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/util/message/message.dart';

class Response<T> extends Equatable {
  final T data;
  final Message message;
  final bool success;
  bool get failure => !success;

  const Response({
    @required this.data,
    @required this.success,
    @required this.message,
  });

  factory Response.success({T data, Message message}) {
    return Response(
      data: data,
      message: message ?? Messages.success,
      success: true,
    );
  }

  factory Response.error({T data, Message message}) {
    return Response(
      data: data,
      message: message ?? Messages.error,
      success: false,
    );
  }

  @override
  List<Object> get props => [data, message, success];

  @override
  String toString() =>
      'Response(data: $data, message: $message, success: $success)';

  Response<T> copyWith({
    T data,
    Message message,
    bool success,
  }) {
    return Response<T>(
      data: data ?? this.data,
      message: message ?? this.message,
      success: success ?? this.success,
    );
  }
}
