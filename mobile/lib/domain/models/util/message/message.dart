import 'package:equatable/equatable.dart';

class Message extends Equatable {
  final String content;
  final MessageType type;

  const Message._({this.content, this.type});

  factory Message.error(String content) {
    return Message._(type: MessageType.error, content: content);
  }

  factory Message.info(String content) {
    return Message._(type: MessageType.info, content: content);
  }

  factory Message.success(String content) {
    return Message._(type: MessageType.success, content: content);
  }

  @override
  List<Object> get props => [content, type];

  @override
  String toString() => 'Message(content: $content, messageType: $type)';
}

enum MessageType {
  info,
  error,
  success,
}
