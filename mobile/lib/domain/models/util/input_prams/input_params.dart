import 'package:freezed_annotation/freezed_annotation.dart';

part 'input_params.freezed.dart';

@freezed
abstract class InputParams implements _$InputParams {
  factory InputParams({
    @Default('') String placeholder,
    @Default('') String initialValue,
    @Default(true) bool canSubmit,
    @Default(false) bool clearOnSubmit,
    @Default(false) bool obscureText,
    Function() onSubmit,
    Function(String) onChange,
    String value,
  }) = _InputParams;
}
