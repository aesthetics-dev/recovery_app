// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'input_params.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$InputParamsTearOff {
  const _$InputParamsTearOff();

// ignore: unused_element
  _InputParams call(
      {String placeholder = '',
      String initialValue = '',
      bool canSubmit = true,
      bool clearOnSubmit = false,
      bool obscureText = false,
      dynamic Function() onSubmit,
      dynamic Function(String) onChange,
      String value}) {
    return _InputParams(
      placeholder: placeholder,
      initialValue: initialValue,
      canSubmit: canSubmit,
      clearOnSubmit: clearOnSubmit,
      obscureText: obscureText,
      onSubmit: onSubmit,
      onChange: onChange,
      value: value,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $InputParams = _$InputParamsTearOff();

/// @nodoc
mixin _$InputParams {
  String get placeholder;
  String get initialValue;
  bool get canSubmit;
  bool get clearOnSubmit;
  bool get obscureText;
  dynamic Function() get onSubmit;
  dynamic Function(String) get onChange;
  String get value;

  $InputParamsCopyWith<InputParams> get copyWith;
}

/// @nodoc
abstract class $InputParamsCopyWith<$Res> {
  factory $InputParamsCopyWith(
          InputParams value, $Res Function(InputParams) then) =
      _$InputParamsCopyWithImpl<$Res>;
  $Res call(
      {String placeholder,
      String initialValue,
      bool canSubmit,
      bool clearOnSubmit,
      bool obscureText,
      dynamic Function() onSubmit,
      dynamic Function(String) onChange,
      String value});
}

/// @nodoc
class _$InputParamsCopyWithImpl<$Res> implements $InputParamsCopyWith<$Res> {
  _$InputParamsCopyWithImpl(this._value, this._then);

  final InputParams _value;
  // ignore: unused_field
  final $Res Function(InputParams) _then;

  @override
  $Res call({
    Object placeholder = freezed,
    Object initialValue = freezed,
    Object canSubmit = freezed,
    Object clearOnSubmit = freezed,
    Object obscureText = freezed,
    Object onSubmit = freezed,
    Object onChange = freezed,
    Object value = freezed,
  }) {
    return _then(_value.copyWith(
      placeholder:
          placeholder == freezed ? _value.placeholder : placeholder as String,
      initialValue: initialValue == freezed
          ? _value.initialValue
          : initialValue as String,
      canSubmit: canSubmit == freezed ? _value.canSubmit : canSubmit as bool,
      clearOnSubmit: clearOnSubmit == freezed
          ? _value.clearOnSubmit
          : clearOnSubmit as bool,
      obscureText:
          obscureText == freezed ? _value.obscureText : obscureText as bool,
      onSubmit: onSubmit == freezed
          ? _value.onSubmit
          : onSubmit as dynamic Function(),
      onChange: onChange == freezed
          ? _value.onChange
          : onChange as dynamic Function(String),
      value: value == freezed ? _value.value : value as String,
    ));
  }
}

/// @nodoc
abstract class _$InputParamsCopyWith<$Res>
    implements $InputParamsCopyWith<$Res> {
  factory _$InputParamsCopyWith(
          _InputParams value, $Res Function(_InputParams) then) =
      __$InputParamsCopyWithImpl<$Res>;
  @override
  $Res call(
      {String placeholder,
      String initialValue,
      bool canSubmit,
      bool clearOnSubmit,
      bool obscureText,
      dynamic Function() onSubmit,
      dynamic Function(String) onChange,
      String value});
}

/// @nodoc
class __$InputParamsCopyWithImpl<$Res> extends _$InputParamsCopyWithImpl<$Res>
    implements _$InputParamsCopyWith<$Res> {
  __$InputParamsCopyWithImpl(
      _InputParams _value, $Res Function(_InputParams) _then)
      : super(_value, (v) => _then(v as _InputParams));

  @override
  _InputParams get _value => super._value as _InputParams;

  @override
  $Res call({
    Object placeholder = freezed,
    Object initialValue = freezed,
    Object canSubmit = freezed,
    Object clearOnSubmit = freezed,
    Object obscureText = freezed,
    Object onSubmit = freezed,
    Object onChange = freezed,
    Object value = freezed,
  }) {
    return _then(_InputParams(
      placeholder:
          placeholder == freezed ? _value.placeholder : placeholder as String,
      initialValue: initialValue == freezed
          ? _value.initialValue
          : initialValue as String,
      canSubmit: canSubmit == freezed ? _value.canSubmit : canSubmit as bool,
      clearOnSubmit: clearOnSubmit == freezed
          ? _value.clearOnSubmit
          : clearOnSubmit as bool,
      obscureText:
          obscureText == freezed ? _value.obscureText : obscureText as bool,
      onSubmit: onSubmit == freezed
          ? _value.onSubmit
          : onSubmit as dynamic Function(),
      onChange: onChange == freezed
          ? _value.onChange
          : onChange as dynamic Function(String),
      value: value == freezed ? _value.value : value as String,
    ));
  }
}

/// @nodoc
class _$_InputParams implements _InputParams {
  _$_InputParams(
      {this.placeholder = '',
      this.initialValue = '',
      this.canSubmit = true,
      this.clearOnSubmit = false,
      this.obscureText = false,
      this.onSubmit,
      this.onChange,
      this.value})
      : assert(placeholder != null),
        assert(initialValue != null),
        assert(canSubmit != null),
        assert(clearOnSubmit != null),
        assert(obscureText != null);

  @JsonKey(defaultValue: '')
  @override
  final String placeholder;
  @JsonKey(defaultValue: '')
  @override
  final String initialValue;
  @JsonKey(defaultValue: true)
  @override
  final bool canSubmit;
  @JsonKey(defaultValue: false)
  @override
  final bool clearOnSubmit;
  @JsonKey(defaultValue: false)
  @override
  final bool obscureText;
  @override
  final dynamic Function() onSubmit;
  @override
  final dynamic Function(String) onChange;
  @override
  final String value;

  @override
  String toString() {
    return 'InputParams(placeholder: $placeholder, initialValue: $initialValue, canSubmit: $canSubmit, clearOnSubmit: $clearOnSubmit, obscureText: $obscureText, onSubmit: $onSubmit, onChange: $onChange, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _InputParams &&
            (identical(other.placeholder, placeholder) ||
                const DeepCollectionEquality()
                    .equals(other.placeholder, placeholder)) &&
            (identical(other.initialValue, initialValue) ||
                const DeepCollectionEquality()
                    .equals(other.initialValue, initialValue)) &&
            (identical(other.canSubmit, canSubmit) ||
                const DeepCollectionEquality()
                    .equals(other.canSubmit, canSubmit)) &&
            (identical(other.clearOnSubmit, clearOnSubmit) ||
                const DeepCollectionEquality()
                    .equals(other.clearOnSubmit, clearOnSubmit)) &&
            (identical(other.obscureText, obscureText) ||
                const DeepCollectionEquality()
                    .equals(other.obscureText, obscureText)) &&
            (identical(other.onSubmit, onSubmit) ||
                const DeepCollectionEquality()
                    .equals(other.onSubmit, onSubmit)) &&
            (identical(other.onChange, onChange) ||
                const DeepCollectionEquality()
                    .equals(other.onChange, onChange)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(placeholder) ^
      const DeepCollectionEquality().hash(initialValue) ^
      const DeepCollectionEquality().hash(canSubmit) ^
      const DeepCollectionEquality().hash(clearOnSubmit) ^
      const DeepCollectionEquality().hash(obscureText) ^
      const DeepCollectionEquality().hash(onSubmit) ^
      const DeepCollectionEquality().hash(onChange) ^
      const DeepCollectionEquality().hash(value);

  @override
  _$InputParamsCopyWith<_InputParams> get copyWith =>
      __$InputParamsCopyWithImpl<_InputParams>(this, _$identity);
}

abstract class _InputParams implements InputParams {
  factory _InputParams(
      {String placeholder,
      String initialValue,
      bool canSubmit,
      bool clearOnSubmit,
      bool obscureText,
      dynamic Function() onSubmit,
      dynamic Function(String) onChange,
      String value}) = _$_InputParams;

  @override
  String get placeholder;
  @override
  String get initialValue;
  @override
  bool get canSubmit;
  @override
  bool get clearOnSubmit;
  @override
  bool get obscureText;
  @override
  dynamic Function() get onSubmit;
  @override
  dynamic Function(String) get onChange;
  @override
  String get value;
  @override
  _$InputParamsCopyWith<_InputParams> get copyWith;
}
