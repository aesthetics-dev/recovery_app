// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'section.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Section _$_$_SectionFromJson(Map<String, dynamic> json) {
  return _$_Section(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    beds: (json['beds'] as List)
            ?.map((e) =>
                e == null ? null : Bed.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
  );
}

Map<String, dynamic> _$_$_SectionToJson(_$_Section instance) =>
    <String, dynamic>{
      'ref': instance.ref?.toJson(),
      'beds': instance.beds?.map((e) => e?.toJson())?.toList(),
    };
