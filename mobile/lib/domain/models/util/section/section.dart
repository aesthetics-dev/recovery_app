import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:recovery_app/domain/models/data/bed/bed.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'section.freezed.dart';
part 'section.g.dart';

@freezed
abstract class Section with _$Section {
  @JsonSerializable(explicitToJson: true)
  factory Section({
    @required Reference ref,
    @Default([]) List<Bed> beds,
  }) = _Section;

  factory Section.fromJson(Map<String, dynamic> json) =>
      _$SectionFromJson(json);
}
