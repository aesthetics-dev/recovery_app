// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'section.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Section _$SectionFromJson(Map<String, dynamic> json) {
  return _Section.fromJson(json);
}

/// @nodoc
class _$SectionTearOff {
  const _$SectionTearOff();

// ignore: unused_element
  _Section call({@required Reference ref, List<Bed> beds = const []}) {
    return _Section(
      ref: ref,
      beds: beds,
    );
  }

// ignore: unused_element
  Section fromJson(Map<String, Object> json) {
    return Section.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Section = _$SectionTearOff();

/// @nodoc
mixin _$Section {
  Reference get ref;
  List<Bed> get beds;

  Map<String, dynamic> toJson();
  $SectionCopyWith<Section> get copyWith;
}

/// @nodoc
abstract class $SectionCopyWith<$Res> {
  factory $SectionCopyWith(Section value, $Res Function(Section) then) =
      _$SectionCopyWithImpl<$Res>;
  $Res call({Reference ref, List<Bed> beds});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$SectionCopyWithImpl<$Res> implements $SectionCopyWith<$Res> {
  _$SectionCopyWithImpl(this._value, this._then);

  final Section _value;
  // ignore: unused_field
  final $Res Function(Section) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object beds = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      beds: beds == freezed ? _value.beds : beds as List<Bed>,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$SectionCopyWith<$Res> implements $SectionCopyWith<$Res> {
  factory _$SectionCopyWith(_Section value, $Res Function(_Section) then) =
      __$SectionCopyWithImpl<$Res>;
  @override
  $Res call({Reference ref, List<Bed> beds});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$SectionCopyWithImpl<$Res> extends _$SectionCopyWithImpl<$Res>
    implements _$SectionCopyWith<$Res> {
  __$SectionCopyWithImpl(_Section _value, $Res Function(_Section) _then)
      : super(_value, (v) => _then(v as _Section));

  @override
  _Section get _value => super._value as _Section;

  @override
  $Res call({
    Object ref = freezed,
    Object beds = freezed,
  }) {
    return _then(_Section(
      ref: ref == freezed ? _value.ref : ref as Reference,
      beds: beds == freezed ? _value.beds : beds as List<Bed>,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Section implements _Section {
  _$_Section({@required this.ref, this.beds = const []})
      : assert(ref != null),
        assert(beds != null);

  factory _$_Section.fromJson(Map<String, dynamic> json) =>
      _$_$_SectionFromJson(json);

  @override
  final Reference ref;
  @JsonKey(defaultValue: const [])
  @override
  final List<Bed> beds;

  @override
  String toString() {
    return 'Section(ref: $ref, beds: $beds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Section &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.beds, beds) ||
                const DeepCollectionEquality().equals(other.beds, beds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(beds);

  @override
  _$SectionCopyWith<_Section> get copyWith =>
      __$SectionCopyWithImpl<_Section>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SectionToJson(this);
  }
}

abstract class _Section implements Section {
  factory _Section({@required Reference ref, List<Bed> beds}) = _$_Section;

  factory _Section.fromJson(Map<String, dynamic> json) = _$_Section.fromJson;

  @override
  Reference get ref;
  @override
  List<Bed> get beds;
  @override
  _$SectionCopyWith<_Section> get copyWith;
}
