import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'location_task_average.freezed.dart';
part 'location_task_average.g.dart';

@freezed
abstract class LocationTaskAverage implements _$LocationTaskAverage {
  factory LocationTaskAverage({
    Reference locRef,
    int secondsToComplete,
  }) = _LocationTaskAverage;

  const LocationTaskAverage._();



  factory LocationTaskAverage.fromJson(Map<String, dynamic> json) =>
      _$LocationTaskAverageFromJson(json);
}
