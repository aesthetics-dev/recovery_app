// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'location_task_average.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
LocationTaskAverage _$LocationTaskAverageFromJson(Map<String, dynamic> json) {
  return _LocationTaskAverage.fromJson(json);
}

/// @nodoc
class _$LocationTaskAverageTearOff {
  const _$LocationTaskAverageTearOff();

// ignore: unused_element
  _LocationTaskAverage call({Reference locRef, int secondsToComplete}) {
    return _LocationTaskAverage(
      locRef: locRef,
      secondsToComplete: secondsToComplete,
    );
  }

// ignore: unused_element
  LocationTaskAverage fromJson(Map<String, Object> json) {
    return LocationTaskAverage.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $LocationTaskAverage = _$LocationTaskAverageTearOff();

/// @nodoc
mixin _$LocationTaskAverage {
  Reference get locRef;
  int get secondsToComplete;

  Map<String, dynamic> toJson();
  $LocationTaskAverageCopyWith<LocationTaskAverage> get copyWith;
}

/// @nodoc
abstract class $LocationTaskAverageCopyWith<$Res> {
  factory $LocationTaskAverageCopyWith(
          LocationTaskAverage value, $Res Function(LocationTaskAverage) then) =
      _$LocationTaskAverageCopyWithImpl<$Res>;
  $Res call({Reference locRef, int secondsToComplete});

  $ReferenceCopyWith<$Res> get locRef;
}

/// @nodoc
class _$LocationTaskAverageCopyWithImpl<$Res>
    implements $LocationTaskAverageCopyWith<$Res> {
  _$LocationTaskAverageCopyWithImpl(this._value, this._then);

  final LocationTaskAverage _value;
  // ignore: unused_field
  final $Res Function(LocationTaskAverage) _then;

  @override
  $Res call({
    Object locRef = freezed,
    Object secondsToComplete = freezed,
  }) {
    return _then(_value.copyWith(
      locRef: locRef == freezed ? _value.locRef : locRef as Reference,
      secondsToComplete: secondsToComplete == freezed
          ? _value.secondsToComplete
          : secondsToComplete as int,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get locRef {
    if (_value.locRef == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.locRef, (value) {
      return _then(_value.copyWith(locRef: value));
    });
  }
}

/// @nodoc
abstract class _$LocationTaskAverageCopyWith<$Res>
    implements $LocationTaskAverageCopyWith<$Res> {
  factory _$LocationTaskAverageCopyWith(_LocationTaskAverage value,
          $Res Function(_LocationTaskAverage) then) =
      __$LocationTaskAverageCopyWithImpl<$Res>;
  @override
  $Res call({Reference locRef, int secondsToComplete});

  @override
  $ReferenceCopyWith<$Res> get locRef;
}

/// @nodoc
class __$LocationTaskAverageCopyWithImpl<$Res>
    extends _$LocationTaskAverageCopyWithImpl<$Res>
    implements _$LocationTaskAverageCopyWith<$Res> {
  __$LocationTaskAverageCopyWithImpl(
      _LocationTaskAverage _value, $Res Function(_LocationTaskAverage) _then)
      : super(_value, (v) => _then(v as _LocationTaskAverage));

  @override
  _LocationTaskAverage get _value => super._value as _LocationTaskAverage;

  @override
  $Res call({
    Object locRef = freezed,
    Object secondsToComplete = freezed,
  }) {
    return _then(_LocationTaskAverage(
      locRef: locRef == freezed ? _value.locRef : locRef as Reference,
      secondsToComplete: secondsToComplete == freezed
          ? _value.secondsToComplete
          : secondsToComplete as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_LocationTaskAverage extends _LocationTaskAverage {
  _$_LocationTaskAverage({this.locRef, this.secondsToComplete}) : super._();

  factory _$_LocationTaskAverage.fromJson(Map<String, dynamic> json) =>
      _$_$_LocationTaskAverageFromJson(json);

  @override
  final Reference locRef;
  @override
  final int secondsToComplete;

  @override
  String toString() {
    return 'LocationTaskAverage(locRef: $locRef, secondsToComplete: $secondsToComplete)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LocationTaskAverage &&
            (identical(other.locRef, locRef) ||
                const DeepCollectionEquality().equals(other.locRef, locRef)) &&
            (identical(other.secondsToComplete, secondsToComplete) ||
                const DeepCollectionEquality()
                    .equals(other.secondsToComplete, secondsToComplete)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(locRef) ^
      const DeepCollectionEquality().hash(secondsToComplete);

  @override
  _$LocationTaskAverageCopyWith<_LocationTaskAverage> get copyWith =>
      __$LocationTaskAverageCopyWithImpl<_LocationTaskAverage>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LocationTaskAverageToJson(this);
  }
}

abstract class _LocationTaskAverage extends LocationTaskAverage {
  _LocationTaskAverage._() : super._();
  factory _LocationTaskAverage({Reference locRef, int secondsToComplete}) =
      _$_LocationTaskAverage;

  factory _LocationTaskAverage.fromJson(Map<String, dynamic> json) =
      _$_LocationTaskAverage.fromJson;

  @override
  Reference get locRef;
  @override
  int get secondsToComplete;
  @override
  _$LocationTaskAverageCopyWith<_LocationTaskAverage> get copyWith;
}
