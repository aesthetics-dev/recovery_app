// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_task_average.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LocationTaskAverage _$_$_LocationTaskAverageFromJson(
    Map<String, dynamic> json) {
  return _$_LocationTaskAverage(
    locRef: json['locRef'] == null
        ? null
        : Reference.fromJson(json['locRef'] as Map<String, dynamic>),
    secondsToComplete: json['secondsToComplete'] as int,
  );
}

Map<String, dynamic> _$_$_LocationTaskAverageToJson(
        _$_LocationTaskAverage instance) =>
    <String, dynamic>{
      'locRef': instance.locRef,
      'secondsToComplete': instance.secondsToComplete,
    };
