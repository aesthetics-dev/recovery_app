// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'location.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Location _$LocationFromJson(Map<String, dynamic> json) {
  return _Location.fromJson(json);
}

/// @nodoc
class _$LocationTearOff {
  const _$LocationTearOff();

// ignore: unused_element
  _Location call(
      {@required Reference ref,
      List<String> tags = const [],
      List<String> mdTags = const [],
      List<Bed> beds = const []}) {
    return _Location(
      ref: ref,
      tags: tags,
      mdTags: mdTags,
      beds: beds,
    );
  }

// ignore: unused_element
  Location fromJson(Map<String, Object> json) {
    return Location.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Location = _$LocationTearOff();

/// @nodoc
mixin _$Location {
  Reference get ref;
  List<String> get tags;
  List<String> get mdTags;
  List<Bed> get beds;

  Map<String, dynamic> toJson();
  $LocationCopyWith<Location> get copyWith;
}

/// @nodoc
abstract class $LocationCopyWith<$Res> {
  factory $LocationCopyWith(Location value, $Res Function(Location) then) =
      _$LocationCopyWithImpl<$Res>;
  $Res call(
      {Reference ref, List<String> tags, List<String> mdTags, List<Bed> beds});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$LocationCopyWithImpl<$Res> implements $LocationCopyWith<$Res> {
  _$LocationCopyWithImpl(this._value, this._then);

  final Location _value;
  // ignore: unused_field
  final $Res Function(Location) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object tags = freezed,
    Object mdTags = freezed,
    Object beds = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      tags: tags == freezed ? _value.tags : tags as List<String>,
      mdTags: mdTags == freezed ? _value.mdTags : mdTags as List<String>,
      beds: beds == freezed ? _value.beds : beds as List<Bed>,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$LocationCopyWith<$Res> implements $LocationCopyWith<$Res> {
  factory _$LocationCopyWith(_Location value, $Res Function(_Location) then) =
      __$LocationCopyWithImpl<$Res>;
  @override
  $Res call(
      {Reference ref, List<String> tags, List<String> mdTags, List<Bed> beds});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$LocationCopyWithImpl<$Res> extends _$LocationCopyWithImpl<$Res>
    implements _$LocationCopyWith<$Res> {
  __$LocationCopyWithImpl(_Location _value, $Res Function(_Location) _then)
      : super(_value, (v) => _then(v as _Location));

  @override
  _Location get _value => super._value as _Location;

  @override
  $Res call({
    Object ref = freezed,
    Object tags = freezed,
    Object mdTags = freezed,
    Object beds = freezed,
  }) {
    return _then(_Location(
      ref: ref == freezed ? _value.ref : ref as Reference,
      tags: tags == freezed ? _value.tags : tags as List<String>,
      mdTags: mdTags == freezed ? _value.mdTags : mdTags as List<String>,
      beds: beds == freezed ? _value.beds : beds as List<Bed>,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Location with DiagnosticableTreeMixin implements _Location {
  _$_Location(
      {@required this.ref,
      this.tags = const [],
      this.mdTags = const [],
      this.beds = const []})
      : assert(ref != null),
        assert(tags != null),
        assert(mdTags != null),
        assert(beds != null);

  factory _$_Location.fromJson(Map<String, dynamic> json) =>
      _$_$_LocationFromJson(json);

  @override
  final Reference ref;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> tags;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> mdTags;
  @JsonKey(defaultValue: const [])
  @override
  final List<Bed> beds;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Location(ref: $ref, tags: $tags, mdTags: $mdTags, beds: $beds)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Location'))
      ..add(DiagnosticsProperty('ref', ref))
      ..add(DiagnosticsProperty('tags', tags))
      ..add(DiagnosticsProperty('mdTags', mdTags))
      ..add(DiagnosticsProperty('beds', beds));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Location &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.tags, tags) ||
                const DeepCollectionEquality().equals(other.tags, tags)) &&
            (identical(other.mdTags, mdTags) ||
                const DeepCollectionEquality().equals(other.mdTags, mdTags)) &&
            (identical(other.beds, beds) ||
                const DeepCollectionEquality().equals(other.beds, beds)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(tags) ^
      const DeepCollectionEquality().hash(mdTags) ^
      const DeepCollectionEquality().hash(beds);

  @override
  _$LocationCopyWith<_Location> get copyWith =>
      __$LocationCopyWithImpl<_Location>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LocationToJson(this);
  }
}

abstract class _Location implements Location {
  factory _Location(
      {@required Reference ref,
      List<String> tags,
      List<String> mdTags,
      List<Bed> beds}) = _$_Location;

  factory _Location.fromJson(Map<String, dynamic> json) = _$_Location.fromJson;

  @override
  Reference get ref;
  @override
  List<String> get tags;
  @override
  List<String> get mdTags;
  @override
  List<Bed> get beds;
  @override
  _$LocationCopyWith<_Location> get copyWith;
}
