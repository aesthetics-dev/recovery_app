// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Location _$_$_LocationFromJson(Map<String, dynamic> json) {
  return _$_Location(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    tags: (json['tags'] as List)?.map((e) => e as String)?.toList() ?? [],
    mdTags: (json['mdTags'] as List)?.map((e) => e as String)?.toList() ?? [],
    beds: (json['beds'] as List)
            ?.map((e) =>
                e == null ? null : Bed.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
  );
}

Map<String, dynamic> _$_$_LocationToJson(_$_Location instance) =>
    <String, dynamic>{
      'ref': instance.ref?.toJson(),
      'tags': instance.tags,
      'mdTags': instance.mdTags,
      'beds': instance.beds?.map((e) => e?.toJson())?.toList(),
    };
