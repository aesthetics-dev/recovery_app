import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:recovery_app/domain/models/data/bed/bed.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'location.freezed.dart';
part 'location.g.dart';

@freezed
abstract class Location with _$Location {
  @JsonSerializable(explicitToJson: true)
  factory Location({
    @required Reference ref,
    @Default([]) List<String> tags,
    @Default([]) List<String> mdTags,
    @Default([]) List<Bed> beds,
  }) = _Location;

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);
}
