// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'heartbeat.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Heartbeat _$HeartbeatFromJson(Map<String, dynamic> json) {
  return _Heartbeat.fromJson(json);
}

/// @nodoc
class _$HeartbeatTearOff {
  const _$HeartbeatTearOff();

// ignore: unused_element
  _Heartbeat call(
      {String clientVersion, String clientOs, String clientBuildNumber}) {
    return _Heartbeat(
      clientVersion: clientVersion,
      clientOs: clientOs,
      clientBuildNumber: clientBuildNumber,
    );
  }

// ignore: unused_element
  Heartbeat fromJson(Map<String, Object> json) {
    return Heartbeat.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Heartbeat = _$HeartbeatTearOff();

/// @nodoc
mixin _$Heartbeat {
  String get clientVersion;
  String get clientOs;
  String get clientBuildNumber;

  Map<String, dynamic> toJson();
  $HeartbeatCopyWith<Heartbeat> get copyWith;
}

/// @nodoc
abstract class $HeartbeatCopyWith<$Res> {
  factory $HeartbeatCopyWith(Heartbeat value, $Res Function(Heartbeat) then) =
      _$HeartbeatCopyWithImpl<$Res>;
  $Res call({String clientVersion, String clientOs, String clientBuildNumber});
}

/// @nodoc
class _$HeartbeatCopyWithImpl<$Res> implements $HeartbeatCopyWith<$Res> {
  _$HeartbeatCopyWithImpl(this._value, this._then);

  final Heartbeat _value;
  // ignore: unused_field
  final $Res Function(Heartbeat) _then;

  @override
  $Res call({
    Object clientVersion = freezed,
    Object clientOs = freezed,
    Object clientBuildNumber = freezed,
  }) {
    return _then(_value.copyWith(
      clientVersion: clientVersion == freezed
          ? _value.clientVersion
          : clientVersion as String,
      clientOs: clientOs == freezed ? _value.clientOs : clientOs as String,
      clientBuildNumber: clientBuildNumber == freezed
          ? _value.clientBuildNumber
          : clientBuildNumber as String,
    ));
  }
}

/// @nodoc
abstract class _$HeartbeatCopyWith<$Res> implements $HeartbeatCopyWith<$Res> {
  factory _$HeartbeatCopyWith(
          _Heartbeat value, $Res Function(_Heartbeat) then) =
      __$HeartbeatCopyWithImpl<$Res>;
  @override
  $Res call({String clientVersion, String clientOs, String clientBuildNumber});
}

/// @nodoc
class __$HeartbeatCopyWithImpl<$Res> extends _$HeartbeatCopyWithImpl<$Res>
    implements _$HeartbeatCopyWith<$Res> {
  __$HeartbeatCopyWithImpl(_Heartbeat _value, $Res Function(_Heartbeat) _then)
      : super(_value, (v) => _then(v as _Heartbeat));

  @override
  _Heartbeat get _value => super._value as _Heartbeat;

  @override
  $Res call({
    Object clientVersion = freezed,
    Object clientOs = freezed,
    Object clientBuildNumber = freezed,
  }) {
    return _then(_Heartbeat(
      clientVersion: clientVersion == freezed
          ? _value.clientVersion
          : clientVersion as String,
      clientOs: clientOs == freezed ? _value.clientOs : clientOs as String,
      clientBuildNumber: clientBuildNumber == freezed
          ? _value.clientBuildNumber
          : clientBuildNumber as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Heartbeat implements _Heartbeat {
  _$_Heartbeat({this.clientVersion, this.clientOs, this.clientBuildNumber});

  factory _$_Heartbeat.fromJson(Map<String, dynamic> json) =>
      _$_$_HeartbeatFromJson(json);

  @override
  final String clientVersion;
  @override
  final String clientOs;
  @override
  final String clientBuildNumber;

  @override
  String toString() {
    return 'Heartbeat(clientVersion: $clientVersion, clientOs: $clientOs, clientBuildNumber: $clientBuildNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Heartbeat &&
            (identical(other.clientVersion, clientVersion) ||
                const DeepCollectionEquality()
                    .equals(other.clientVersion, clientVersion)) &&
            (identical(other.clientOs, clientOs) ||
                const DeepCollectionEquality()
                    .equals(other.clientOs, clientOs)) &&
            (identical(other.clientBuildNumber, clientBuildNumber) ||
                const DeepCollectionEquality()
                    .equals(other.clientBuildNumber, clientBuildNumber)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(clientVersion) ^
      const DeepCollectionEquality().hash(clientOs) ^
      const DeepCollectionEquality().hash(clientBuildNumber);

  @override
  _$HeartbeatCopyWith<_Heartbeat> get copyWith =>
      __$HeartbeatCopyWithImpl<_Heartbeat>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_HeartbeatToJson(this);
  }
}

abstract class _Heartbeat implements Heartbeat {
  factory _Heartbeat(
      {String clientVersion,
      String clientOs,
      String clientBuildNumber}) = _$_Heartbeat;

  factory _Heartbeat.fromJson(Map<String, dynamic> json) =
      _$_Heartbeat.fromJson;

  @override
  String get clientVersion;
  @override
  String get clientOs;
  @override
  String get clientBuildNumber;
  @override
  _$HeartbeatCopyWith<_Heartbeat> get copyWith;
}
