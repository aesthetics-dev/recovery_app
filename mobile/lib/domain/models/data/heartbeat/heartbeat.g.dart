// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'heartbeat.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Heartbeat _$_$_HeartbeatFromJson(Map<String, dynamic> json) {
  return _$_Heartbeat(
    clientVersion: json['clientVersion'] as String,
    clientOs: json['clientOs'] as String,
    clientBuildNumber: json['clientBuildNumber'] as String,
  );
}

Map<String, dynamic> _$_$_HeartbeatToJson(_$_Heartbeat instance) =>
    <String, dynamic>{
      'clientVersion': instance.clientVersion,
      'clientOs': instance.clientOs,
      'clientBuildNumber': instance.clientBuildNumber,
    };
