import 'package:freezed_annotation/freezed_annotation.dart';

part 'heartbeat.freezed.dart';
part 'heartbeat.g.dart';

@freezed
abstract class Heartbeat with _$Heartbeat {
  factory Heartbeat({
    String clientVersion,
    String clientOs,
    String clientBuildNumber,
  }) = _Heartbeat;

  factory Heartbeat.fromJson(Map<String, dynamic> json) =>
      _$HeartbeatFromJson(json);
}
