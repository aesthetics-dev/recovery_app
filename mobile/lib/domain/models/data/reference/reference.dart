import 'package:freezed_annotation/freezed_annotation.dart';

part 'reference.freezed.dart';
part 'reference.g.dart';

@freezed
abstract class Reference with _$Reference {
  @JsonSerializable()
  factory Reference({
    @Default('') String id,
    @Default('') String secondaryId,
    @Default('') String name,
  }) = _Reference;

  factory Reference.fromJson(Map<String, dynamic> json) =>
      _$ReferenceFromJson(json);
}
