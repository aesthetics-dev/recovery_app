// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reference.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Reference _$_$_ReferenceFromJson(Map<String, dynamic> json) {
  return _$_Reference(
    id: json['id'] as String ?? '',
    secondaryId: json['secondaryId'] as String ?? '',
    name: json['name'] as String ?? '',
  );
}

Map<String, dynamic> _$_$_ReferenceToJson(_$_Reference instance) =>
    <String, dynamic>{
      'id': instance.id,
      'secondaryId': instance.secondaryId,
      'name': instance.name,
    };
