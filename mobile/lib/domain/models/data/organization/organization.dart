import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:recovery_app/domain/models/data/location/location.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/data/role/role.dart';

part 'organization.freezed.dart';
part 'organization.g.dart';

@freezed
abstract class Organization implements _$Organization {
  @JsonSerializable(explicitToJson: true)
  factory Organization({
    @required Reference ref,
    @required String authType,
    @Default([]) List<Location> locations,
    @Default([]) List<Role> roles,
    @Default([]) List<String> emails,
    @Default(0) int utcOffset,
  }) = _Organization;

  Organization._();

  Location findLocation(String locID) =>
      locations.firstWhere((loc) => loc.ref.id == locID, orElse: () => null);

  factory Organization.fromJson(Map<String, dynamic> json) =>
      _$OrganizationFromJson(json);
}
