// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Organization _$_$_OrganizationFromJson(Map<String, dynamic> json) {
  return _$_Organization(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    authType: json['authType'] as String,
    locations: (json['locations'] as List)
            ?.map((e) =>
                e == null ? null : Location.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    roles: (json['roles'] as List)
            ?.map((e) =>
                e == null ? null : Role.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    emails: (json['emails'] as List)?.map((e) => e as String)?.toList() ?? [],
    utcOffset: json['utcOffset'] as int ?? 0,
  );
}

Map<String, dynamic> _$_$_OrganizationToJson(_$_Organization instance) =>
    <String, dynamic>{
      'ref': instance.ref?.toJson(),
      'authType': instance.authType,
      'locations': instance.locations?.map((e) => e?.toJson())?.toList(),
      'roles': instance.roles?.map((e) => e?.toJson())?.toList(),
      'emails': instance.emails,
      'utcOffset': instance.utcOffset,
    };
