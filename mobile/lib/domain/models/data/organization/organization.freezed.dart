// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'organization.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Organization _$OrganizationFromJson(Map<String, dynamic> json) {
  return _Organization.fromJson(json);
}

/// @nodoc
class _$OrganizationTearOff {
  const _$OrganizationTearOff();

// ignore: unused_element
  _Organization call(
      {@required Reference ref,
      @required String authType,
      List<Location> locations = const [],
      List<Role> roles = const [],
      List<String> emails = const [],
      int utcOffset = 0}) {
    return _Organization(
      ref: ref,
      authType: authType,
      locations: locations,
      roles: roles,
      emails: emails,
      utcOffset: utcOffset,
    );
  }

// ignore: unused_element
  Organization fromJson(Map<String, Object> json) {
    return Organization.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Organization = _$OrganizationTearOff();

/// @nodoc
mixin _$Organization {
  Reference get ref;
  String get authType;
  List<Location> get locations;
  List<Role> get roles;
  List<String> get emails;
  int get utcOffset;

  Map<String, dynamic> toJson();
  $OrganizationCopyWith<Organization> get copyWith;
}

/// @nodoc
abstract class $OrganizationCopyWith<$Res> {
  factory $OrganizationCopyWith(
          Organization value, $Res Function(Organization) then) =
      _$OrganizationCopyWithImpl<$Res>;
  $Res call(
      {Reference ref,
      String authType,
      List<Location> locations,
      List<Role> roles,
      List<String> emails,
      int utcOffset});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$OrganizationCopyWithImpl<$Res> implements $OrganizationCopyWith<$Res> {
  _$OrganizationCopyWithImpl(this._value, this._then);

  final Organization _value;
  // ignore: unused_field
  final $Res Function(Organization) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object authType = freezed,
    Object locations = freezed,
    Object roles = freezed,
    Object emails = freezed,
    Object utcOffset = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      authType: authType == freezed ? _value.authType : authType as String,
      locations:
          locations == freezed ? _value.locations : locations as List<Location>,
      roles: roles == freezed ? _value.roles : roles as List<Role>,
      emails: emails == freezed ? _value.emails : emails as List<String>,
      utcOffset: utcOffset == freezed ? _value.utcOffset : utcOffset as int,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$OrganizationCopyWith<$Res>
    implements $OrganizationCopyWith<$Res> {
  factory _$OrganizationCopyWith(
          _Organization value, $Res Function(_Organization) then) =
      __$OrganizationCopyWithImpl<$Res>;
  @override
  $Res call(
      {Reference ref,
      String authType,
      List<Location> locations,
      List<Role> roles,
      List<String> emails,
      int utcOffset});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$OrganizationCopyWithImpl<$Res> extends _$OrganizationCopyWithImpl<$Res>
    implements _$OrganizationCopyWith<$Res> {
  __$OrganizationCopyWithImpl(
      _Organization _value, $Res Function(_Organization) _then)
      : super(_value, (v) => _then(v as _Organization));

  @override
  _Organization get _value => super._value as _Organization;

  @override
  $Res call({
    Object ref = freezed,
    Object authType = freezed,
    Object locations = freezed,
    Object roles = freezed,
    Object emails = freezed,
    Object utcOffset = freezed,
  }) {
    return _then(_Organization(
      ref: ref == freezed ? _value.ref : ref as Reference,
      authType: authType == freezed ? _value.authType : authType as String,
      locations:
          locations == freezed ? _value.locations : locations as List<Location>,
      roles: roles == freezed ? _value.roles : roles as List<Role>,
      emails: emails == freezed ? _value.emails : emails as List<String>,
      utcOffset: utcOffset == freezed ? _value.utcOffset : utcOffset as int,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Organization extends _Organization with DiagnosticableTreeMixin {
  _$_Organization(
      {@required this.ref,
      @required this.authType,
      this.locations = const [],
      this.roles = const [],
      this.emails = const [],
      this.utcOffset = 0})
      : assert(ref != null),
        assert(authType != null),
        assert(locations != null),
        assert(roles != null),
        assert(emails != null),
        assert(utcOffset != null),
        super._();

  factory _$_Organization.fromJson(Map<String, dynamic> json) =>
      _$_$_OrganizationFromJson(json);

  @override
  final Reference ref;
  @override
  final String authType;
  @JsonKey(defaultValue: const [])
  @override
  final List<Location> locations;
  @JsonKey(defaultValue: const [])
  @override
  final List<Role> roles;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> emails;
  @JsonKey(defaultValue: 0)
  @override
  final int utcOffset;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Organization(ref: $ref, authType: $authType, locations: $locations, roles: $roles, emails: $emails, utcOffset: $utcOffset)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Organization'))
      ..add(DiagnosticsProperty('ref', ref))
      ..add(DiagnosticsProperty('authType', authType))
      ..add(DiagnosticsProperty('locations', locations))
      ..add(DiagnosticsProperty('roles', roles))
      ..add(DiagnosticsProperty('emails', emails))
      ..add(DiagnosticsProperty('utcOffset', utcOffset));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Organization &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.authType, authType) ||
                const DeepCollectionEquality()
                    .equals(other.authType, authType)) &&
            (identical(other.locations, locations) ||
                const DeepCollectionEquality()
                    .equals(other.locations, locations)) &&
            (identical(other.roles, roles) ||
                const DeepCollectionEquality().equals(other.roles, roles)) &&
            (identical(other.emails, emails) ||
                const DeepCollectionEquality().equals(other.emails, emails)) &&
            (identical(other.utcOffset, utcOffset) ||
                const DeepCollectionEquality()
                    .equals(other.utcOffset, utcOffset)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(authType) ^
      const DeepCollectionEquality().hash(locations) ^
      const DeepCollectionEquality().hash(roles) ^
      const DeepCollectionEquality().hash(emails) ^
      const DeepCollectionEquality().hash(utcOffset);

  @override
  _$OrganizationCopyWith<_Organization> get copyWith =>
      __$OrganizationCopyWithImpl<_Organization>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_OrganizationToJson(this);
  }
}

abstract class _Organization extends Organization {
  _Organization._() : super._();
  factory _Organization(
      {@required Reference ref,
      @required String authType,
      List<Location> locations,
      List<Role> roles,
      List<String> emails,
      int utcOffset}) = _$_Organization;

  factory _Organization.fromJson(Map<String, dynamic> json) =
      _$_Organization.fromJson;

  @override
  Reference get ref;
  @override
  String get authType;
  @override
  List<Location> get locations;
  @override
  List<Role> get roles;
  @override
  List<String> get emails;
  @override
  int get utcOffset;
  @override
  _$OrganizationCopyWith<_Organization> get copyWith;
}
