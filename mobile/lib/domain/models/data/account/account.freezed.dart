// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'account.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Account _$AccountFromJson(Map<String, dynamic> json) {
  return _Account.fromJson(json);
}

/// @nodoc
class _$AccountTearOff {
  const _$AccountTearOff();

// ignore: unused_element
  _Account call(
      {@required Reference ref,
      @required Reference orgRef,
      List<Reference> locRefs = const [],
      Role role,
      bool notificationsOn = true,
      List<Reference> followedBeds = const [],
      bool areVoiceAnnouncementsOn = false,
      bool isDisplayAlwaysOn = false,
      bool admin = false}) {
    return _Account(
      ref: ref,
      orgRef: orgRef,
      locRefs: locRefs,
      role: role,
      notificationsOn: notificationsOn,
      followedBeds: followedBeds,
      areVoiceAnnouncementsOn: areVoiceAnnouncementsOn,
      isDisplayAlwaysOn: isDisplayAlwaysOn,
      admin: admin,
    );
  }

// ignore: unused_element
  Account fromJson(Map<String, Object> json) {
    return Account.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Account = _$AccountTearOff();

/// @nodoc
mixin _$Account {
  Reference get ref;
  Reference get orgRef;
  List<Reference> get locRefs;
  Role get role;
  bool get notificationsOn;
  List<Reference> get followedBeds;
  bool get areVoiceAnnouncementsOn;
  bool get isDisplayAlwaysOn;
  bool get admin;

  Map<String, dynamic> toJson();
  $AccountCopyWith<Account> get copyWith;
}

/// @nodoc
abstract class $AccountCopyWith<$Res> {
  factory $AccountCopyWith(Account value, $Res Function(Account) then) =
      _$AccountCopyWithImpl<$Res>;
  $Res call(
      {Reference ref,
      Reference orgRef,
      List<Reference> locRefs,
      Role role,
      bool notificationsOn,
      List<Reference> followedBeds,
      bool areVoiceAnnouncementsOn,
      bool isDisplayAlwaysOn,
      bool admin});

  $ReferenceCopyWith<$Res> get ref;
  $ReferenceCopyWith<$Res> get orgRef;
  $RoleCopyWith<$Res> get role;
}

/// @nodoc
class _$AccountCopyWithImpl<$Res> implements $AccountCopyWith<$Res> {
  _$AccountCopyWithImpl(this._value, this._then);

  final Account _value;
  // ignore: unused_field
  final $Res Function(Account) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object orgRef = freezed,
    Object locRefs = freezed,
    Object role = freezed,
    Object notificationsOn = freezed,
    Object followedBeds = freezed,
    Object areVoiceAnnouncementsOn = freezed,
    Object isDisplayAlwaysOn = freezed,
    Object admin = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      orgRef: orgRef == freezed ? _value.orgRef : orgRef as Reference,
      locRefs: locRefs == freezed ? _value.locRefs : locRefs as List<Reference>,
      role: role == freezed ? _value.role : role as Role,
      notificationsOn: notificationsOn == freezed
          ? _value.notificationsOn
          : notificationsOn as bool,
      followedBeds: followedBeds == freezed
          ? _value.followedBeds
          : followedBeds as List<Reference>,
      areVoiceAnnouncementsOn: areVoiceAnnouncementsOn == freezed
          ? _value.areVoiceAnnouncementsOn
          : areVoiceAnnouncementsOn as bool,
      isDisplayAlwaysOn: isDisplayAlwaysOn == freezed
          ? _value.isDisplayAlwaysOn
          : isDisplayAlwaysOn as bool,
      admin: admin == freezed ? _value.admin : admin as bool,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }

  @override
  $ReferenceCopyWith<$Res> get orgRef {
    if (_value.orgRef == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.orgRef, (value) {
      return _then(_value.copyWith(orgRef: value));
    });
  }

  @override
  $RoleCopyWith<$Res> get role {
    if (_value.role == null) {
      return null;
    }
    return $RoleCopyWith<$Res>(_value.role, (value) {
      return _then(_value.copyWith(role: value));
    });
  }
}

/// @nodoc
abstract class _$AccountCopyWith<$Res> implements $AccountCopyWith<$Res> {
  factory _$AccountCopyWith(_Account value, $Res Function(_Account) then) =
      __$AccountCopyWithImpl<$Res>;
  @override
  $Res call(
      {Reference ref,
      Reference orgRef,
      List<Reference> locRefs,
      Role role,
      bool notificationsOn,
      List<Reference> followedBeds,
      bool areVoiceAnnouncementsOn,
      bool isDisplayAlwaysOn,
      bool admin});

  @override
  $ReferenceCopyWith<$Res> get ref;
  @override
  $ReferenceCopyWith<$Res> get orgRef;
  @override
  $RoleCopyWith<$Res> get role;
}

/// @nodoc
class __$AccountCopyWithImpl<$Res> extends _$AccountCopyWithImpl<$Res>
    implements _$AccountCopyWith<$Res> {
  __$AccountCopyWithImpl(_Account _value, $Res Function(_Account) _then)
      : super(_value, (v) => _then(v as _Account));

  @override
  _Account get _value => super._value as _Account;

  @override
  $Res call({
    Object ref = freezed,
    Object orgRef = freezed,
    Object locRefs = freezed,
    Object role = freezed,
    Object notificationsOn = freezed,
    Object followedBeds = freezed,
    Object areVoiceAnnouncementsOn = freezed,
    Object isDisplayAlwaysOn = freezed,
    Object admin = freezed,
  }) {
    return _then(_Account(
      ref: ref == freezed ? _value.ref : ref as Reference,
      orgRef: orgRef == freezed ? _value.orgRef : orgRef as Reference,
      locRefs: locRefs == freezed ? _value.locRefs : locRefs as List<Reference>,
      role: role == freezed ? _value.role : role as Role,
      notificationsOn: notificationsOn == freezed
          ? _value.notificationsOn
          : notificationsOn as bool,
      followedBeds: followedBeds == freezed
          ? _value.followedBeds
          : followedBeds as List<Reference>,
      areVoiceAnnouncementsOn: areVoiceAnnouncementsOn == freezed
          ? _value.areVoiceAnnouncementsOn
          : areVoiceAnnouncementsOn as bool,
      isDisplayAlwaysOn: isDisplayAlwaysOn == freezed
          ? _value.isDisplayAlwaysOn
          : isDisplayAlwaysOn as bool,
      admin: admin == freezed ? _value.admin : admin as bool,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Account extends _Account {
  _$_Account(
      {@required this.ref,
      @required this.orgRef,
      this.locRefs = const [],
      this.role,
      this.notificationsOn = true,
      this.followedBeds = const [],
      this.areVoiceAnnouncementsOn = false,
      this.isDisplayAlwaysOn = false,
      this.admin = false})
      : assert(ref != null),
        assert(orgRef != null),
        assert(locRefs != null),
        assert(notificationsOn != null),
        assert(followedBeds != null),
        assert(areVoiceAnnouncementsOn != null),
        assert(isDisplayAlwaysOn != null),
        assert(admin != null),
        super._();

  factory _$_Account.fromJson(Map<String, dynamic> json) =>
      _$_$_AccountFromJson(json);

  @override
  final Reference ref;
  @override
  final Reference orgRef;
  @JsonKey(defaultValue: const [])
  @override
  final List<Reference> locRefs;
  @override
  final Role role;
  @JsonKey(defaultValue: true)
  @override
  final bool notificationsOn;
  @JsonKey(defaultValue: const [])
  @override
  final List<Reference> followedBeds;
  @JsonKey(defaultValue: false)
  @override
  final bool areVoiceAnnouncementsOn;
  @JsonKey(defaultValue: false)
  @override
  final bool isDisplayAlwaysOn;
  @JsonKey(defaultValue: false)
  @override
  final bool admin;

  @override
  String toString() {
    return 'Account(ref: $ref, orgRef: $orgRef, locRefs: $locRefs, role: $role, notificationsOn: $notificationsOn, followedBeds: $followedBeds, areVoiceAnnouncementsOn: $areVoiceAnnouncementsOn, isDisplayAlwaysOn: $isDisplayAlwaysOn, admin: $admin)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Account &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.orgRef, orgRef) ||
                const DeepCollectionEquality().equals(other.orgRef, orgRef)) &&
            (identical(other.locRefs, locRefs) ||
                const DeepCollectionEquality()
                    .equals(other.locRefs, locRefs)) &&
            (identical(other.role, role) ||
                const DeepCollectionEquality().equals(other.role, role)) &&
            (identical(other.notificationsOn, notificationsOn) ||
                const DeepCollectionEquality()
                    .equals(other.notificationsOn, notificationsOn)) &&
            (identical(other.followedBeds, followedBeds) ||
                const DeepCollectionEquality()
                    .equals(other.followedBeds, followedBeds)) &&
            (identical(
                    other.areVoiceAnnouncementsOn, areVoiceAnnouncementsOn) ||
                const DeepCollectionEquality().equals(
                    other.areVoiceAnnouncementsOn, areVoiceAnnouncementsOn)) &&
            (identical(other.isDisplayAlwaysOn, isDisplayAlwaysOn) ||
                const DeepCollectionEquality()
                    .equals(other.isDisplayAlwaysOn, isDisplayAlwaysOn)) &&
            (identical(other.admin, admin) ||
                const DeepCollectionEquality().equals(other.admin, admin)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(orgRef) ^
      const DeepCollectionEquality().hash(locRefs) ^
      const DeepCollectionEquality().hash(role) ^
      const DeepCollectionEquality().hash(notificationsOn) ^
      const DeepCollectionEquality().hash(followedBeds) ^
      const DeepCollectionEquality().hash(areVoiceAnnouncementsOn) ^
      const DeepCollectionEquality().hash(isDisplayAlwaysOn) ^
      const DeepCollectionEquality().hash(admin);

  @override
  _$AccountCopyWith<_Account> get copyWith =>
      __$AccountCopyWithImpl<_Account>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_AccountToJson(this);
  }
}

abstract class _Account extends Account {
  _Account._() : super._();
  factory _Account(
      {@required Reference ref,
      @required Reference orgRef,
      List<Reference> locRefs,
      Role role,
      bool notificationsOn,
      List<Reference> followedBeds,
      bool areVoiceAnnouncementsOn,
      bool isDisplayAlwaysOn,
      bool admin}) = _$_Account;

  factory _Account.fromJson(Map<String, dynamic> json) = _$_Account.fromJson;

  @override
  Reference get ref;
  @override
  Reference get orgRef;
  @override
  List<Reference> get locRefs;
  @override
  Role get role;
  @override
  bool get notificationsOn;
  @override
  List<Reference> get followedBeds;
  @override
  bool get areVoiceAnnouncementsOn;
  @override
  bool get isDisplayAlwaysOn;
  @override
  bool get admin;
  @override
  _$AccountCopyWith<_Account> get copyWith;
}
