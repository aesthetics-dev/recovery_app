import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/data/role/role.dart';

part 'account.freezed.dart';
part 'account.g.dart';

@freezed
abstract class Account with _$Account {
  @JsonSerializable(explicitToJson: true)
  factory Account({
    @required Reference ref,
    @required Reference orgRef,
    @Default([]) List<Reference> locRefs,
    Role role,
    @Default(true) bool notificationsOn,
    @Default([]) List<Reference> followedBeds,
    @Default(false) bool areVoiceAnnouncementsOn,
    @Default(false) bool isDisplayAlwaysOn,
    @Default(false) bool admin,
  }) = _Account;

  Account._();

  bool get needsRegistration =>
      locRefs.isEmpty || ref.name.isEmpty || role == null;

  bool get isNurse => role?.type == RoleTypes.poster;

  factory Account.fromJson(Map<String, dynamic> json) =>
      _$AccountFromJson(json);
}
