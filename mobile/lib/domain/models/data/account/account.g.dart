// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Account _$_$_AccountFromJson(Map<String, dynamic> json) {
  return _$_Account(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    orgRef: json['orgRef'] == null
        ? null
        : Reference.fromJson(json['orgRef'] as Map<String, dynamic>),
    locRefs: (json['locRefs'] as List)
            ?.map((e) => e == null
                ? null
                : Reference.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    role: json['role'] == null
        ? null
        : Role.fromJson(json['role'] as Map<String, dynamic>),
    notificationsOn: json['notificationsOn'] as bool ?? true,
    followedBeds: (json['followedBeds'] as List)
            ?.map((e) => e == null
                ? null
                : Reference.fromJson(e as Map<String, dynamic>))
            ?.toList() ??
        [],
    areVoiceAnnouncementsOn: json['areVoiceAnnouncementsOn'] as bool ?? false,
    isDisplayAlwaysOn: json['isDisplayAlwaysOn'] as bool ?? false,
    admin: json['admin'] as bool ?? false,
  );
}

Map<String, dynamic> _$_$_AccountToJson(_$_Account instance) =>
    <String, dynamic>{
      'ref': instance.ref?.toJson(),
      'orgRef': instance.orgRef?.toJson(),
      'locRefs': instance.locRefs?.map((e) => e?.toJson())?.toList(),
      'role': instance.role?.toJson(),
      'notificationsOn': instance.notificationsOn,
      'followedBeds': instance.followedBeds?.map((e) => e?.toJson())?.toList(),
      'areVoiceAnnouncementsOn': instance.areVoiceAnnouncementsOn,
      'isDisplayAlwaysOn': instance.isDisplayAlwaysOn,
      'admin': instance.admin,
    };
