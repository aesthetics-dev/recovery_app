// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'abridged_organization.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
AbridgedOrganization _$AbridgedOrganizationFromJson(Map<String, dynamic> json) {
  return _AbridgedOrganization.fromJson(json);
}

/// @nodoc
class _$AbridgedOrganizationTearOff {
  const _$AbridgedOrganizationTearOff();

// ignore: unused_element
  _AbridgedOrganization call(
      {@required Reference ref, String authType = AuthType.email}) {
    return _AbridgedOrganization(
      ref: ref,
      authType: authType,
    );
  }

// ignore: unused_element
  AbridgedOrganization fromJson(Map<String, Object> json) {
    return AbridgedOrganization.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $AbridgedOrganization = _$AbridgedOrganizationTearOff();

/// @nodoc
mixin _$AbridgedOrganization {
  Reference get ref;
  String get authType;

  Map<String, dynamic> toJson();
  $AbridgedOrganizationCopyWith<AbridgedOrganization> get copyWith;
}

/// @nodoc
abstract class $AbridgedOrganizationCopyWith<$Res> {
  factory $AbridgedOrganizationCopyWith(AbridgedOrganization value,
          $Res Function(AbridgedOrganization) then) =
      _$AbridgedOrganizationCopyWithImpl<$Res>;
  $Res call({Reference ref, String authType});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$AbridgedOrganizationCopyWithImpl<$Res>
    implements $AbridgedOrganizationCopyWith<$Res> {
  _$AbridgedOrganizationCopyWithImpl(this._value, this._then);

  final AbridgedOrganization _value;
  // ignore: unused_field
  final $Res Function(AbridgedOrganization) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object authType = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      authType: authType == freezed ? _value.authType : authType as String,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$AbridgedOrganizationCopyWith<$Res>
    implements $AbridgedOrganizationCopyWith<$Res> {
  factory _$AbridgedOrganizationCopyWith(_AbridgedOrganization value,
          $Res Function(_AbridgedOrganization) then) =
      __$AbridgedOrganizationCopyWithImpl<$Res>;
  @override
  $Res call({Reference ref, String authType});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$AbridgedOrganizationCopyWithImpl<$Res>
    extends _$AbridgedOrganizationCopyWithImpl<$Res>
    implements _$AbridgedOrganizationCopyWith<$Res> {
  __$AbridgedOrganizationCopyWithImpl(
      _AbridgedOrganization _value, $Res Function(_AbridgedOrganization) _then)
      : super(_value, (v) => _then(v as _AbridgedOrganization));

  @override
  _AbridgedOrganization get _value => super._value as _AbridgedOrganization;

  @override
  $Res call({
    Object ref = freezed,
    Object authType = freezed,
  }) {
    return _then(_AbridgedOrganization(
      ref: ref == freezed ? _value.ref : ref as Reference,
      authType: authType == freezed ? _value.authType : authType as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_AbridgedOrganization implements _AbridgedOrganization {
  _$_AbridgedOrganization({@required this.ref, this.authType = AuthType.email})
      : assert(ref != null),
        assert(authType != null);

  factory _$_AbridgedOrganization.fromJson(Map<String, dynamic> json) =>
      _$_$_AbridgedOrganizationFromJson(json);

  @override
  final Reference ref;
  @JsonKey(defaultValue: AuthType.email)
  @override
  final String authType;

  @override
  String toString() {
    return 'AbridgedOrganization(ref: $ref, authType: $authType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AbridgedOrganization &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.authType, authType) ||
                const DeepCollectionEquality()
                    .equals(other.authType, authType)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(authType);

  @override
  _$AbridgedOrganizationCopyWith<_AbridgedOrganization> get copyWith =>
      __$AbridgedOrganizationCopyWithImpl<_AbridgedOrganization>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_AbridgedOrganizationToJson(this);
  }
}

abstract class _AbridgedOrganization implements AbridgedOrganization {
  factory _AbridgedOrganization({@required Reference ref, String authType}) =
      _$_AbridgedOrganization;

  factory _AbridgedOrganization.fromJson(Map<String, dynamic> json) =
      _$_AbridgedOrganization.fromJson;

  @override
  Reference get ref;
  @override
  String get authType;
  @override
  _$AbridgedOrganizationCopyWith<_AbridgedOrganization> get copyWith;
}
