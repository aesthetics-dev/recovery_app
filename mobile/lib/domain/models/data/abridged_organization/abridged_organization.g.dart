// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'abridged_organization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AbridgedOrganization _$_$_AbridgedOrganizationFromJson(
    Map<String, dynamic> json) {
  return _$_AbridgedOrganization(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    authType: json['authType'] as String ?? 'email',
  );
}

Map<String, dynamic> _$_$_AbridgedOrganizationToJson(
        _$_AbridgedOrganization instance) =>
    <String, dynamic>{
      'ref': instance.ref,
      'authType': instance.authType,
    };
