import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'abridged_organization.freezed.dart';
part 'abridged_organization.g.dart';

@freezed
abstract class AbridgedOrganization with _$AbridgedOrganization {
  factory AbridgedOrganization({
    @required Reference ref,
    @Default(AuthType.email) String authType,
  }) = _AbridgedOrganization;

  factory AbridgedOrganization.fromJson(Map<String, dynamic> json) =>
      _$AbridgedOrganizationFromJson(json);
}

abstract class AuthType {
  static const String email = 'email';
  static const String password = 'password';
}
