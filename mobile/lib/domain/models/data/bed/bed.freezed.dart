// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'bed.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Bed _$BedFromJson(Map<String, dynamic> json) {
  return _Bed.fromJson(json);
}

/// @nodoc
class _$BedTearOff {
  const _$BedTearOff();

// ignore: unused_element
  _Bed call({@required Reference ref}) {
    return _Bed(
      ref: ref,
    );
  }

// ignore: unused_element
  Bed fromJson(Map<String, Object> json) {
    return Bed.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Bed = _$BedTearOff();

/// @nodoc
mixin _$Bed {
  Reference get ref;

  Map<String, dynamic> toJson();
  $BedCopyWith<Bed> get copyWith;
}

/// @nodoc
abstract class $BedCopyWith<$Res> {
  factory $BedCopyWith(Bed value, $Res Function(Bed) then) =
      _$BedCopyWithImpl<$Res>;
  $Res call({Reference ref});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$BedCopyWithImpl<$Res> implements $BedCopyWith<$Res> {
  _$BedCopyWithImpl(this._value, this._then);

  final Bed _value;
  // ignore: unused_field
  final $Res Function(Bed) _then;

  @override
  $Res call({
    Object ref = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$BedCopyWith<$Res> implements $BedCopyWith<$Res> {
  factory _$BedCopyWith(_Bed value, $Res Function(_Bed) then) =
      __$BedCopyWithImpl<$Res>;
  @override
  $Res call({Reference ref});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$BedCopyWithImpl<$Res> extends _$BedCopyWithImpl<$Res>
    implements _$BedCopyWith<$Res> {
  __$BedCopyWithImpl(_Bed _value, $Res Function(_Bed) _then)
      : super(_value, (v) => _then(v as _Bed));

  @override
  _Bed get _value => super._value as _Bed;

  @override
  $Res call({
    Object ref = freezed,
  }) {
    return _then(_Bed(
      ref: ref == freezed ? _value.ref : ref as Reference,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Bed with DiagnosticableTreeMixin implements _Bed {
  _$_Bed({@required this.ref}) : assert(ref != null);

  factory _$_Bed.fromJson(Map<String, dynamic> json) => _$_$_BedFromJson(json);

  @override
  final Reference ref;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Bed(ref: $ref)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Bed'))
      ..add(DiagnosticsProperty('ref', ref));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Bed &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(ref);

  @override
  _$BedCopyWith<_Bed> get copyWith =>
      __$BedCopyWithImpl<_Bed>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_BedToJson(this);
  }
}

abstract class _Bed implements Bed {
  factory _Bed({@required Reference ref}) = _$_Bed;

  factory _Bed.fromJson(Map<String, dynamic> json) = _$_Bed.fromJson;

  @override
  Reference get ref;
  @override
  _$BedCopyWith<_Bed> get copyWith;
}
