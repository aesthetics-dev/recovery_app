import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'bed.freezed.dart';
part 'bed.g.dart';

@freezed
abstract class Bed with _$Bed {
  @JsonSerializable(explicitToJson: true)
  factory Bed({@required Reference ref}) = _Bed;

  factory Bed.fromJson(Map<String, dynamic> json) => _$BedFromJson(json);
}
