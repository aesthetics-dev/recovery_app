// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bed.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Bed _$_$_BedFromJson(Map<String, dynamic> json) {
  return _$_Bed(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_BedToJson(_$_Bed instance) => <String, dynamic>{
      'ref': instance.ref?.toJson(),
    };
