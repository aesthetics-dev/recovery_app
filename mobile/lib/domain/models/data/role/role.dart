import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'role.freezed.dart';
part 'role.g.dart';

@freezed
abstract class Role with _$Role {
  factory Role({
    @required Reference ref,
    @required int type,
    @Default(true) bool editable,
  }) = _Role;

  factory Role.fromJson(Map<String, dynamic> json) => _$RoleFromJson(json);
}

abstract class RoleTypes {
  static const completer = 0;
  static const poster = 1;
}
