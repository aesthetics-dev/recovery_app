// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'role.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Role _$_$_RoleFromJson(Map<String, dynamic> json) {
  return _$_Role(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    type: json['type'] as int,
    editable: json['editable'] as bool ?? true,
  );
}

Map<String, dynamic> _$_$_RoleToJson(_$_Role instance) => <String, dynamic>{
      'ref': instance.ref,
      'type': instance.type,
      'editable': instance.editable,
    };
