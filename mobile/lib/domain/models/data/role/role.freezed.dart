// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'role.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Role _$RoleFromJson(Map<String, dynamic> json) {
  return _Role.fromJson(json);
}

/// @nodoc
class _$RoleTearOff {
  const _$RoleTearOff();

// ignore: unused_element
  _Role call(
      {@required Reference ref, @required int type, bool editable = true}) {
    return _Role(
      ref: ref,
      type: type,
      editable: editable,
    );
  }

// ignore: unused_element
  Role fromJson(Map<String, Object> json) {
    return Role.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Role = _$RoleTearOff();

/// @nodoc
mixin _$Role {
  Reference get ref;
  int get type;
  bool get editable;

  Map<String, dynamic> toJson();
  $RoleCopyWith<Role> get copyWith;
}

/// @nodoc
abstract class $RoleCopyWith<$Res> {
  factory $RoleCopyWith(Role value, $Res Function(Role) then) =
      _$RoleCopyWithImpl<$Res>;
  $Res call({Reference ref, int type, bool editable});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$RoleCopyWithImpl<$Res> implements $RoleCopyWith<$Res> {
  _$RoleCopyWithImpl(this._value, this._then);

  final Role _value;
  // ignore: unused_field
  final $Res Function(Role) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object type = freezed,
    Object editable = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      type: type == freezed ? _value.type : type as int,
      editable: editable == freezed ? _value.editable : editable as bool,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$RoleCopyWith<$Res> implements $RoleCopyWith<$Res> {
  factory _$RoleCopyWith(_Role value, $Res Function(_Role) then) =
      __$RoleCopyWithImpl<$Res>;
  @override
  $Res call({Reference ref, int type, bool editable});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$RoleCopyWithImpl<$Res> extends _$RoleCopyWithImpl<$Res>
    implements _$RoleCopyWith<$Res> {
  __$RoleCopyWithImpl(_Role _value, $Res Function(_Role) _then)
      : super(_value, (v) => _then(v as _Role));

  @override
  _Role get _value => super._value as _Role;

  @override
  $Res call({
    Object ref = freezed,
    Object type = freezed,
    Object editable = freezed,
  }) {
    return _then(_Role(
      ref: ref == freezed ? _value.ref : ref as Reference,
      type: type == freezed ? _value.type : type as int,
      editable: editable == freezed ? _value.editable : editable as bool,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Role implements _Role {
  _$_Role({@required this.ref, @required this.type, this.editable = true})
      : assert(ref != null),
        assert(type != null),
        assert(editable != null);

  factory _$_Role.fromJson(Map<String, dynamic> json) =>
      _$_$_RoleFromJson(json);

  @override
  final Reference ref;
  @override
  final int type;
  @JsonKey(defaultValue: true)
  @override
  final bool editable;

  @override
  String toString() {
    return 'Role(ref: $ref, type: $type, editable: $editable)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Role &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.editable, editable) ||
                const DeepCollectionEquality()
                    .equals(other.editable, editable)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(editable);

  @override
  _$RoleCopyWith<_Role> get copyWith =>
      __$RoleCopyWithImpl<_Role>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_RoleToJson(this);
  }
}

abstract class _Role implements Role {
  factory _Role({@required Reference ref, @required int type, bool editable}) =
      _$_Role;

  factory _Role.fromJson(Map<String, dynamic> json) = _$_Role.fromJson;

  @override
  Reference get ref;
  @override
  int get type;
  @override
  bool get editable;
  @override
  _$RoleCopyWith<_Role> get copyWith;
}
