// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'task.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Task _$TaskFromJson(Map<String, dynamic> json) {
  return _Task.fromJson(json);
}

/// @nodoc
class _$TaskTearOff {
  const _$TaskTearOff();

// ignore: unused_element
  _Task call(
      {Reference orgRef,
      Reference locRef,
      Reference bedRef,
      List<String> tags = const [],
      List<String> mdTags = const [],
      String customTag = '',
      String customMDTag = '',
      Footprint mdETA,
      Footprint created,
      Footprint removed}) {
    return _Task(
      orgRef: orgRef,
      locRef: locRef,
      bedRef: bedRef,
      tags: tags,
      mdTags: mdTags,
      customTag: customTag,
      customMDTag: customMDTag,
      mdETA: mdETA,
      created: created,
      removed: removed,
    );
  }

// ignore: unused_element
  Task fromJson(Map<String, Object> json) {
    return Task.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Task = _$TaskTearOff();

/// @nodoc
mixin _$Task {
  Reference get orgRef;
  Reference get locRef;
  Reference get bedRef;
  List<String> get tags;
  List<String> get mdTags;
  String get customTag;
  String get customMDTag;
  Footprint get mdETA;
  Footprint get created;
  Footprint get removed;

  Map<String, dynamic> toJson();
  $TaskCopyWith<Task> get copyWith;
}

/// @nodoc
abstract class $TaskCopyWith<$Res> {
  factory $TaskCopyWith(Task value, $Res Function(Task) then) =
      _$TaskCopyWithImpl<$Res>;
  $Res call(
      {Reference orgRef,
      Reference locRef,
      Reference bedRef,
      List<String> tags,
      List<String> mdTags,
      String customTag,
      String customMDTag,
      Footprint mdETA,
      Footprint created,
      Footprint removed});

  $ReferenceCopyWith<$Res> get orgRef;
  $ReferenceCopyWith<$Res> get locRef;
  $ReferenceCopyWith<$Res> get bedRef;
  $FootprintCopyWith<$Res> get mdETA;
  $FootprintCopyWith<$Res> get created;
  $FootprintCopyWith<$Res> get removed;
}

/// @nodoc
class _$TaskCopyWithImpl<$Res> implements $TaskCopyWith<$Res> {
  _$TaskCopyWithImpl(this._value, this._then);

  final Task _value;
  // ignore: unused_field
  final $Res Function(Task) _then;

  @override
  $Res call({
    Object orgRef = freezed,
    Object locRef = freezed,
    Object bedRef = freezed,
    Object tags = freezed,
    Object mdTags = freezed,
    Object customTag = freezed,
    Object customMDTag = freezed,
    Object mdETA = freezed,
    Object created = freezed,
    Object removed = freezed,
  }) {
    return _then(_value.copyWith(
      orgRef: orgRef == freezed ? _value.orgRef : orgRef as Reference,
      locRef: locRef == freezed ? _value.locRef : locRef as Reference,
      bedRef: bedRef == freezed ? _value.bedRef : bedRef as Reference,
      tags: tags == freezed ? _value.tags : tags as List<String>,
      mdTags: mdTags == freezed ? _value.mdTags : mdTags as List<String>,
      customTag: customTag == freezed ? _value.customTag : customTag as String,
      customMDTag:
          customMDTag == freezed ? _value.customMDTag : customMDTag as String,
      mdETA: mdETA == freezed ? _value.mdETA : mdETA as Footprint,
      created: created == freezed ? _value.created : created as Footprint,
      removed: removed == freezed ? _value.removed : removed as Footprint,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get orgRef {
    if (_value.orgRef == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.orgRef, (value) {
      return _then(_value.copyWith(orgRef: value));
    });
  }

  @override
  $ReferenceCopyWith<$Res> get locRef {
    if (_value.locRef == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.locRef, (value) {
      return _then(_value.copyWith(locRef: value));
    });
  }

  @override
  $ReferenceCopyWith<$Res> get bedRef {
    if (_value.bedRef == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.bedRef, (value) {
      return _then(_value.copyWith(bedRef: value));
    });
  }

  @override
  $FootprintCopyWith<$Res> get mdETA {
    if (_value.mdETA == null) {
      return null;
    }
    return $FootprintCopyWith<$Res>(_value.mdETA, (value) {
      return _then(_value.copyWith(mdETA: value));
    });
  }

  @override
  $FootprintCopyWith<$Res> get created {
    if (_value.created == null) {
      return null;
    }
    return $FootprintCopyWith<$Res>(_value.created, (value) {
      return _then(_value.copyWith(created: value));
    });
  }

  @override
  $FootprintCopyWith<$Res> get removed {
    if (_value.removed == null) {
      return null;
    }
    return $FootprintCopyWith<$Res>(_value.removed, (value) {
      return _then(_value.copyWith(removed: value));
    });
  }
}

/// @nodoc
abstract class _$TaskCopyWith<$Res> implements $TaskCopyWith<$Res> {
  factory _$TaskCopyWith(_Task value, $Res Function(_Task) then) =
      __$TaskCopyWithImpl<$Res>;
  @override
  $Res call(
      {Reference orgRef,
      Reference locRef,
      Reference bedRef,
      List<String> tags,
      List<String> mdTags,
      String customTag,
      String customMDTag,
      Footprint mdETA,
      Footprint created,
      Footprint removed});

  @override
  $ReferenceCopyWith<$Res> get orgRef;
  @override
  $ReferenceCopyWith<$Res> get locRef;
  @override
  $ReferenceCopyWith<$Res> get bedRef;
  @override
  $FootprintCopyWith<$Res> get mdETA;
  @override
  $FootprintCopyWith<$Res> get created;
  @override
  $FootprintCopyWith<$Res> get removed;
}

/// @nodoc
class __$TaskCopyWithImpl<$Res> extends _$TaskCopyWithImpl<$Res>
    implements _$TaskCopyWith<$Res> {
  __$TaskCopyWithImpl(_Task _value, $Res Function(_Task) _then)
      : super(_value, (v) => _then(v as _Task));

  @override
  _Task get _value => super._value as _Task;

  @override
  $Res call({
    Object orgRef = freezed,
    Object locRef = freezed,
    Object bedRef = freezed,
    Object tags = freezed,
    Object mdTags = freezed,
    Object customTag = freezed,
    Object customMDTag = freezed,
    Object mdETA = freezed,
    Object created = freezed,
    Object removed = freezed,
  }) {
    return _then(_Task(
      orgRef: orgRef == freezed ? _value.orgRef : orgRef as Reference,
      locRef: locRef == freezed ? _value.locRef : locRef as Reference,
      bedRef: bedRef == freezed ? _value.bedRef : bedRef as Reference,
      tags: tags == freezed ? _value.tags : tags as List<String>,
      mdTags: mdTags == freezed ? _value.mdTags : mdTags as List<String>,
      customTag: customTag == freezed ? _value.customTag : customTag as String,
      customMDTag:
          customMDTag == freezed ? _value.customMDTag : customMDTag as String,
      mdETA: mdETA == freezed ? _value.mdETA : mdETA as Footprint,
      created: created == freezed ? _value.created : created as Footprint,
      removed: removed == freezed ? _value.removed : removed as Footprint,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Task extends _Task {
  _$_Task(
      {this.orgRef,
      this.locRef,
      this.bedRef,
      this.tags = const [],
      this.mdTags = const [],
      this.customTag = '',
      this.customMDTag = '',
      this.mdETA,
      this.created,
      this.removed})
      : assert(tags != null),
        assert(mdTags != null),
        assert(customTag != null),
        assert(customMDTag != null),
        super._();

  factory _$_Task.fromJson(Map<String, dynamic> json) =>
      _$_$_TaskFromJson(json);

  @override
  final Reference orgRef;
  @override
  final Reference locRef;
  @override
  final Reference bedRef;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> tags;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> mdTags;
  @JsonKey(defaultValue: '')
  @override
  final String customTag;
  @JsonKey(defaultValue: '')
  @override
  final String customMDTag;
  @override
  final Footprint mdETA;
  @override
  final Footprint created;
  @override
  final Footprint removed;

  @override
  String toString() {
    return 'Task(orgRef: $orgRef, locRef: $locRef, bedRef: $bedRef, tags: $tags, mdTags: $mdTags, customTag: $customTag, customMDTag: $customMDTag, mdETA: $mdETA, created: $created, removed: $removed)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Task &&
            (identical(other.orgRef, orgRef) ||
                const DeepCollectionEquality().equals(other.orgRef, orgRef)) &&
            (identical(other.locRef, locRef) ||
                const DeepCollectionEquality().equals(other.locRef, locRef)) &&
            (identical(other.bedRef, bedRef) ||
                const DeepCollectionEquality().equals(other.bedRef, bedRef)) &&
            (identical(other.tags, tags) ||
                const DeepCollectionEquality().equals(other.tags, tags)) &&
            (identical(other.mdTags, mdTags) ||
                const DeepCollectionEquality().equals(other.mdTags, mdTags)) &&
            (identical(other.customTag, customTag) ||
                const DeepCollectionEquality()
                    .equals(other.customTag, customTag)) &&
            (identical(other.customMDTag, customMDTag) ||
                const DeepCollectionEquality()
                    .equals(other.customMDTag, customMDTag)) &&
            (identical(other.mdETA, mdETA) ||
                const DeepCollectionEquality().equals(other.mdETA, mdETA)) &&
            (identical(other.created, created) ||
                const DeepCollectionEquality()
                    .equals(other.created, created)) &&
            (identical(other.removed, removed) ||
                const DeepCollectionEquality().equals(other.removed, removed)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(orgRef) ^
      const DeepCollectionEquality().hash(locRef) ^
      const DeepCollectionEquality().hash(bedRef) ^
      const DeepCollectionEquality().hash(tags) ^
      const DeepCollectionEquality().hash(mdTags) ^
      const DeepCollectionEquality().hash(customTag) ^
      const DeepCollectionEquality().hash(customMDTag) ^
      const DeepCollectionEquality().hash(mdETA) ^
      const DeepCollectionEquality().hash(created) ^
      const DeepCollectionEquality().hash(removed);

  @override
  _$TaskCopyWith<_Task> get copyWith =>
      __$TaskCopyWithImpl<_Task>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_TaskToJson(this);
  }
}

abstract class _Task extends Task {
  _Task._() : super._();
  factory _Task(
      {Reference orgRef,
      Reference locRef,
      Reference bedRef,
      List<String> tags,
      List<String> mdTags,
      String customTag,
      String customMDTag,
      Footprint mdETA,
      Footprint created,
      Footprint removed}) = _$_Task;

  factory _Task.fromJson(Map<String, dynamic> json) = _$_Task.fromJson;

  @override
  Reference get orgRef;
  @override
  Reference get locRef;
  @override
  Reference get bedRef;
  @override
  List<String> get tags;
  @override
  List<String> get mdTags;
  @override
  String get customTag;
  @override
  String get customMDTag;
  @override
  Footprint get mdETA;
  @override
  Footprint get created;
  @override
  Footprint get removed;
  @override
  _$TaskCopyWith<_Task> get copyWith;
}
