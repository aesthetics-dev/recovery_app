// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Task _$_$_TaskFromJson(Map<String, dynamic> json) {
  return _$_Task(
    orgRef: json['orgRef'] == null
        ? null
        : Reference.fromJson(json['orgRef'] as Map<String, dynamic>),
    locRef: json['locRef'] == null
        ? null
        : Reference.fromJson(json['locRef'] as Map<String, dynamic>),
    bedRef: json['bedRef'] == null
        ? null
        : Reference.fromJson(json['bedRef'] as Map<String, dynamic>),
    tags: (json['tags'] as List)?.map((e) => e as String)?.toList() ?? [],
    mdTags: (json['mdTags'] as List)?.map((e) => e as String)?.toList() ?? [],
    customTag: json['customTag'] as String ?? '',
    customMDTag: json['customMDTag'] as String ?? '',
    mdETA: json['mdETA'] == null
        ? null
        : Footprint.fromJson(json['mdETA'] as Map<String, dynamic>),
    created: json['created'] == null
        ? null
        : Footprint.fromJson(json['created'] as Map<String, dynamic>),
    removed: json['removed'] == null
        ? null
        : Footprint.fromJson(json['removed'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_TaskToJson(_$_Task instance) => <String, dynamic>{
      'orgRef': instance.orgRef?.toJson(),
      'locRef': instance.locRef?.toJson(),
      'bedRef': instance.bedRef?.toJson(),
      'tags': instance.tags,
      'mdTags': instance.mdTags,
      'customTag': instance.customTag,
      'customMDTag': instance.customMDTag,
      'mdETA': instance.mdETA?.toJson(),
      'created': instance.created?.toJson(),
      'removed': instance.removed?.toJson(),
    };
