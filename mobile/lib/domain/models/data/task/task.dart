import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/models/data/footprint/footprint.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'task.freezed.dart';

part 'task.g.dart';

@freezed
abstract class Task implements _$Task {
  @JsonSerializable(explicitToJson: true)
  factory Task({
    Reference orgRef,
    Reference locRef,
    Reference bedRef,
    @Default([]) List<String> tags,
    @Default([]) List<String> mdTags,
    @Default('') String customTag,
    @Default('') String customMDTag,
    Footprint mdETA,
    Footprint created,
    Footprint removed,
  }) = _Task;

  const Task._();

  List<String> get allTags {
    final out = [...tags];
    return customTag.isEmpty ? out : [customTag, ...out];
  }

  List<String> get allMdTags {
    final out = [...mdTags];
    return customMDTag.isEmpty ? out : [customMDTag, ...out];
  }

  bool get isRecent {
    return created.timestamp
        .isAfter(DateTime.now().subtract(Duration(seconds: 5)));
  }

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
}
