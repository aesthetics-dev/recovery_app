// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'footprint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Footprint _$_$_FootprintFromJson(Map<String, dynamic> json) {
  return _$_Footprint(
    ref: json['ref'] == null
        ? null
        : Reference.fromJson(json['ref'] as Map<String, dynamic>),
    timestamp: DateTimeConverters.dateFromJson(json['timestamp'] as String),
  );
}

Map<String, dynamic> _$_$_FootprintToJson(_$_Footprint instance) =>
    <String, dynamic>{
      'ref': instance.ref?.toJson(),
      'timestamp': DateTimeConverters.dateToJson(instance.timestamp),
    };
