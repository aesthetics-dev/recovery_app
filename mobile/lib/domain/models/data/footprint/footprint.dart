import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:recovery_app/domain/core/utils/date_time_converters.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';

part 'footprint.freezed.dart';

part 'footprint.g.dart';

@freezed
abstract class Footprint with _$Footprint {
  @JsonSerializable(explicitToJson: true)
  factory Footprint({
    Reference ref,
    @JsonKey(
      fromJson: DateTimeConverters.dateFromJson,
      toJson: DateTimeConverters.dateToJson,
    )
        DateTime timestamp,
  }) = _Footprint;

  factory Footprint.fromJson(Map<String, dynamic> json) =>
      _$FootprintFromJson(json);
}
