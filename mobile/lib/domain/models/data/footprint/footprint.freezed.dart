// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'footprint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
Footprint _$FootprintFromJson(Map<String, dynamic> json) {
  return _Footprint.fromJson(json);
}

/// @nodoc
class _$FootprintTearOff {
  const _$FootprintTearOff();

// ignore: unused_element
  _Footprint call(
      {Reference ref,
      @JsonKey(fromJson: DateTimeConverters.dateFromJson, toJson: DateTimeConverters.dateToJson)
          DateTime timestamp}) {
    return _Footprint(
      ref: ref,
      timestamp: timestamp,
    );
  }

// ignore: unused_element
  Footprint fromJson(Map<String, Object> json) {
    return Footprint.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $Footprint = _$FootprintTearOff();

/// @nodoc
mixin _$Footprint {
  Reference get ref;
  @JsonKey(
    fromJson: DateTimeConverters.dateFromJson,
    toJson: DateTimeConverters.dateToJson,
  )
  DateTime get timestamp;

  Map<String, dynamic> toJson();
  $FootprintCopyWith<Footprint> get copyWith;
}

/// @nodoc
abstract class $FootprintCopyWith<$Res> {
  factory $FootprintCopyWith(Footprint value, $Res Function(Footprint) then) =
      _$FootprintCopyWithImpl<$Res>;
  $Res call(
      {Reference ref,
      @JsonKey(fromJson: DateTimeConverters.dateFromJson, toJson: DateTimeConverters.dateToJson)
          DateTime timestamp});

  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class _$FootprintCopyWithImpl<$Res> implements $FootprintCopyWith<$Res> {
  _$FootprintCopyWithImpl(this._value, this._then);

  final Footprint _value;
  // ignore: unused_field
  final $Res Function(Footprint) _then;

  @override
  $Res call({
    Object ref = freezed,
    Object timestamp = freezed,
  }) {
    return _then(_value.copyWith(
      ref: ref == freezed ? _value.ref : ref as Reference,
      timestamp:
          timestamp == freezed ? _value.timestamp : timestamp as DateTime,
    ));
  }

  @override
  $ReferenceCopyWith<$Res> get ref {
    if (_value.ref == null) {
      return null;
    }
    return $ReferenceCopyWith<$Res>(_value.ref, (value) {
      return _then(_value.copyWith(ref: value));
    });
  }
}

/// @nodoc
abstract class _$FootprintCopyWith<$Res> implements $FootprintCopyWith<$Res> {
  factory _$FootprintCopyWith(
          _Footprint value, $Res Function(_Footprint) then) =
      __$FootprintCopyWithImpl<$Res>;
  @override
  $Res call(
      {Reference ref,
      @JsonKey(fromJson: DateTimeConverters.dateFromJson, toJson: DateTimeConverters.dateToJson)
          DateTime timestamp});

  @override
  $ReferenceCopyWith<$Res> get ref;
}

/// @nodoc
class __$FootprintCopyWithImpl<$Res> extends _$FootprintCopyWithImpl<$Res>
    implements _$FootprintCopyWith<$Res> {
  __$FootprintCopyWithImpl(_Footprint _value, $Res Function(_Footprint) _then)
      : super(_value, (v) => _then(v as _Footprint));

  @override
  _Footprint get _value => super._value as _Footprint;

  @override
  $Res call({
    Object ref = freezed,
    Object timestamp = freezed,
  }) {
    return _then(_Footprint(
      ref: ref == freezed ? _value.ref : ref as Reference,
      timestamp:
          timestamp == freezed ? _value.timestamp : timestamp as DateTime,
    ));
  }
}

@JsonSerializable(explicitToJson: true)

/// @nodoc
class _$_Footprint with DiagnosticableTreeMixin implements _Footprint {
  _$_Footprint(
      {this.ref,
      @JsonKey(fromJson: DateTimeConverters.dateFromJson, toJson: DateTimeConverters.dateToJson)
          this.timestamp});

  factory _$_Footprint.fromJson(Map<String, dynamic> json) =>
      _$_$_FootprintFromJson(json);

  @override
  final Reference ref;
  @override
  @JsonKey(
      fromJson: DateTimeConverters.dateFromJson,
      toJson: DateTimeConverters.dateToJson)
  final DateTime timestamp;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Footprint(ref: $ref, timestamp: $timestamp)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Footprint'))
      ..add(DiagnosticsProperty('ref', ref))
      ..add(DiagnosticsProperty('timestamp', timestamp));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Footprint &&
            (identical(other.ref, ref) ||
                const DeepCollectionEquality().equals(other.ref, ref)) &&
            (identical(other.timestamp, timestamp) ||
                const DeepCollectionEquality()
                    .equals(other.timestamp, timestamp)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(ref) ^
      const DeepCollectionEquality().hash(timestamp);

  @override
  _$FootprintCopyWith<_Footprint> get copyWith =>
      __$FootprintCopyWithImpl<_Footprint>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FootprintToJson(this);
  }
}

abstract class _Footprint implements Footprint {
  factory _Footprint(
      {Reference ref,
      @JsonKey(fromJson: DateTimeConverters.dateFromJson, toJson: DateTimeConverters.dateToJson)
          DateTime timestamp}) = _$_Footprint;

  factory _Footprint.fromJson(Map<String, dynamic> json) =
      _$_Footprint.fromJson;

  @override
  Reference get ref;
  @override
  @JsonKey(
      fromJson: DateTimeConverters.dateFromJson,
      toJson: DateTimeConverters.dateToJson)
  DateTime get timestamp;
  @override
  _$FootprintCopyWith<_Footprint> get copyWith;
}
