// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'logout_trace.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
LogoutTrace _$LogoutTraceFromJson(Map<String, dynamic> json) {
  return _LogoutTrace.fromJson(json);
}

/// @nodoc
class _$LogoutTraceTearOff {
  const _$LogoutTraceTearOff();

// ignore: unused_element
  _LogoutTrace call({@required String logoutMethod}) {
    return _LogoutTrace(
      logoutMethod: logoutMethod,
    );
  }

// ignore: unused_element
  LogoutTrace fromJson(Map<String, Object> json) {
    return LogoutTrace.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $LogoutTrace = _$LogoutTraceTearOff();

/// @nodoc
mixin _$LogoutTrace {
  String get logoutMethod;

  Map<String, dynamic> toJson();
  $LogoutTraceCopyWith<LogoutTrace> get copyWith;
}

/// @nodoc
abstract class $LogoutTraceCopyWith<$Res> {
  factory $LogoutTraceCopyWith(
          LogoutTrace value, $Res Function(LogoutTrace) then) =
      _$LogoutTraceCopyWithImpl<$Res>;
  $Res call({String logoutMethod});
}

/// @nodoc
class _$LogoutTraceCopyWithImpl<$Res> implements $LogoutTraceCopyWith<$Res> {
  _$LogoutTraceCopyWithImpl(this._value, this._then);

  final LogoutTrace _value;
  // ignore: unused_field
  final $Res Function(LogoutTrace) _then;

  @override
  $Res call({
    Object logoutMethod = freezed,
  }) {
    return _then(_value.copyWith(
      logoutMethod: logoutMethod == freezed
          ? _value.logoutMethod
          : logoutMethod as String,
    ));
  }
}

/// @nodoc
abstract class _$LogoutTraceCopyWith<$Res>
    implements $LogoutTraceCopyWith<$Res> {
  factory _$LogoutTraceCopyWith(
          _LogoutTrace value, $Res Function(_LogoutTrace) then) =
      __$LogoutTraceCopyWithImpl<$Res>;
  @override
  $Res call({String logoutMethod});
}

/// @nodoc
class __$LogoutTraceCopyWithImpl<$Res> extends _$LogoutTraceCopyWithImpl<$Res>
    implements _$LogoutTraceCopyWith<$Res> {
  __$LogoutTraceCopyWithImpl(
      _LogoutTrace _value, $Res Function(_LogoutTrace) _then)
      : super(_value, (v) => _then(v as _LogoutTrace));

  @override
  _LogoutTrace get _value => super._value as _LogoutTrace;

  @override
  $Res call({
    Object logoutMethod = freezed,
  }) {
    return _then(_LogoutTrace(
      logoutMethod: logoutMethod == freezed
          ? _value.logoutMethod
          : logoutMethod as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_LogoutTrace implements _LogoutTrace {
  _$_LogoutTrace({@required this.logoutMethod}) : assert(logoutMethod != null);

  factory _$_LogoutTrace.fromJson(Map<String, dynamic> json) =>
      _$_$_LogoutTraceFromJson(json);

  @override
  final String logoutMethod;

  @override
  String toString() {
    return 'LogoutTrace(logoutMethod: $logoutMethod)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LogoutTrace &&
            (identical(other.logoutMethod, logoutMethod) ||
                const DeepCollectionEquality()
                    .equals(other.logoutMethod, logoutMethod)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(logoutMethod);

  @override
  _$LogoutTraceCopyWith<_LogoutTrace> get copyWith =>
      __$LogoutTraceCopyWithImpl<_LogoutTrace>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LogoutTraceToJson(this);
  }
}

abstract class _LogoutTrace implements LogoutTrace {
  factory _LogoutTrace({@required String logoutMethod}) = _$_LogoutTrace;

  factory _LogoutTrace.fromJson(Map<String, dynamic> json) =
      _$_LogoutTrace.fromJson;

  @override
  String get logoutMethod;
  @override
  _$LogoutTraceCopyWith<_LogoutTrace> get copyWith;
}
