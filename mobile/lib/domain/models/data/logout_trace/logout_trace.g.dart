// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logout_trace.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LogoutTrace _$_$_LogoutTraceFromJson(Map<String, dynamic> json) {
  return _$_LogoutTrace(
    logoutMethod: json['logoutMethod'] as String,
  );
}

Map<String, dynamic> _$_$_LogoutTraceToJson(_$_LogoutTrace instance) =>
    <String, dynamic>{
      'logoutMethod': instance.logoutMethod,
    };
