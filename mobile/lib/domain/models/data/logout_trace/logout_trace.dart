import 'package:freezed_annotation/freezed_annotation.dart';

part 'logout_trace.freezed.dart';
part 'logout_trace.g.dart';

@freezed
abstract class LogoutTrace with _$LogoutTrace {
  factory LogoutTrace({
    @required String logoutMethod,
  }) = _LogoutTrace;

  factory LogoutTrace.fromJson(Map<String, dynamic> json) =>
      _$LogoutTraceFromJson(json);
}
