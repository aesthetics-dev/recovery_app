import 'dart:io';

import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:recovery_app/domain/utilities/log.dart';

class UtilityFunctions {
  static Future<void> launchMail() => AppAvailability.launchApp(
          Platform.isIOS ? "message://" : "com.google.android.gm")
      .catchError((err) => Log.error('Couldnt launch Mail app: $err'));
}
