import 'package:flutter/material.dart';

abstract class NavUtils {
  static void popIfCurrent(BuildContext context, {bool rootNavigator = false}) {
    final nav = Navigator.of(context, rootNavigator: rootNavigator);
    final routeName = ModalRoute.of(context).settings.name;
    bool popped = false;
    nav.popUntil((route) {
      if (!popped && route.settings.name == routeName) {
        return false;
      }

      return popped = true;
    });
  }
}
