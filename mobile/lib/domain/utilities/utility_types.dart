typedef Mutator<T> = T Function(T val);

typedef TypeSwitch<F, T> = T Function(F val);

typedef Comparator<T> = bool Function(T a, T b);

typedef FromJson<T> = T Function(Map<String, dynamic> json);
