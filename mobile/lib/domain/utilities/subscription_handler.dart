import 'dart:async';

class SubscriptionHandler {
  final List<StreamSubscription> _subs = [];

  SubscriptionHandler operator +(StreamSubscription sub) =>
      this.._subs.add(sub);

  void close() => _subs.forEach((sub) => sub.cancel());
}
