import 'package:page_transition/page_transition.dart';

class NavTransitions {
  static const String topToBottom = 'topToBottom';
  static const String bottomToTop = 'bottomToTop';
  static const String leftToRight = 'leftToRight';
  static const String rightToLeft = 'rightToLeft';

  static const transitionMap = {
    topToBottom: PageTransitionType.topToBottom,
    bottomToTop: PageTransitionType.bottomToTop,
    leftToRight: PageTransitionType.leftToRight,
    rightToLeft: PageTransitionType.rightToLeft,
    null: PageTransitionType.fade
  };
}
