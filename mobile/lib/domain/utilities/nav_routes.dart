import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:recovery_app/presentation/screens/admin_screen/admin_screen.dart';
import 'package:recovery_app/presentation/screens/authentication/email_verification_screen.dart';
import 'package:recovery_app/presentation/screens/authentication/authentication_screen.dart';
import 'package:recovery_app/presentation/screens/home/main_screen.dart';
import 'package:recovery_app/presentation/screens/loading_screen/loading_screen.dart';
import 'package:recovery_app/presentation/screens/no_connection_screen/no_connection_screen.dart';
import 'package:recovery_app/presentation/screens/settings/settings_screen.dart';

class NavRoutes {
  static const String auth = "AuthenticationScreen";
  static const String emailVerification = "EmailVerificationScreen";
  static const String loading = "LoadingScreen";
  static const String settings = "SettingsScreen";
  static const String home = "HomeScreen";
  static const String noConnection = "NoConnectionScreen";
  static const String adminScreen = "adminScreen";

  static final Map<String, Widget Function()> routeMap = {
    auth: () => AuthenticationScreen(),
    emailVerification: () => EmailVerificationScreen(),
    loading: () => LoadingScreen(),
    settings: () => SettingsScreen(),
    home: () => MainScreen(),
    adminScreen: () => AdminScreen(),
    noConnection: () => NoConnectionScreen(),
  };

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final route = routeMap[settings.name];
    // final params = TransitionParams.fromJson(settings.arguments);
    return PageTransition(
      child: route(),
      type: PageTransitionType.fade,
      settings: settings,
      duration: Duration(milliseconds: 200),
    );
  }
}
