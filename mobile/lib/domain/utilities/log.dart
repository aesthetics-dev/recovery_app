abstract class Log {
  static String compile(
    String emoji, {
    String origin = 'unknown',
    bool withTrace = false,
  }) {
    if (withTrace) {
      // ignore: avoid_print
      print(StackTrace.current);
    }
    return '$emoji | $origin | ${DateTime.now()}';
  }

  static void prettyPrint(String val) {
    // ignore: avoid_print
    print("""$val\n---\n---""");
  }

  static void info(dynamic val, [String origin = 'unknown']) {
    final out = compile('💡', origin: origin);
    prettyPrint('$out\n$val');
  }

  static void error(dynamic val, [String origin = 'unknown']) {
    final out = compile('❗', origin: origin, withTrace: true);
    prettyPrint('$out\n$val');
  }

  static void warning(dynamic val, [String origin = 'unknown']) {
    final out = compile('⚠️', origin: origin);
    prettyPrint('$out\n$val');
  }

  static void wtf(dynamic val, [String origin = 'unknown']) {
    final out = compile('👽', origin: origin);
    prettyPrint('$out\n$val');
  }

  static void debug(dynamic val, [String origin = 'unknown']) {
    final out = compile('🐛', origin: origin);
    prettyPrint('$out\n$val');
  }
}
