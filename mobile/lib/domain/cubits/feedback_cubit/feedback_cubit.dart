import 'dart:math';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/util/message/message.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

part 'feedback_state.dart';

class FeedbackCubit extends Cubit<FeedbackState> {
  final logger = Logger();

  FeedbackCubit() : super(FeedbackState());
  BuildContext context;

  void blockLoading() {
    emit(state.copyWith(loadingBlocked: true));
  }

  void unblockLoading() {
    emit(state.copyWith(loadingBlocked: false));
  }

  void addMessage(Message message) {
    BotToast.showText(
      text: message.content,
      contentColor: _colorMap[message.type],
    );
  }

  void addFailure(String message) {
    addMessage(Message.error(message));
  }

  void addSuccess(String message) {
    addMessage(Message.success(message));
  }

  void clearLoading() {
    emit(state.copyWith(numLoading: 0));
  }

  void _addLoading() {
    emit(state.copyWith(numLoading: state.numLoading + 1));
  }

  void _removeLoading({int delayMilliseconds = 0}) {
    Future.delayed(Duration(milliseconds: delayMilliseconds), () {
      emit(state.copyWith(numLoading: max(state.numLoading - 1, 0)));
    });
  }

  Future<T> controlled<T>(
    Future<T> Function() func, {
    @required bool shouldRethrow,
    @required bool withLoading,
    @required bool displayError,
    T fallback,
    Message overrideError,
    Message fallbackError,
  }) async {
    try {
      if (withLoading) _addLoading();
      return await func();
    } catch (error) {
      if (error is! Message) logger.e(error);

      // decide on the message
      Message message;
      if (overrideError != null) {
        message = overrideError;
      } else if (error is Message) {
        message = error;
      } else if (fallbackError != null) {
        message = fallbackError;
      }

      // display error if specified
      if (displayError) {
        addMessage(message ?? Messages.error);
      }

      if (shouldRethrow) {
        throw message ?? error;
      } else {
        return fallback;
      }
    } finally {
      if (withLoading) _removeLoading();
    }
  }
}

Map<MessageType, Color> _colorMap = {
  MessageType.success: MyColors.success,
  MessageType.error: MyColors.error,
  MessageType.info: MyColors.info,
};
