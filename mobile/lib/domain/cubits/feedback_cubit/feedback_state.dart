part of 'feedback_cubit.dart';

@immutable
class FeedbackState extends Equatable {
  final Message message;
  final int numLoading;
  final bool loadingBlocked;

  const FeedbackState({
    this.message,
    this.numLoading = 0,
    this.loadingBlocked = false,
  });

  bool get loading => numLoading > 0 && !loadingBlocked;

  @override
  List<Object> get props => [message, numLoading, loadingBlocked];

  FeedbackState copyWith({
    Message message,
    int numLoading,
    bool loading,
    bool loadingBlocked,
  }) {
    return FeedbackState(
      message: message ?? this.message,
      numLoading: numLoading ?? this.numLoading,
      loadingBlocked: loadingBlocked ?? this.loadingBlocked,
    );
  }
}
