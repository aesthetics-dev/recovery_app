part of 'admin_cubit.dart';

@freezed
abstract class AdminState implements _$AdminState {
  factory AdminState({
    Organization organization,
  }) = _AdminState;

  const AdminState._();
}
