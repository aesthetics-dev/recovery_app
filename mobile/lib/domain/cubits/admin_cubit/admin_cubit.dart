import 'package:basic_utils/basic_utils.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/domain/core/utils/shortcuts.dart';
import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/data/bed/bed.dart';
import 'package:recovery_app/domain/models/data/location/location.dart';
import 'package:recovery_app/domain/models/data/organization/organization.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/data/role/role.dart';
import 'package:recovery_app/domain/services/organization_service.dart';
import 'package:recovery_app/domain/core/extensions/list_extensions.dart';

part 'admin_cubit.freezed.dart';

part 'admin_state.dart';

class AdminCubit extends Cubit<AdminState> {
  final CoreCubit coreCubit;
  final OrganizationService organizationService;
  final FeedbackCubit feedback;

  Organization get org => state.organization;

  AdminCubit({
    @required this.coreCubit,
    @required this.organizationService,
    @required this.feedback,
  }) : super(
          AdminState(organization: coreCubit.state.organization.copyWith()),
        );

  void changeOrganizationName(String newName) {
    emit(state.copyWith.organization.ref(name: newName));
  }

  Future<bool> submitOrganization() async {
    return feedback.controlled(
      () async {
        final locs = state.organization.locations;
        if (locs.any((l) => l.ref.name.isEmpty)) {
          feedback.addFailure('Location names cannot be empty.');
          return false;
        }
        final hasDuplicates =
            locs.map((l) => l.ref.name).toSet().length != locs.length;
        if (hasDuplicates) {
          feedback.addFailure('Location names have to be different.');
          return false;
        }

        if (org == coreCubit.state.organization) {
          return true;
        }

        await organizationService.updateOrganization(state.organization);
        return true;
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
  }

  void removeLocation(Reference locRef) {
    changeLocations((list) => removeListItem(
        list, org.locations.indexWhere((element) => element.ref == locRef)));
    feedback.addMessage(Messages.removed(locRef.name));
  }

  void changeLocationName(int index, String name) {
    changeLocations((list) => list
        .indexedMap((l, i) => i == index ? l.copyWith.ref(name: name) : l)
        .toList());
  }

  void removeRole(int index) {
    emit(state.copyWith.organization(roles: [...org.roles]..removeAt(index)));
  }

  void toggleRoleType(int index) {
    emit(state.copyWith.organization(
        roles: org.roles
            .indexedMap((r, i) => index == i
                ? r.copyWith(
                    type: r.type == RoleTypes.poster
                        ? RoleTypes.completer
                        : RoleTypes.poster)
                : r)
            .toList()));
  }

  void addRole(String role) {
    emit(state.copyWith.organization(roles: [
      ...state.organization.roles,
      Role(ref: Reference(name: role), type: 0)
    ]));
  }

  void addLocation(String name) {
    final isValid = _validateInput(
      name,
      org.locations.map((l) => l.ref.name).toList(),
    );

    if (!isValid) {
      return;
    }

    changeLocations(
      (list) => addListItem(list, Location(ref: Reference(name: name))),
    );
    feedback.addMessage(Messages.added(name));
  }

  void removeBed(Reference locRef, String bedName) {
    final ind = org
        .findLocation(locRef.id)
        .beds
        .indexWhere((b) => b.ref.name == bedName);

    changeBeds(locRef, (list) => removeListItem(list, ind));
    feedback.addMessage(Messages.removed(bedName));
  }

  void addBed(Reference locRef, String name) {
    final isValid = _validateInput(
      name,
      org.findLocation(locRef.id).beds.map((e) => e.ref.name).toList(),
    );
    if (!isValid) {
      return;
    }

    changeBeds(
      locRef,
      (list) => addListItem(list, Bed(ref: Reference(name: name))),
    );
    feedback.addMessage(Messages.added(name));
  }

  void removeTag(Reference locRef, String name, TagType tagType) {
    changeTags(
      locRef,
      (tags) => tags.where((t) => t != name).toList(),
      tagType,
    );
    feedback.addMessage(Messages.removed(name));
  }

  void setTags(Reference locRef, TagType type, List<String> tags) {
    changeTags(locRef, (list) => tags, type);
  }

  void addTag(Reference locRef, String name, TagType type) {
    final isValid = _validateInput(
      name,
      org.findLocation(locRef.id).tags,
    );

    if (!isValid) {
      return;
    }

    changeTags(locRef, (tags) => addListItem(tags, name), type);
    feedback.addMessage(Messages.added(name));
  }

  bool _validateInput(String value, List<String> values) {
    if (value.isEmpty) {
      feedback.addMessage(Messages.cantBeEmpty('Name'));
      return false;
    }

    if (values.contains(value)) {
      feedback.addMessage(Messages.alreadyExists('Name'));
      return false;
    }

    return true;
  }

  void changeLocations(Mapper<Location> mutator) {
    emit(state.copyWith
        .organization(locations: mutator(state.organization.locations)));
  }

  void changeUtcOffset(int val) {
    emit(state.copyWith.organization(utcOffset: val));
  }

  void changeBeds(Reference locRef, Mapper<Bed> mutator) {
    changeLocations(
      (locs) => locs
          .map((l) => l.ref == locRef ? l.copyWith(beds: mutator(l.beds)) : l)
          .toList(),
    );
  }

  void changeTags(
    Reference locRef,
    Mapper<String> mutator,
    TagType tagType,
  ) {
    changeLocations(
      (locs) => updateListItem(
        locs,
        org.locations.indexWhere((loc) => loc.ref == locRef),
        (loc) => loc.copyWith(
          tags: tagType == TagType.nurse ? mutator(loc.tags) : loc.tags,
          mdTags: tagType == TagType.md ? mutator(loc.mdTags) : loc.mdTags,
        ),
      ),
    );
  }

  void addEmail(String email) {
    if (!EmailUtils.isEmail(email)) {
      feedback.addFailure('Please enter a valid email');
      return;
    }
    final lowercase = email.toLowerCase();
    if (state.organization.emails.contains(lowercase)) {
      feedback.addFailure('$email already exists!');
      return;
    }

    emit(state.copyWith
        .organization(emails: [...state.organization.emails, lowercase]));

    feedback.addSuccess('$email added!');
  }

  void removeEmail(String email) {
    emit(
      state.copyWith.organization(
        emails: state.organization.emails
            .where((element) => element != email)
            .toList(),
      ),
    );

    feedback.addSuccess('$email removed!');
  }
}
