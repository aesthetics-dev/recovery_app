// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'admin_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AdminStateTearOff {
  const _$AdminStateTearOff();

// ignore: unused_element
  _AdminState call({Organization organization}) {
    return _AdminState(
      organization: organization,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AdminState = _$AdminStateTearOff();

/// @nodoc
mixin _$AdminState {
  Organization get organization;

  $AdminStateCopyWith<AdminState> get copyWith;
}

/// @nodoc
abstract class $AdminStateCopyWith<$Res> {
  factory $AdminStateCopyWith(
          AdminState value, $Res Function(AdminState) then) =
      _$AdminStateCopyWithImpl<$Res>;
  $Res call({Organization organization});

  $OrganizationCopyWith<$Res> get organization;
}

/// @nodoc
class _$AdminStateCopyWithImpl<$Res> implements $AdminStateCopyWith<$Res> {
  _$AdminStateCopyWithImpl(this._value, this._then);

  final AdminState _value;
  // ignore: unused_field
  final $Res Function(AdminState) _then;

  @override
  $Res call({
    Object organization = freezed,
  }) {
    return _then(_value.copyWith(
      organization: organization == freezed
          ? _value.organization
          : organization as Organization,
    ));
  }

  @override
  $OrganizationCopyWith<$Res> get organization {
    if (_value.organization == null) {
      return null;
    }
    return $OrganizationCopyWith<$Res>(_value.organization, (value) {
      return _then(_value.copyWith(organization: value));
    });
  }
}

/// @nodoc
abstract class _$AdminStateCopyWith<$Res> implements $AdminStateCopyWith<$Res> {
  factory _$AdminStateCopyWith(
          _AdminState value, $Res Function(_AdminState) then) =
      __$AdminStateCopyWithImpl<$Res>;
  @override
  $Res call({Organization organization});

  @override
  $OrganizationCopyWith<$Res> get organization;
}

/// @nodoc
class __$AdminStateCopyWithImpl<$Res> extends _$AdminStateCopyWithImpl<$Res>
    implements _$AdminStateCopyWith<$Res> {
  __$AdminStateCopyWithImpl(
      _AdminState _value, $Res Function(_AdminState) _then)
      : super(_value, (v) => _then(v as _AdminState));

  @override
  _AdminState get _value => super._value as _AdminState;

  @override
  $Res call({
    Object organization = freezed,
  }) {
    return _then(_AdminState(
      organization: organization == freezed
          ? _value.organization
          : organization as Organization,
    ));
  }
}

/// @nodoc
class _$_AdminState extends _AdminState {
  _$_AdminState({this.organization}) : super._();

  @override
  final Organization organization;

  @override
  String toString() {
    return 'AdminState(organization: $organization)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AdminState &&
            (identical(other.organization, organization) ||
                const DeepCollectionEquality()
                    .equals(other.organization, organization)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(organization);

  @override
  _$AdminStateCopyWith<_AdminState> get copyWith =>
      __$AdminStateCopyWithImpl<_AdminState>(this, _$identity);
}

abstract class _AdminState extends AdminState {
  _AdminState._() : super._();
  factory _AdminState({Organization organization}) = _$_AdminState;

  @override
  Organization get organization;
  @override
  _$AdminStateCopyWith<_AdminState> get copyWith;
}
