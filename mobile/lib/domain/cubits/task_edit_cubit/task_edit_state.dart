part of 'task_edit_cubit.dart';

@freezed
abstract class TaskEditState with _$TaskEditState {
  factory TaskEditState({
    Task task,
    @Default('') String customBedName,
  }) = _TaskEditState;
}
