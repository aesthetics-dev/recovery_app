part of 'task_edit_cubit.dart';

@freezed
abstract class SelectableTag with _$SelectableTag {
  factory SelectableTag({
    @required String name,
    @Default(true) bool selected,
    @required TagType type,
  }) = _SelectableTag;

  static List<SelectableTag> ofTags({
    List<String> allTags,
    List<String> selectedTags,
    TagType type,
  }) {
    return allTags
        .map((tag) => SelectableTag(
            name: tag,
            type: type,
            selected: selectedTags?.any((sTag) => sTag == tag) ?? true))
        .toList();
  }
}

enum TagType { nurse, md }
