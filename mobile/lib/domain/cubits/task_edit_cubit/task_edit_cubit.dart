import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/domain/services/local_storage.dart';
import 'package:recovery_app/domain/services/task_service.dart';

part 'task_edit_state.dart';

part 'selectable_tag.dart';

part 'task_edit_cubit.freezed.dart';

class TaskEditCubit extends Cubit<TaskEditState> {
  final CoreCubit coreCubit;
  final LocalStorage localStorage;
  final TaskService taskService;
  final TaskListCubit taskListCubit;
  final FeedbackCubit feedback;

  TaskEditCubit({
    @required this.coreCubit,
    @required this.feedback,
    @required this.localStorage,
    @required this.taskService,
    @required this.taskListCubit,
  }) : super(TaskEditState());

  Future<void> newTask() async {
    final lastLocId = await localStorage.read(DataKey.lastLocation);

    final acc = coreCubit.state.account;

    final locRef = acc.locRefs.firstWhere(
          (locRef) => locRef.id == lastLocId,
          orElse: () => null,
        ) ??
        acc.locRefs.elementAt(0);

    localStorage.write(DataKey.lastLocation, locRef.id);

    emit(state.copyWith(
      task: Task(
        orgRef: coreCubit.state.organization.ref,
        locRef: locRef,
      ),
    ));
  }

  void setCustomTaskBedName(String newName) =>
      emit(state.copyWith(customBedName: newName));

  void submitCustomTaskBedName() =>
      emit(state.copyWith.task(bedRef: Reference(name: state.customBedName)));

  Future<void> setTaskLocation(Reference locRef) async {
    await localStorage.write(DataKey.lastLocation, locRef.id);
    emit(state.copyWith.task(locRef: locRef));
  }

  void setTask(Task task) {
    if (task == null) {
      newTask();
      return;
    }
    emit(state.copyWith(task: task));
  }

  void setTaskBed(Reference bedRef) {
    final task = taskListCubit.state.tasks.firstWhere(
      (element) => element.bedRef == bedRef,
      orElse: () => null,
    );
    if (task == null) {
      emit(state.copyWith(
        task: Task(
          orgRef: state.task.orgRef,
          locRef: state.task.locRef,
          bedRef: bedRef,
        ),
      ));
    } else {
      emit(state.copyWith(task: task));
    }
  }

  void setTaskTags(TagType tagType, List<String> tags) {
    if (tagType == TagType.nurse) {
      emit(state.copyWith.task(tags: tags));
    } else if (tagType == TagType.md) {
      emit(state.copyWith.task(mdTags: tags));
    }
  }

  Future<bool> saveTask(TagType tagType) async {
    bool success = true;

    if (taskListCubit.state.tasks.any((task) => task == state.task)) {
      return success;
    }

    final toSave = tagType == TagType.md
        ? state.task
        : state.task.copyWith(mdTags: [], customMDTag: '');

    await feedback.controlled(
      () async {
        success &= await taskService.saveTask(toSave);
        success &= await taskListCubit.updateTasks();
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
    return success;
  }

  void setCustomTag(TagType tagType, String customTag) {
    if (tagType == TagType.nurse) {
      emit(state.copyWith.task(customTag: customTag));
    } else {
      emit(state.copyWith.task(customMDTag: customTag));
    }
  }
}
