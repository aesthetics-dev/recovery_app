// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'task_edit_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$TaskEditStateTearOff {
  const _$TaskEditStateTearOff();

// ignore: unused_element
  _TaskEditState call({Task task, String customBedName = ''}) {
    return _TaskEditState(
      task: task,
      customBedName: customBedName,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $TaskEditState = _$TaskEditStateTearOff();

/// @nodoc
mixin _$TaskEditState {
  Task get task;
  String get customBedName;

  $TaskEditStateCopyWith<TaskEditState> get copyWith;
}

/// @nodoc
abstract class $TaskEditStateCopyWith<$Res> {
  factory $TaskEditStateCopyWith(
          TaskEditState value, $Res Function(TaskEditState) then) =
      _$TaskEditStateCopyWithImpl<$Res>;
  $Res call({Task task, String customBedName});

  $TaskCopyWith<$Res> get task;
}

/// @nodoc
class _$TaskEditStateCopyWithImpl<$Res>
    implements $TaskEditStateCopyWith<$Res> {
  _$TaskEditStateCopyWithImpl(this._value, this._then);

  final TaskEditState _value;
  // ignore: unused_field
  final $Res Function(TaskEditState) _then;

  @override
  $Res call({
    Object task = freezed,
    Object customBedName = freezed,
  }) {
    return _then(_value.copyWith(
      task: task == freezed ? _value.task : task as Task,
      customBedName: customBedName == freezed
          ? _value.customBedName
          : customBedName as String,
    ));
  }

  @override
  $TaskCopyWith<$Res> get task {
    if (_value.task == null) {
      return null;
    }
    return $TaskCopyWith<$Res>(_value.task, (value) {
      return _then(_value.copyWith(task: value));
    });
  }
}

/// @nodoc
abstract class _$TaskEditStateCopyWith<$Res>
    implements $TaskEditStateCopyWith<$Res> {
  factory _$TaskEditStateCopyWith(
          _TaskEditState value, $Res Function(_TaskEditState) then) =
      __$TaskEditStateCopyWithImpl<$Res>;
  @override
  $Res call({Task task, String customBedName});

  @override
  $TaskCopyWith<$Res> get task;
}

/// @nodoc
class __$TaskEditStateCopyWithImpl<$Res>
    extends _$TaskEditStateCopyWithImpl<$Res>
    implements _$TaskEditStateCopyWith<$Res> {
  __$TaskEditStateCopyWithImpl(
      _TaskEditState _value, $Res Function(_TaskEditState) _then)
      : super(_value, (v) => _then(v as _TaskEditState));

  @override
  _TaskEditState get _value => super._value as _TaskEditState;

  @override
  $Res call({
    Object task = freezed,
    Object customBedName = freezed,
  }) {
    return _then(_TaskEditState(
      task: task == freezed ? _value.task : task as Task,
      customBedName: customBedName == freezed
          ? _value.customBedName
          : customBedName as String,
    ));
  }
}

/// @nodoc
class _$_TaskEditState implements _TaskEditState {
  _$_TaskEditState({this.task, this.customBedName = ''})
      : assert(customBedName != null);

  @override
  final Task task;
  @JsonKey(defaultValue: '')
  @override
  final String customBedName;

  @override
  String toString() {
    return 'TaskEditState(task: $task, customBedName: $customBedName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TaskEditState &&
            (identical(other.task, task) ||
                const DeepCollectionEquality().equals(other.task, task)) &&
            (identical(other.customBedName, customBedName) ||
                const DeepCollectionEquality()
                    .equals(other.customBedName, customBedName)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(task) ^
      const DeepCollectionEquality().hash(customBedName);

  @override
  _$TaskEditStateCopyWith<_TaskEditState> get copyWith =>
      __$TaskEditStateCopyWithImpl<_TaskEditState>(this, _$identity);
}

abstract class _TaskEditState implements TaskEditState {
  factory _TaskEditState({Task task, String customBedName}) = _$_TaskEditState;

  @override
  Task get task;
  @override
  String get customBedName;
  @override
  _$TaskEditStateCopyWith<_TaskEditState> get copyWith;
}

/// @nodoc
class _$SelectableTagTearOff {
  const _$SelectableTagTearOff();

// ignore: unused_element
  _SelectableTag call(
      {@required String name, bool selected = true, @required TagType type}) {
    return _SelectableTag(
      name: name,
      selected: selected,
      type: type,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SelectableTag = _$SelectableTagTearOff();

/// @nodoc
mixin _$SelectableTag {
  String get name;
  bool get selected;
  TagType get type;

  $SelectableTagCopyWith<SelectableTag> get copyWith;
}

/// @nodoc
abstract class $SelectableTagCopyWith<$Res> {
  factory $SelectableTagCopyWith(
          SelectableTag value, $Res Function(SelectableTag) then) =
      _$SelectableTagCopyWithImpl<$Res>;
  $Res call({String name, bool selected, TagType type});
}

/// @nodoc
class _$SelectableTagCopyWithImpl<$Res>
    implements $SelectableTagCopyWith<$Res> {
  _$SelectableTagCopyWithImpl(this._value, this._then);

  final SelectableTag _value;
  // ignore: unused_field
  final $Res Function(SelectableTag) _then;

  @override
  $Res call({
    Object name = freezed,
    Object selected = freezed,
    Object type = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed ? _value.name : name as String,
      selected: selected == freezed ? _value.selected : selected as bool,
      type: type == freezed ? _value.type : type as TagType,
    ));
  }
}

/// @nodoc
abstract class _$SelectableTagCopyWith<$Res>
    implements $SelectableTagCopyWith<$Res> {
  factory _$SelectableTagCopyWith(
          _SelectableTag value, $Res Function(_SelectableTag) then) =
      __$SelectableTagCopyWithImpl<$Res>;
  @override
  $Res call({String name, bool selected, TagType type});
}

/// @nodoc
class __$SelectableTagCopyWithImpl<$Res>
    extends _$SelectableTagCopyWithImpl<$Res>
    implements _$SelectableTagCopyWith<$Res> {
  __$SelectableTagCopyWithImpl(
      _SelectableTag _value, $Res Function(_SelectableTag) _then)
      : super(_value, (v) => _then(v as _SelectableTag));

  @override
  _SelectableTag get _value => super._value as _SelectableTag;

  @override
  $Res call({
    Object name = freezed,
    Object selected = freezed,
    Object type = freezed,
  }) {
    return _then(_SelectableTag(
      name: name == freezed ? _value.name : name as String,
      selected: selected == freezed ? _value.selected : selected as bool,
      type: type == freezed ? _value.type : type as TagType,
    ));
  }
}

/// @nodoc
class _$_SelectableTag implements _SelectableTag {
  _$_SelectableTag(
      {@required this.name, this.selected = true, @required this.type})
      : assert(name != null),
        assert(selected != null),
        assert(type != null);

  @override
  final String name;
  @JsonKey(defaultValue: true)
  @override
  final bool selected;
  @override
  final TagType type;

  @override
  String toString() {
    return 'SelectableTag(name: $name, selected: $selected, type: $type)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SelectableTag &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.selected, selected) ||
                const DeepCollectionEquality()
                    .equals(other.selected, selected)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(selected) ^
      const DeepCollectionEquality().hash(type);

  @override
  _$SelectableTagCopyWith<_SelectableTag> get copyWith =>
      __$SelectableTagCopyWithImpl<_SelectableTag>(this, _$identity);
}

abstract class _SelectableTag implements SelectableTag {
  factory _SelectableTag(
      {@required String name,
      bool selected,
      @required TagType type}) = _$_SelectableTag;

  @override
  String get name;
  @override
  bool get selected;
  @override
  TagType get type;
  @override
  _$SelectableTagCopyWith<_SelectableTag> get copyWith;
}
