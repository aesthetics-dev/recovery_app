// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'historic_task_list_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$HistoricTaskListStateTearOff {
  const _$HistoricTaskListStateTearOff();

// ignore: unused_element
  _HistoricTaskListState call(
      {List<Task> tasks = const [], int lastUpdate = 0}) {
    return _HistoricTaskListState(
      tasks: tasks,
      lastUpdate: lastUpdate,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $HistoricTaskListState = _$HistoricTaskListStateTearOff();

/// @nodoc
mixin _$HistoricTaskListState {
  List<Task> get tasks;
  int get lastUpdate;

  $HistoricTaskListStateCopyWith<HistoricTaskListState> get copyWith;
}

/// @nodoc
abstract class $HistoricTaskListStateCopyWith<$Res> {
  factory $HistoricTaskListStateCopyWith(HistoricTaskListState value,
          $Res Function(HistoricTaskListState) then) =
      _$HistoricTaskListStateCopyWithImpl<$Res>;
  $Res call({List<Task> tasks, int lastUpdate});
}

/// @nodoc
class _$HistoricTaskListStateCopyWithImpl<$Res>
    implements $HistoricTaskListStateCopyWith<$Res> {
  _$HistoricTaskListStateCopyWithImpl(this._value, this._then);

  final HistoricTaskListState _value;
  // ignore: unused_field
  final $Res Function(HistoricTaskListState) _then;

  @override
  $Res call({
    Object tasks = freezed,
    Object lastUpdate = freezed,
  }) {
    return _then(_value.copyWith(
      tasks: tasks == freezed ? _value.tasks : tasks as List<Task>,
      lastUpdate: lastUpdate == freezed ? _value.lastUpdate : lastUpdate as int,
    ));
  }
}

/// @nodoc
abstract class _$HistoricTaskListStateCopyWith<$Res>
    implements $HistoricTaskListStateCopyWith<$Res> {
  factory _$HistoricTaskListStateCopyWith(_HistoricTaskListState value,
          $Res Function(_HistoricTaskListState) then) =
      __$HistoricTaskListStateCopyWithImpl<$Res>;
  @override
  $Res call({List<Task> tasks, int lastUpdate});
}

/// @nodoc
class __$HistoricTaskListStateCopyWithImpl<$Res>
    extends _$HistoricTaskListStateCopyWithImpl<$Res>
    implements _$HistoricTaskListStateCopyWith<$Res> {
  __$HistoricTaskListStateCopyWithImpl(_HistoricTaskListState _value,
      $Res Function(_HistoricTaskListState) _then)
      : super(_value, (v) => _then(v as _HistoricTaskListState));

  @override
  _HistoricTaskListState get _value => super._value as _HistoricTaskListState;

  @override
  $Res call({
    Object tasks = freezed,
    Object lastUpdate = freezed,
  }) {
    return _then(_HistoricTaskListState(
      tasks: tasks == freezed ? _value.tasks : tasks as List<Task>,
      lastUpdate: lastUpdate == freezed ? _value.lastUpdate : lastUpdate as int,
    ));
  }
}

/// @nodoc
class _$_HistoricTaskListState extends _HistoricTaskListState {
  _$_HistoricTaskListState({this.tasks = const [], this.lastUpdate = 0})
      : assert(tasks != null),
        assert(lastUpdate != null),
        super._();

  @JsonKey(defaultValue: const [])
  @override
  final List<Task> tasks;
  @JsonKey(defaultValue: 0)
  @override
  final int lastUpdate;

  @override
  String toString() {
    return 'HistoricTaskListState(tasks: $tasks, lastUpdate: $lastUpdate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _HistoricTaskListState &&
            (identical(other.tasks, tasks) ||
                const DeepCollectionEquality().equals(other.tasks, tasks)) &&
            (identical(other.lastUpdate, lastUpdate) ||
                const DeepCollectionEquality()
                    .equals(other.lastUpdate, lastUpdate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(tasks) ^
      const DeepCollectionEquality().hash(lastUpdate);

  @override
  _$HistoricTaskListStateCopyWith<_HistoricTaskListState> get copyWith =>
      __$HistoricTaskListStateCopyWithImpl<_HistoricTaskListState>(
          this, _$identity);
}

abstract class _HistoricTaskListState extends HistoricTaskListState {
  _HistoricTaskListState._() : super._();
  factory _HistoricTaskListState({List<Task> tasks, int lastUpdate}) =
      _$_HistoricTaskListState;

  @override
  List<Task> get tasks;
  @override
  int get lastUpdate;
  @override
  _$HistoricTaskListStateCopyWith<_HistoricTaskListState> get copyWith;
}
