part of 'historic_task_list_cubit.dart';

@freezed
abstract class HistoricTaskListState implements _$HistoricTaskListState {
  factory HistoricTaskListState({
    @Default([]) List<Task> tasks,
    @Default(0) int lastUpdate,
  }) = _HistoricTaskListState;

  const HistoricTaskListState._();
}
