import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/domain/services/task_service.dart';

part 'historic_task_list_state.dart';

part 'historic_task_list_cubit.freezed.dart';

class HistoricTaskListCubit extends Cubit<HistoricTaskListState> {
  final TaskListCubit taskListCubit;
  final TaskService taskService;
  final CoreCubit coreCubit;

  HistoricTaskListCubit({@required this.taskListCubit})
      : taskService = taskListCubit.taskService,
        coreCubit = taskListCubit.coreCubit,
        super(HistoricTaskListState());

  Future<void> update() async {
    if (taskListCubit.state.lastUpdate < state.lastUpdate) {
      return;
    }

    final tasks = await taskService.getForAccount(
      coreCubit.state.organization.ref,
      historic: true,
    );

    if (tasks == null) {
      return;
    }

    emit(state.copyWith(
      tasks: tasks,
      lastUpdate: DateTime.now().millisecondsSinceEpoch,
    ));
  }
}
