part of 'task_list_cubit.dart';

@freezed
abstract class TaskListState implements _$TaskListState {
  factory TaskListState({
    List<Task> tasks,
    List<LocationTaskAverage> locationAverages,
    @Default(0) int lastUpdate,
    @Default([]) List<ListAction> actions,
    Message message,
  }) = _TaskListState;

  const TaskListState._();

  @override
  String toString() {
    final beds = tasks.map((t) => t.bedRef.name).toList();
    return 'actions: $actions | tasks: $beds';
  }
}
