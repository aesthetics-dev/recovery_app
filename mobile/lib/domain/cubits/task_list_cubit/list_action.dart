import 'package:equatable/equatable.dart';

class ListAction<T> extends Equatable {
  final ListActionType type;
  final int index;
  final T data;

  const ListAction(this.type, this.index, [this.data]);

  @override
  List<Object> get props => [type, index, data];
}

enum ListActionType { insert, remove }
