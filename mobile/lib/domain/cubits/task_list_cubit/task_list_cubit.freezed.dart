// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'task_list_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$TaskListStateTearOff {
  const _$TaskListStateTearOff();

// ignore: unused_element
  _TaskListState call(
      {List<Task> tasks,
      List<LocationTaskAverage> locationAverages,
      int lastUpdate = 0,
      List<ListAction> actions = const [],
      Message message}) {
    return _TaskListState(
      tasks: tasks,
      locationAverages: locationAverages,
      lastUpdate: lastUpdate,
      actions: actions,
      message: message,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $TaskListState = _$TaskListStateTearOff();

/// @nodoc
mixin _$TaskListState {
  List<Task> get tasks;
  List<LocationTaskAverage> get locationAverages;
  int get lastUpdate;
  List<ListAction> get actions;
  Message get message;

  $TaskListStateCopyWith<TaskListState> get copyWith;
}

/// @nodoc
abstract class $TaskListStateCopyWith<$Res> {
  factory $TaskListStateCopyWith(
          TaskListState value, $Res Function(TaskListState) then) =
      _$TaskListStateCopyWithImpl<$Res>;
  $Res call(
      {List<Task> tasks,
      List<LocationTaskAverage> locationAverages,
      int lastUpdate,
      List<ListAction> actions,
      Message message});
}

/// @nodoc
class _$TaskListStateCopyWithImpl<$Res>
    implements $TaskListStateCopyWith<$Res> {
  _$TaskListStateCopyWithImpl(this._value, this._then);

  final TaskListState _value;
  // ignore: unused_field
  final $Res Function(TaskListState) _then;

  @override
  $Res call({
    Object tasks = freezed,
    Object locationAverages = freezed,
    Object lastUpdate = freezed,
    Object actions = freezed,
    Object message = freezed,
  }) {
    return _then(_value.copyWith(
      tasks: tasks == freezed ? _value.tasks : tasks as List<Task>,
      locationAverages: locationAverages == freezed
          ? _value.locationAverages
          : locationAverages as List<LocationTaskAverage>,
      lastUpdate: lastUpdate == freezed ? _value.lastUpdate : lastUpdate as int,
      actions:
          actions == freezed ? _value.actions : actions as List<ListAction>,
      message: message == freezed ? _value.message : message as Message,
    ));
  }
}

/// @nodoc
abstract class _$TaskListStateCopyWith<$Res>
    implements $TaskListStateCopyWith<$Res> {
  factory _$TaskListStateCopyWith(
          _TaskListState value, $Res Function(_TaskListState) then) =
      __$TaskListStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<Task> tasks,
      List<LocationTaskAverage> locationAverages,
      int lastUpdate,
      List<ListAction> actions,
      Message message});
}

/// @nodoc
class __$TaskListStateCopyWithImpl<$Res>
    extends _$TaskListStateCopyWithImpl<$Res>
    implements _$TaskListStateCopyWith<$Res> {
  __$TaskListStateCopyWithImpl(
      _TaskListState _value, $Res Function(_TaskListState) _then)
      : super(_value, (v) => _then(v as _TaskListState));

  @override
  _TaskListState get _value => super._value as _TaskListState;

  @override
  $Res call({
    Object tasks = freezed,
    Object locationAverages = freezed,
    Object lastUpdate = freezed,
    Object actions = freezed,
    Object message = freezed,
  }) {
    return _then(_TaskListState(
      tasks: tasks == freezed ? _value.tasks : tasks as List<Task>,
      locationAverages: locationAverages == freezed
          ? _value.locationAverages
          : locationAverages as List<LocationTaskAverage>,
      lastUpdate: lastUpdate == freezed ? _value.lastUpdate : lastUpdate as int,
      actions:
          actions == freezed ? _value.actions : actions as List<ListAction>,
      message: message == freezed ? _value.message : message as Message,
    ));
  }
}

/// @nodoc
class _$_TaskListState extends _TaskListState {
  _$_TaskListState(
      {this.tasks,
      this.locationAverages,
      this.lastUpdate = 0,
      this.actions = const [],
      this.message})
      : assert(lastUpdate != null),
        assert(actions != null),
        super._();

  @override
  final List<Task> tasks;
  @override
  final List<LocationTaskAverage> locationAverages;
  @JsonKey(defaultValue: 0)
  @override
  final int lastUpdate;
  @JsonKey(defaultValue: const [])
  @override
  final List<ListAction> actions;
  @override
  final Message message;

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TaskListState &&
            (identical(other.tasks, tasks) ||
                const DeepCollectionEquality().equals(other.tasks, tasks)) &&
            (identical(other.locationAverages, locationAverages) ||
                const DeepCollectionEquality()
                    .equals(other.locationAverages, locationAverages)) &&
            (identical(other.lastUpdate, lastUpdate) ||
                const DeepCollectionEquality()
                    .equals(other.lastUpdate, lastUpdate)) &&
            (identical(other.actions, actions) ||
                const DeepCollectionEquality()
                    .equals(other.actions, actions)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(tasks) ^
      const DeepCollectionEquality().hash(locationAverages) ^
      const DeepCollectionEquality().hash(lastUpdate) ^
      const DeepCollectionEquality().hash(actions) ^
      const DeepCollectionEquality().hash(message);

  @override
  _$TaskListStateCopyWith<_TaskListState> get copyWith =>
      __$TaskListStateCopyWithImpl<_TaskListState>(this, _$identity);
}

abstract class _TaskListState extends TaskListState {
  _TaskListState._() : super._();
  factory _TaskListState(
      {List<Task> tasks,
      List<LocationTaskAverage> locationAverages,
      int lastUpdate,
      List<ListAction> actions,
      Message message}) = _$_TaskListState;

  @override
  List<Task> get tasks;
  @override
  List<LocationTaskAverage> get locationAverages;
  @override
  int get lastUpdate;
  @override
  List<ListAction> get actions;
  @override
  Message get message;
  @override
  _$TaskListStateCopyWith<_TaskListState> get copyWith;
}
