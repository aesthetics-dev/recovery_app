import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/list_action.dart';
import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/data/location_task_average/location_task_average.dart';
import 'package:recovery_app/domain/models/util/message/message.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/domain/services/notification_service.dart';
import 'package:recovery_app/domain/services/task_service.dart';
import 'package:recovery_app/domain/services/tts_service.dart';
import 'package:recovery_app/domain/services/user_service.dart';
import 'package:recovery_app/domain/utilities/subscription_handler.dart';
import 'package:dartx/dartx.dart';

part 'task_list_state.dart';

part 'task_list_cubit.freezed.dart';

class TaskListCubit extends Cubit<TaskListState> {
  final TaskService taskService;
  final CoreCubit coreCubit;
  final TTSService ttsService;
  final NotificationService notificationService;
  final FeedbackCubit feedback;

  SubscriptionHandler subs = SubscriptionHandler();

  TaskListCubit({
    @required this.taskService,
    @required this.notificationService,
    @required this.coreCubit,
    @required this.ttsService,
    @required this.feedback,
  }) : super(TaskListState()) {
    // when there's a location update
    subs += notificationService.$refreshReminder.listen(_notificationListener);
    // when something changes about the account
    subs += coreCubit.listen(_coreCubitListener);
    // initial update
    updateTasks();
  }

  @override
  Future<void> close() {
    subs.close();
    return super.close();
  }

  void _notificationListener(value) {
    if (value == RefreshValue.tasks) {
      updateTasks();
    }
  }

  void _coreCubitListener(CoreState state) {
    if (state.account?.locRefs != null &&
        coreCubit.userService.authStatus == AuthStatus.signedIn) {
      updateTasks();
    }
  }

  Future<bool> updateTasks() async {
    if (coreCubit.state.organization == null) return false;

    return feedback.controlled(
      () async {
        final tasks =
            await taskService.getForAccount(coreCubit.state.organization.ref);
        if (tasks == null) {
          emit(state.copyWith(tasks: []));
          return false;
        }
        _audioNotifyAboutTasks(state.tasks, tasks);

        emit(state.copyWith(
          tasks: tasks,
          lastUpdate: DateTime.now().millisecondsSinceEpoch,
        ));
        return true;
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: false,
      fallback: false,
    );
  }

  Future<void> updateStats() async {
    feedback.controlled(() async {
      final avg = await taskService.getStats(
        coreCubit.state.organization.ref,
      );
      emit(state.copyWith(locationAverages: avg));
    },
        shouldRethrow: false,
        withLoading: true,
        displayError: true,
        overrideError: Messages.couldNotUpdateStats);
  }

  Future<void> _audioNotifyAboutTasks(
    List<Task> prevTasks,
    List<Task> newTasks,
  ) async {
    if (!coreCubit.state.account.areVoiceAnnouncementsOn ||
        [prevTasks, newTasks].contains(null)) {
      return;
    }

    final diff = newTasks.length - prevTasks.length;
    if (diff == 1 && newTasks.first.isRecent) {
      ttsService.taskInsert(newTasks.first);
    } else if (diff == 0) {
      _processPossibleTaskUpdate(newTasks, prevTasks);
    }
  }

  void _processPossibleTaskUpdate(List<Task> newTasks, List<Task> prevTasks) {
    // the order may change so we need to compare every new task to every old
    for (final newTask in newTasks) {
      for (final oldTask in prevTasks) {
        // different beds -- disregard
        if (newTask.bedRef != oldTask.bedRef) continue;

        // only care about changes in `allTags`
        if (newTask.allTags.contentEquals(oldTask.allTags)) continue;

        // announce the change
        ttsService.taskUpdate(newTask);

        // only one announcement
        return;
      }
    }
  }

  Future<void> removeTask(Task task) async {
    await feedback.controlled(
      () async => taskService.removeTask(task),
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
      overrideError: Messages.couldNotRemoveTask,
    );
  }
}
