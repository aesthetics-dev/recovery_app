import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/data/abridged_organization/abridged_organization.dart';
import 'package:recovery_app/domain/services/organization_service.dart';
import 'package:recovery_app/domain/services/user_service.dart';
import 'package:flutter/foundation.dart';

part 'auth_state.dart';

part 'auth_cubit.freezed.dart';

class AuthCubit extends Cubit<AuthState> {
  final UserService userService;
  final OrganizationService organizationService;
  final FeedbackCubit feedback;
  final FirebaseDynamicLinks dynamicLinks;

  AuthCubit({
    @required this.userService,
    @required this.organizationService,
    @required this.feedback,
    @required this.dynamicLinks,
  }) : super(AuthState(stage: AuthStage.initStage)) {
    initDynamicLink();
  }

  @override
  Future<void> close() {
    feedback.close();
    return super.close();
  }

  void reset() {
    emit(AuthState(stage: AuthStage.infoEnter));
  }

  void selectOrganization(AbridgedOrganization org) {
    emit(state.copyWith(organization: org));
  }

  void setEmail(String email) {
    emit(state.copyWith(email: email));
  }

  void setPassword(String password) {
    emit(state.copyWith(password: password));
  }

  Future<void> initDynamicLink() async {
    final data = await dynamicLinks.getInitialLink();
    onDynamicLink(data);

    FirebaseDynamicLinks.instance.onLink(
      onSuccess: onDynamicLink,
    );
  }

  Future<void> onDynamicLink(PendingDynamicLinkData dynamicLinkData) async {
    feedback.controlled(
      () async {
        await userService.trySignIn(dynamicLinkData);
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
  }

  Future<void> submitAuth() async {
    if (!state.isEmailValid) {
      feedback.addMessage(Messages.badEmailFormat);
      return;
    }

    if (state.organization.authType == AuthType.password &&
        state.password.isEmpty) {
      feedback.addMessage(Messages.badPasswordFormat);
      return;
    }

    feedback.controlled(
      () async {
        final emailStatus = await userService.checkEmailInDomain(
          state.organization.ref,
          state.email,
        );

        if (emailStatus != EmailStatus.valid) {
          throw Messages.unrecognizedEmail;
        }
        if (state.organization.authType == AuthType.password) {
          await _signInWithEmailAndPassword();
        } else {
          await _signInWithEmailLink();
        }
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
  }

  Future _signInWithEmailLink() async {
    final sentResponse = await userService.sendSignInLink(
      state.organization.ref,
      state.email,
    );

    if (sentResponse.success) {
      emit(state.copyWith(stage: AuthStage.emailVerification));
    } else {
      feedback.addMessage(Messages.couldNotSendSignInLink);
    }
  }

  Future _signInWithEmailAndPassword() async {
    await feedback.controlled(
      () async => userService.signInWithEmailAndPassword(
        state.organization.ref.id,
        state.email,
        state.password,
      ),
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
      overrideError: Messages.invalidEmailOrPassword,
    );
  }

  void cancelEmailVerification() {
    emit(state.copyWith(stage: AuthStage.infoEnter));
  }
}
