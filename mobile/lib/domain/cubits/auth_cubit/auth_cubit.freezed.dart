// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'auth_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

// ignore: unused_element
  _AuthState call(
      {@required AuthStage stage,
      String email = '',
      String password = '',
      AbridgedOrganization organization}) {
    return _AuthState(
      stage: stage,
      email: email,
      password: password,
      organization: organization,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  AuthStage get stage;
  String get email;
  String get password;
  AbridgedOrganization get organization;

  $AuthStateCopyWith<AuthState> get copyWith;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
  $Res call(
      {AuthStage stage,
      String email,
      String password,
      AbridgedOrganization organization});

  $AbridgedOrganizationCopyWith<$Res> get organization;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;

  @override
  $Res call({
    Object stage = freezed,
    Object email = freezed,
    Object password = freezed,
    Object organization = freezed,
  }) {
    return _then(_value.copyWith(
      stage: stage == freezed ? _value.stage : stage as AuthStage,
      email: email == freezed ? _value.email : email as String,
      password: password == freezed ? _value.password : password as String,
      organization: organization == freezed
          ? _value.organization
          : organization as AbridgedOrganization,
    ));
  }

  @override
  $AbridgedOrganizationCopyWith<$Res> get organization {
    if (_value.organization == null) {
      return null;
    }
    return $AbridgedOrganizationCopyWith<$Res>(_value.organization, (value) {
      return _then(_value.copyWith(organization: value));
    });
  }
}

/// @nodoc
abstract class _$AuthStateCopyWith<$Res> implements $AuthStateCopyWith<$Res> {
  factory _$AuthStateCopyWith(
          _AuthState value, $Res Function(_AuthState) then) =
      __$AuthStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {AuthStage stage,
      String email,
      String password,
      AbridgedOrganization organization});

  @override
  $AbridgedOrganizationCopyWith<$Res> get organization;
}

/// @nodoc
class __$AuthStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateCopyWith<$Res> {
  __$AuthStateCopyWithImpl(_AuthState _value, $Res Function(_AuthState) _then)
      : super(_value, (v) => _then(v as _AuthState));

  @override
  _AuthState get _value => super._value as _AuthState;

  @override
  $Res call({
    Object stage = freezed,
    Object email = freezed,
    Object password = freezed,
    Object organization = freezed,
  }) {
    return _then(_AuthState(
      stage: stage == freezed ? _value.stage : stage as AuthStage,
      email: email == freezed ? _value.email : email as String,
      password: password == freezed ? _value.password : password as String,
      organization: organization == freezed
          ? _value.organization
          : organization as AbridgedOrganization,
    ));
  }
}

/// @nodoc
class _$_AuthState extends _AuthState with DiagnosticableTreeMixin {
  _$_AuthState(
      {@required this.stage,
      this.email = '',
      this.password = '',
      this.organization})
      : assert(stage != null),
        assert(email != null),
        assert(password != null),
        super._();

  @override
  final AuthStage stage;
  @JsonKey(defaultValue: '')
  @override
  final String email;
  @JsonKey(defaultValue: '')
  @override
  final String password;
  @override
  final AbridgedOrganization organization;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AuthState'))
      ..add(DiagnosticsProperty('stage', stage))
      ..add(DiagnosticsProperty('email', email))
      ..add(DiagnosticsProperty('password', password))
      ..add(DiagnosticsProperty('organization', organization));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AuthState &&
            (identical(other.stage, stage) ||
                const DeepCollectionEquality().equals(other.stage, stage)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.organization, organization) ||
                const DeepCollectionEquality()
                    .equals(other.organization, organization)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(stage) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(organization);

  @override
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      __$AuthStateCopyWithImpl<_AuthState>(this, _$identity);
}

abstract class _AuthState extends AuthState {
  _AuthState._() : super._();
  factory _AuthState(
      {@required AuthStage stage,
      String email,
      String password,
      AbridgedOrganization organization}) = _$_AuthState;

  @override
  AuthStage get stage;
  @override
  String get email;
  @override
  String get password;
  @override
  AbridgedOrganization get organization;
  @override
  _$AuthStateCopyWith<_AuthState> get copyWith;
}
