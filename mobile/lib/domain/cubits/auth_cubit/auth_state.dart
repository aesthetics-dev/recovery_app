part of 'auth_cubit.dart';

@freezed
abstract class AuthState implements _$AuthState {
  factory AuthState({
    @required AuthStage stage,
    @Default('') String email,
    @Default('') String password,
    AbridgedOrganization organization,
  }) = _AuthState;

  const AuthState._();

  bool get isEmailValid => EmailValidator.validate(email);

  @override
  String toString() {
    return 'AuthLoadedState(stage: $stage, email: $email';
  }
}

enum AuthStage {
  initStage,
  infoEnter,
  emailVerification,
  fatalError,
}
