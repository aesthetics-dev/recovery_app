// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'settings_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SettingsStateTearOff {
  const _$SettingsStateTearOff();

// ignore: unused_element
  _SettingsScreenState call({Account account}) {
    return _SettingsScreenState(
      account: account,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SettingsState = _$SettingsStateTearOff();

/// @nodoc
mixin _$SettingsState {
  Account get account;

  $SettingsStateCopyWith<SettingsState> get copyWith;
}

/// @nodoc
abstract class $SettingsStateCopyWith<$Res> {
  factory $SettingsStateCopyWith(
          SettingsState value, $Res Function(SettingsState) then) =
      _$SettingsStateCopyWithImpl<$Res>;
  $Res call({Account account});

  $AccountCopyWith<$Res> get account;
}

/// @nodoc
class _$SettingsStateCopyWithImpl<$Res>
    implements $SettingsStateCopyWith<$Res> {
  _$SettingsStateCopyWithImpl(this._value, this._then);

  final SettingsState _value;
  // ignore: unused_field
  final $Res Function(SettingsState) _then;

  @override
  $Res call({
    Object account = freezed,
  }) {
    return _then(_value.copyWith(
      account: account == freezed ? _value.account : account as Account,
    ));
  }

  @override
  $AccountCopyWith<$Res> get account {
    if (_value.account == null) {
      return null;
    }
    return $AccountCopyWith<$Res>(_value.account, (value) {
      return _then(_value.copyWith(account: value));
    });
  }
}

/// @nodoc
abstract class _$SettingsScreenStateCopyWith<$Res>
    implements $SettingsStateCopyWith<$Res> {
  factory _$SettingsScreenStateCopyWith(_SettingsScreenState value,
          $Res Function(_SettingsScreenState) then) =
      __$SettingsScreenStateCopyWithImpl<$Res>;
  @override
  $Res call({Account account});

  @override
  $AccountCopyWith<$Res> get account;
}

/// @nodoc
class __$SettingsScreenStateCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$SettingsScreenStateCopyWith<$Res> {
  __$SettingsScreenStateCopyWithImpl(
      _SettingsScreenState _value, $Res Function(_SettingsScreenState) _then)
      : super(_value, (v) => _then(v as _SettingsScreenState));

  @override
  _SettingsScreenState get _value => super._value as _SettingsScreenState;

  @override
  $Res call({
    Object account = freezed,
  }) {
    return _then(_SettingsScreenState(
      account: account == freezed ? _value.account : account as Account,
    ));
  }
}

/// @nodoc
class _$_SettingsScreenState extends _SettingsScreenState
    with DiagnosticableTreeMixin {
  _$_SettingsScreenState({this.account}) : super._();

  @override
  final Account account;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SettingsState(account: $account)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SettingsState'))
      ..add(DiagnosticsProperty('account', account));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SettingsScreenState &&
            (identical(other.account, account) ||
                const DeepCollectionEquality().equals(other.account, account)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(account);

  @override
  _$SettingsScreenStateCopyWith<_SettingsScreenState> get copyWith =>
      __$SettingsScreenStateCopyWithImpl<_SettingsScreenState>(
          this, _$identity);
}

abstract class _SettingsScreenState extends SettingsState {
  _SettingsScreenState._() : super._();
  factory _SettingsScreenState({Account account}) = _$_SettingsScreenState;

  @override
  Account get account;
  @override
  _$SettingsScreenStateCopyWith<_SettingsScreenState> get copyWith;
}
