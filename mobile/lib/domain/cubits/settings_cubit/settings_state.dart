part of 'settings_cubit.dart';

@freezed
abstract class SettingsState implements _$SettingsState {
  factory SettingsState({
    Account account,
  }) = _SettingsScreenState;

  const SettingsState._();
}

enum SettingsSwitch {
  notificationsOn,
  displayAlwaysOn,
  voiceAnnouncementsOn,
}