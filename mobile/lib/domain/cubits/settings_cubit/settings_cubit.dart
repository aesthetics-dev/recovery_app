import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/data/account/account.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/data/role/role.dart';
import 'package:recovery_app/domain/services/notification_service.dart';
import 'package:recovery_app/domain/services/user_service.dart';
import 'package:flutter/foundation.dart';

part 'settings_state.dart';

part 'settings_cubit.freezed.dart';

class SettingsCubit extends Cubit<SettingsState> {
  final UserService userService;
  final NotificationService notificationService;
  final FeedbackCubit feedback;
  final CoreCubit coreCubit;

  SettingsCubit({
    @required this.notificationService,
    @required this.userService,
    @required this.feedback,
    @required this.coreCubit,
  }) : super(SettingsState(
          account: coreCubit.state.account,
        ));

  void reset() {
    emit(state.copyWith(account: coreCubit.state.account));
  }

  void toggleField(SettingsSwitch field) {
    switch (field) {
      case SettingsSwitch.notificationsOn:
        emit(state.copyWith
            .account(notificationsOn: !state.account.notificationsOn));
        break;
      case SettingsSwitch.displayAlwaysOn:
        emit(state.copyWith
            .account(isDisplayAlwaysOn: !state.account.isDisplayAlwaysOn));
        break;
      case SettingsSwitch.voiceAnnouncementsOn:
        emit(state.copyWith.account(
            areVoiceAnnouncementsOn: !state.account.areVoiceAnnouncementsOn));
        break;
    }
  }

  void setFollowed(List<Reference> locRefs) {
    emit(state.copyWith.account(locRefs: locRefs));
  }

  Future<void> signOut() async {
    feedback.controlled(
      () async {
        await save();
        await notificationService.deleteToken(coreCubit.state.account);
        await userService.signOut();
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
  }

  Future<bool> save() async {
    if (coreCubit.state.account == state.account &&
        !state.account.needsRegistration) {
      return true;
    }

    if (state.account.ref.name.isEmpty) {
      feedback.addMessage(Messages.nameCantBeEmpty);
      return false;
    }
    if (state.account.locRefs.isEmpty) {
      feedback.addMessage(Messages.selectLocation);
      return false;
    }
    if (state.account.role == null) {
      feedback.addMessage(Messages.selectRole);
      return false;
    }

    return feedback.controlled(
      () async {
        await userService.writeAccount(state.account);
        await coreCubit.reload();
        return true;
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
      fallback: false,
    );
  }

  void nameChanged(String value) {
    emit(state.copyWith.account.ref(name: value));
  }

  void roleChanged(Role role) {
    emit(state.copyWith.account(role: role));
  }
}
