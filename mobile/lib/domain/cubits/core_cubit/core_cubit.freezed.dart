// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'core_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CoreStateTearOff {
  const _$CoreStateTearOff();

// ignore: unused_element
  _CoreState call(
      {Account account,
      Organization organization,
      List<AbridgedOrganization> orgs}) {
    return _CoreState(
      account: account,
      organization: organization,
      orgs: orgs,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CoreState = _$CoreStateTearOff();

/// @nodoc
mixin _$CoreState {
  Account get account;
  Organization get organization;
  List<AbridgedOrganization> get orgs;

  $CoreStateCopyWith<CoreState> get copyWith;
}

/// @nodoc
abstract class $CoreStateCopyWith<$Res> {
  factory $CoreStateCopyWith(CoreState value, $Res Function(CoreState) then) =
      _$CoreStateCopyWithImpl<$Res>;
  $Res call(
      {Account account,
      Organization organization,
      List<AbridgedOrganization> orgs});

  $AccountCopyWith<$Res> get account;
  $OrganizationCopyWith<$Res> get organization;
}

/// @nodoc
class _$CoreStateCopyWithImpl<$Res> implements $CoreStateCopyWith<$Res> {
  _$CoreStateCopyWithImpl(this._value, this._then);

  final CoreState _value;
  // ignore: unused_field
  final $Res Function(CoreState) _then;

  @override
  $Res call({
    Object account = freezed,
    Object organization = freezed,
    Object orgs = freezed,
  }) {
    return _then(_value.copyWith(
      account: account == freezed ? _value.account : account as Account,
      organization: organization == freezed
          ? _value.organization
          : organization as Organization,
      orgs: orgs == freezed ? _value.orgs : orgs as List<AbridgedOrganization>,
    ));
  }

  @override
  $AccountCopyWith<$Res> get account {
    if (_value.account == null) {
      return null;
    }
    return $AccountCopyWith<$Res>(_value.account, (value) {
      return _then(_value.copyWith(account: value));
    });
  }

  @override
  $OrganizationCopyWith<$Res> get organization {
    if (_value.organization == null) {
      return null;
    }
    return $OrganizationCopyWith<$Res>(_value.organization, (value) {
      return _then(_value.copyWith(organization: value));
    });
  }
}

/// @nodoc
abstract class _$CoreStateCopyWith<$Res> implements $CoreStateCopyWith<$Res> {
  factory _$CoreStateCopyWith(
          _CoreState value, $Res Function(_CoreState) then) =
      __$CoreStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Account account,
      Organization organization,
      List<AbridgedOrganization> orgs});

  @override
  $AccountCopyWith<$Res> get account;
  @override
  $OrganizationCopyWith<$Res> get organization;
}

/// @nodoc
class __$CoreStateCopyWithImpl<$Res> extends _$CoreStateCopyWithImpl<$Res>
    implements _$CoreStateCopyWith<$Res> {
  __$CoreStateCopyWithImpl(_CoreState _value, $Res Function(_CoreState) _then)
      : super(_value, (v) => _then(v as _CoreState));

  @override
  _CoreState get _value => super._value as _CoreState;

  @override
  $Res call({
    Object account = freezed,
    Object organization = freezed,
    Object orgs = freezed,
  }) {
    return _then(_CoreState(
      account: account == freezed ? _value.account : account as Account,
      organization: organization == freezed
          ? _value.organization
          : organization as Organization,
      orgs: orgs == freezed ? _value.orgs : orgs as List<AbridgedOrganization>,
    ));
  }
}

/// @nodoc
class _$_CoreState extends _CoreState with DiagnosticableTreeMixin {
  _$_CoreState({this.account, this.organization, this.orgs}) : super._();

  @override
  final Account account;
  @override
  final Organization organization;
  @override
  final List<AbridgedOrganization> orgs;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CoreState(account: $account, organization: $organization, orgs: $orgs)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CoreState'))
      ..add(DiagnosticsProperty('account', account))
      ..add(DiagnosticsProperty('organization', organization))
      ..add(DiagnosticsProperty('orgs', orgs));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CoreState &&
            (identical(other.account, account) ||
                const DeepCollectionEquality()
                    .equals(other.account, account)) &&
            (identical(other.organization, organization) ||
                const DeepCollectionEquality()
                    .equals(other.organization, organization)) &&
            (identical(other.orgs, orgs) ||
                const DeepCollectionEquality().equals(other.orgs, orgs)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(account) ^
      const DeepCollectionEquality().hash(organization) ^
      const DeepCollectionEquality().hash(orgs);

  @override
  _$CoreStateCopyWith<_CoreState> get copyWith =>
      __$CoreStateCopyWithImpl<_CoreState>(this, _$identity);
}

abstract class _CoreState extends CoreState {
  _CoreState._() : super._();
  factory _CoreState(
      {Account account,
      Organization organization,
      List<AbridgedOrganization> orgs}) = _$_CoreState;

  @override
  Account get account;
  @override
  Organization get organization;
  @override
  List<AbridgedOrganization> get orgs;
  @override
  _$CoreStateCopyWith<_CoreState> get copyWith;
}
