import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/models/data/abridged_organization/abridged_organization.dart';
import 'package:recovery_app/domain/models/data/account/account.dart';
import 'package:recovery_app/domain/models/data/organization/organization.dart';
import 'package:recovery_app/domain/models/util/message/message.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/domain/services/local_storage.dart';
import 'package:recovery_app/domain/services/notification_service.dart';
import 'package:recovery_app/domain/services/organization_service.dart';
import 'package:recovery_app/domain/services/user_service.dart';
import 'package:recovery_app/domain/core/extensions/nav_extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:recovery_app/domain/utilities/nav_routes.dart';
import 'package:wakelock/wakelock.dart';

part 'core_state.dart';

part 'core_cubit.freezed.dart';

class CoreCubit extends Cubit<CoreState> {
  final UserService userService;
  final OrganizationService organizationService;
  final LocalStorage localStorage;
  final NotificationService notificationService;
  final GlobalKey<NavigatorState> navKey;
  final FeedbackCubit feedback;

  CoreCubit({
    @required this.navKey,
    @required this.userService,
    @required this.organizationService,
    @required this.localStorage,
    @required this.notificationService,
    @required this.feedback,
  }) : super(CoreState()) {
    notificationService.$refreshReminder.listen(_refreshControl);
    userService.$authStatus.listen(_authControl);
    organizationService.http.$connection.listen(_connectionControl);
  }

  void _refreshControl(value) {
    if (value == RefreshValue.organization) reload();
  }

  NavigatorState get nav => navKey.currentState;

  void _connectionControl(ConnectionStatus status) {
    if (status == ConnectionStatus.noInternet ||
        status == ConnectionStatus.noBackend) {
      nav.pushNamed(NavRoutes.noConnection);
    }
  }

  Future<void> _authControl(AuthStatus authStatus) {
    return feedback.controlled(
      () async {
        // check connection
        final status = await organizationService.http.checkConnection();
        if (status != ConnectionStatus.connected) return;

        // if signed out
        if (authStatus == AuthStatus.signedOut) {
          if (state.orgs == null) await refreshOrganizationList();

          nav.popAllAndPushNamed(NavRoutes.auth);
          return;
        }
        
        // all good - go to main screen
        await reload();
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
  }

  Future<void> refreshOrganizationList() async {
    final orgs = await organizationService.fetchOrgRefs();
    if (orgs != null) emit(state.copyWith(orgs: orgs));
  }

  Future<void> reload() async {
    return feedback.controlled(
      () async {
        // get org id
        final orgId = await localStorage.read(DataKey.userOrganization);
        if (orgId == null) return userService.signOut();
        
        // get account
        final account = await userService.getAccount(orgId);
        if (account == null) return userService.signOut();

        // get organization
        final organization = await organizationService.getByRef(account.orgRef);
        if (organization == null) return;

        _setScreenLock(account.isDisplayAlwaysOn);

        await Future.wait([
          notificationService.updateToken(account),
          userService.sendHeartbeat(account.orgRef.id),
        ]);

        emit(state.copyWith(
          account: account,
          organization: organization,
        ));

        nav.pushNamedAndRemoveUntil(NavRoutes.home, (_) => false);

        if (state.account.needsRegistration) {
          nav.pushNamed(NavRoutes.settings);
        }
      },
      shouldRethrow: false,
      withLoading: true,
      displayError: true,
    );
  }

  void _setScreenLock(bool displayOn) {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if (displayOn) {
        Wakelock.enable();
      } else {
        Wakelock.disable();
      }
    });
  }

  Future<void> tryReconnect() async {
    feedback.controlled(
      () async {
        final status = await organizationService.http.checkConnection();
        if (status != ConnectionStatus.connected) {
          feedback.addMessage(Message.error('Still nothing :('));
        } else {
          await _authControl(userService.authStatus);
        }
      },
      shouldRethrow: false,
      withLoading: false,
      displayError: true,
    );
  }
}
