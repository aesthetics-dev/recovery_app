part of 'core_cubit.dart';

@freezed
abstract class CoreState implements _$CoreState {
  factory CoreState({
    Account account,
    Organization organization,
    List<AbridgedOrganization> orgs,
  }) = _CoreState;

  const CoreState._();
}
