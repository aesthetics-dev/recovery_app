import 'package:flutter/material.dart';

typedef RemovedItemBuilder<T> = Widget Function(
  T item,
  BuildContext context,
  Animation<double> animation,
);

class AnimatedListController<T> {
  AnimatedListController({
    @required this.listKey,
    @required this.removedItemBuilder,
    List<T> initialItems = const [],
  }) {
    updateList(initialItems);
  }

  final List<T> items = [];
  final GlobalKey<AnimatedListState> listKey;
  final RemovedItemBuilder<T> removedItemBuilder;

  AnimatedListState get listState => listKey.currentState;

  int get length => items.length;

  int indexOf(T data) => items.indexOf(data);

  void updateList(List<T> newList) {
    final exists = items.asMap().map((_, value) => MapEntry(value, 1));

    int curr = items.length - 1;
    for (final newItem in newList.reversed) {
      if (exists[newItem] != 1) {
        addAt(curr + 1, newItem);
        continue;
      }

      while (curr != -1 && newItem != items[curr]) {
        exists[items[curr]] = null;
        removeAt(curr);
        curr--;
      }

      if (curr == -1) {
        addAt(0, newItem);
      } else {
        curr--;
      }
    }

    while (newList.length != items.length) {
      removeAt(0);
    }
    // for (int i = 0; i < newList.length; i++) {
    //   final newItem = newList[i];

    //   while (i != items.length && newItem != items[i]) {
    //     removeAt(i);
    //   }

    //   if (i == items.length) {
    //     addAt(i, newItem);
    //   }
    // }

    // while (newList.length != items.length) {
    //   removeAt(newList.length);
    // }
  }

  void removeAt(int index) {
    final item = items.removeAt(index);
    listState.removeItem(index,
        (context, animation) => removedItemBuilder(item, context, animation));
  }

  void addAt(int index, T data) {
    items.insert(index, data);
    listState.insertItem(index);
  }
}
