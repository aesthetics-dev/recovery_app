import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:recovery_app/domain/services/auth_service.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/domain/services/notification_service.dart';
import 'package:recovery_app/domain/services/tts_service.dart';
import 'package:recovery_app/domain/services/task_service.dart';
import 'package:recovery_app/domain/services/user_service.dart';
import 'package:recovery_app/domain/services/local_storage.dart';
import 'package:recovery_app/domain/services/organization_service.dart';

Future<GetIt> init() async {
  final GetIt sl = GetIt.instance;

  await Firebase.initializeApp();

  // auth
  sl.registerSingleton(FirebaseMessaging());

  sl.registerSingleton(FirebaseAuth.instance);
  sl.registerSingleton(FirebaseDynamicLinks.instance);

  // util
  sl.registerSingleton(http.Client());
  sl.registerSingleton(AuthService(auth: sl()));
  sl.registerSingleton(TTSService());
  sl.registerSingleton(HttpController(authService: sl(), http: sl()));

  //! Services
  sl.registerSingleton(NotificationService(messaging: sl(), http: sl()));
  sl.registerSingleton(LocalStorage());
  sl.registerSingleton(TaskService(authUtility: sl(), http: sl()));

  sl.registerSingleton(OrganizationService(
    http: sl(),
    authUtility: sl(),
  ));

  sl.registerSingleton(UserService(
    auth: sl(),
    localStorage: sl(),
    dynamicLinks: sl(),
    http: sl(),
    authUtility: sl(),
  ));

  //! Global Keys
  sl.registerSingleton(GlobalKey());

  return sl;
}

T getInstance<T>() => GetIt.instance<T>();
