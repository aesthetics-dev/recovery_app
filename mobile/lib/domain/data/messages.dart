import 'package:recovery_app/domain/models/util/message/message.dart';

class Messages {
  //! General
  static final Message success = Message.success('Done!');
  static final Message error = Message.error('Something went wrong!');

  //! Login
  static final Message unrecognizedEmail =
      Message.error('Email not recognized by the organization.');
  static final Message invalidEmailOrPassword =
      Message.error('Invalid email or password.');
  static final Message badEmailFormat =
      Message.error('Please enter a valid email');
  static final Message badPasswordFormat =
      Message.error('Please enter a password');
  static final Message noOrganizationSelected =
      Message.error('Please select an organization');
  static final Message couldNotFetchOrganizations = Message.error(
      'Could not get a list of organizations! Are you connected to the internet?');
  static final Message couldNotSendSignInLink =
      Message.error('Could not send a sign in link to the email address :(');
  static final Message couldNotSignIn = Message.error(
      "Something went wrong in the sign in process! Please try again later!");
  static final Message outdatedSigninLink =
      Message.error('Outdated authentication link.');

  //! Other
  static final Message couldNotRemoveTask =
      Message.error("Could not remove task!");
  static final Message couldNotUpdateStats =
      Message.error("Could not update stats!");

  //! Settings
  static final Message selectLocation =
      Message.error("Please select a location");
  static final Message selectRole = Message.error("Please select a role");
  static final Message nameCantBeEmpty =
      Message.error("Please enter your name");

  //! Forms
  static Message cantBeEmpty(String objectName) =>
      Message.error('$objectName cannot be empty.');

  static Message alreadyExists(String objectName) =>
      Message.error('$objectName already exists.');

  static Message added(String objectName) =>
      Message.success('$objectName added!');

  static Message removed(String objectName) =>
      Message.success('$objectName removed!');
}
