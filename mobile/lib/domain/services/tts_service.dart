import 'package:flutter_tts/flutter_tts.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';

class TTSService {
  final FlutterTts _tts;

  TTSService() : _tts = FlutterTts() {
    _tts.setVoice('Aaron');
    _tts.setIosAudioCategory(
      IosTextToSpeechAudioCategory.ambient,
      [IosTextToSpeechAudioCategoryOptions.mixWithOthers],
    );
    _tts.setVolume(1);
  }

  Future speak(String words) async {
    await _tts.speak(words);
  }

  Future taskUpdate(Task task) async {
    speak(
      'An update for ${task.locRef.name} ${task.bedRef.name}',
    );
  }

  Future taskInsert(Task task) async {
    speak(
      '${task.allTags.join(', ')} in ${task.locRef.name} ${task.bedRef.name}',
    );
  }
}
