import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http_lib;
import 'package:meta/meta.dart';
import 'package:recovery_app/domain/services/auth_service.dart';
import 'package:recovery_app/domain/services/url_paths.dart';
import 'package:recovery_app/domain/utilities/log.dart';
import 'package:recovery_app/domain/utilities/utility_types.dart';
import 'package:rxdart/subjects.dart';
import 'package:recovery_app/domain/core/extensions/http_extensions.dart';

class HttpController {
  AuthService authService;
  final http_lib.Client http;
  // ignore: close_sinks
  final BehaviorSubject<ConnectionStatus> _connection =
      BehaviorSubject.seeded(ConnectionStatus.unknown);

  DateTime lastConnectionCheck = DateTime(2000);
  Future<void> currentCheck;

  ConnectionStatus get connection => _connection.value;

  Stream<ConnectionStatus> get $connection => _connection.distinct();

  final timeLimit = Duration(seconds: 15);

  HttpController({@required this.http, @required this.authService}) {
    checkConnection();
  }

  Future<Map<String, String>> getHeader({bool authorize = true}) async {
    return {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      HttpHeaders.authorizationHeader: authorize ? await getToken() : null
    };
  }

  Future<String> getToken() async {
    final user = authService.auth.currentUser;

    if (user == null) {
      return null;
    }

    final token = await user.getIdToken();

    return token != null ? "Bearer $token" : null;
  }

  Future<T> parseResponse<T>(
    RequestParams params,
    http_lib.StreamedResponse response, {
    FromJson<T> parser,
  }) async {
    if (params.checkStatus &&
        (response.statusCode < 200 || response.statusCode > 299)) {
      return null;
    }

    final bodyString = await response.stream.bytesToString();
    if (bodyString == null || bodyString.isEmpty) {
      return null;
    }

    final bodyMap = jsonDecode(bodyString);

    return parser != null
        ? parser(bodyMap as Map<String, dynamic>)
        : bodyMap as T;
  }

  Future<T> makeRequest<T>(
    RequestParams params, {
    FromJson<T> parser,
  }) async {
    try {
      final request = http_lib.Request(params.reqType, Uri.parse(params.uri));
      request.headers.addAll(await getHeader(authorize: params.withAuth));
      if (params.body != null) request.body = jsonEncode(params.body);
      final response = await http.send(request);
      return parseResponse(params, response, parser: parser);
    } catch (error) {
      Log.error(error, 'HttpController makeRequest');
      return null;
    }
  }

  Future<List<T>> makeArrayRequest<T>(
    RequestParams params, {
    TypeSwitch<dynamic, T> parser,
    String key = 'data',
  }) async {
    try {
      final data = await makeRequest<Map<String, dynamic>>(params);
      return (data[key] as List).map((e) => parser(e)).toList();
    } catch (error) {
      return null;
    }
  }

  Future<T> fetchObject<T>(
    String url,
    FromJson<T> fromJson, {
    bool withAuth = false,
    T errorFallback,
  }) async {
    if (!connected) {
      return errorFallback;
    }
    try {
      final response = await http
          .get(
            url,
            headers: withAuth ? await getHeader() : null,
          )
          .timeout(timeLimit);

      final data = response.verify().parseObject<T>(constructor: fromJson);
      return data;
    } catch (error) {
      Log.error(error);
      await checkConnection();

      return errorFallback;
    }
  }

  bool get connected => connection == ConnectionStatus.connected;

  Future<List<T>> fetchObjectList<T>(
    String url,
    FromJson<T> fromJson, {
    bool withAuth = false,
    List<T> errorFallback,
    String listKey = 'data',
  }) async {
    return fetchObject<List<T>>(
      url,
      (json) => (json[listKey] as List)
          ?.map<T>((v) => fromJson(v as Map<String, dynamic>))
          ?.toList(),
      errorFallback: errorFallback,
      withAuth: withAuth,
    );
  }

  Future<http_lib.Response> putObject(
    String url,
    Map<String, dynamic> body, {
    bool withAuth = false,
  }) async {
    if (!connected) {
      return null;
    }

    try {
      final response = await http
          .put(
            url,
            headers: await getHeader(authorize: withAuth),
            body: json.encode(body),
          )
          .timeout(timeLimit);
      return response.verify();
    } catch (error) {
      Log.error(error);
      await checkConnection();
      return null;
    }
  }

  Future<bool> deleteObject(
    String url, {
    bool withAuth = false,
  }) async {
    if (!connected) {
      return false;
    }

    try {
      final response = await http
          .delete(
            url,
            headers: withAuth ? await getHeader() : null,
          )
          .timeout(timeLimit);

      response.verify();
      return true;
    } catch (error) {
      Log.error(error);
      await checkConnection();
      return false;
    }
  }

  Future<void> _testConnection(String url, Function(bool success) callback) =>
      http.get(url).timeout(Duration(seconds: 10)).then(
            (_) => callback(true),
            onError: (_) => callback(false),
          );

  Future<ConnectionStatus> checkConnection({bool force = false}) async {
    final DateTime now = DateTime.now();
    final testTime = now.subtract(Duration(seconds: 5));

    if (currentCheck != null) {
      await currentCheck;
      return connection;
    }

    if (!force && lastConnectionCheck.isAfter(testTime)) {
      return connection;
    }

    bool google;
    bool backend;
    currentCheck = Future.wait([
      _testConnection(UrlPaths.baseUrl, (r) => backend = r),
      _testConnection('https://google.com', (r) => google = r),
    ]);

    await currentCheck;
    currentCheck = null;
    lastConnectionCheck = DateTime.now();
    ConnectionStatus out = ConnectionStatus.connected;
    if (!google) out = ConnectionStatus.noInternet;
    if (!backend) out = ConnectionStatus.noBackend;
    _connection.add(out);
    return out;
  }
}

enum ConnectionStatus {
  noInternet,
  noBackend,
  connected,
  unknown,
}

@immutable
class RequestParams {
  final String reqType;
  final String uri;
  final Map<String, dynamic> body;
  final bool withAuth;
  final bool checkStatus;

  const RequestParams(
    this.reqType,
    this.uri, {
    this.body,
    this.withAuth = false,
    this.checkStatus = true,
  });
}
