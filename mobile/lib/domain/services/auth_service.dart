import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  FirebaseAuth auth;

  AuthService({this.auth});

  Future<Map<String, String>> getHeader({bool authorize = true}) async {
    return {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      HttpHeaders.authorizationHeader: authorize ? await getToken() : ''
    };
  }

  Future<String> getToken() async {
    final user = auth.currentUser;
    final token = await user?.getIdToken();
    return token != null ? "Bearer $token" : null;
  }
}
