import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:meta/meta.dart';
import 'package:recovery_app/domain/models/data/account/account.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/domain/services/url_paths.dart';
import 'package:recovery_app/domain/utilities/log.dart';
import 'package:rxdart/subjects.dart';

class NotificationService {
  final FirebaseMessaging messaging;
  final HttpController http;
  bool messagesConfigured = false;

  final BehaviorSubject<RefreshValue> $refreshReminder =
      BehaviorSubject.seeded(null);

  NotificationService({
    @required this.messaging,
    @required this.http,
  });

  Future<void> requestPermissions() async {
    await messaging.requestNotificationPermissions();
  }

  void subscribeToMessages() {
    if (messagesConfigured) return;

    messaging.configure(
      onMessage: (message) async {
        Log.info(message, 'NotificationService onMessage callback');
        $refreshReminder.add(
          _encodedRefreshValues[Platform.isIOS
              ? message['shouldRefresh']
              : (message['data'] ?? {})['shouldRefresh']],
        );
      },
    );
  }

  Future<void> updateToken(Account acc) async {
    await requestPermissions();
    subscribeToMessages();

    final token = await messaging.getToken();
    await http.putObject(
      UrlPaths.token(orgId: acc.orgRef.id),
      {"token": token},
      withAuth: true,
    );
  }

  Future<void> deleteToken(Account acc) async {
    final token = await messaging.getToken();
    await http.makeRequest(
      RequestParams(
        'DELETE',
        UrlPaths.token(orgId: acc.orgRef.id),
        withAuth: true,
        body: {"token": token},
      ),
    );
  }
}

enum RefreshValue {
  organization,
  tasks,
}

final _encodedRefreshValues = {
  'organization': RefreshValue.organization,
  'tasks': RefreshValue.tasks,
};
