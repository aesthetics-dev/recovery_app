import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:package_info/package_info.dart';
import 'package:recovery_app/domain/data/messages.dart';
import 'package:recovery_app/domain/models/data/account/account.dart';
import 'package:recovery_app/domain/models/core/response.dart';
import 'package:recovery_app/domain/models/data/heartbeat/heartbeat.dart';
import 'package:recovery_app/domain/models/data/logout_trace/logout_trace.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/util/message/message.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/domain/services/local_storage.dart';
import 'package:recovery_app/domain/services/url_paths.dart';
import 'package:recovery_app/utils/constants.dart';
import 'package:rxdart/subjects.dart';

import 'auth_service.dart';

class UserService {
  final FirebaseAuth auth;
  final LocalStorage localStorage;
  final FirebaseDynamicLinks dynamicLinks;
  final HttpController http;
  final AuthService authUtility;
  final logger = Logger();

  final _authStatus = BehaviorSubject.seeded(AuthStatus.unknown);
  Stream<AuthStatus> get $authStatus => _authStatus.distinct();
  AuthStatus get authStatus => _authStatus.value;

  final $signingIn = BehaviorSubject.seeded(false);

  UserService({
    @required this.auth,
    @required this.localStorage,
    @required this.dynamicLinks,
    @required this.http,
    @required this.authUtility,
  }) {
    auth
        .authStateChanges()
        .listen((user) => _authStatus.add(_toAuthStatus(user)));
    _authStatus.add(_toAuthStatus(auth.currentUser));
  }

  /// Fetches the account with remote and local parameters
  Future<Account> getAccount(String orgId) async {
    final acc = await http.fetchObject(
      UrlPaths.acc(orgId: orgId),
      (val) => Account.fromJson(val),
      withAuth: true,
    );

    final alwaysOn = await localStorage
        .read(DataKey.isDisplayAlwaysOn)
        .then((r) => r == 'true');
    final voiceOn = await localStorage
        .read(DataKey.areVoiceAnnouncementsOn)
        .then((r) => r == 'true');

    return acc?.copyWith(
      isDisplayAlwaysOn: alwaysOn,
      areVoiceAnnouncementsOn: voiceOn,
    );
  }

  /// Updates server and local storage account params
  Future<void> writeAccount(Account account) async {
    await Future.wait([
      http.putObject(
        UrlPaths.acc(orgId: account.orgRef.id),
        account.toJson(),
        withAuth: true,
      ),
      localStorage.write(
        DataKey.isDisplayAlwaysOn,
        account.isDisplayAlwaysOn.toString(),
      ),
      localStorage.write(
        DataKey.areVoiceAnnouncementsOn,
        account.areVoiceAnnouncementsOn.toString(),
      ),
    ]);
  }

  /// Signs the user out via Firebase, sends trace to backend, and clears cache
  Future<void> signOut() async {
    final account = auth.currentUser;
    if (account == null) return;
    final org = await localStorage.read(DataKey.userOrganization);
    if (org != null) await sendSignoutTrace(org);
    await Future.wait([
      auth.signOut(),
      localStorage.clear(),
    ]);
  }

  Future<void> sendSignoutTrace(String orgId) {
    final trace = LogoutTrace(logoutMethod: 'manual');
    return http.makeRequest(
      RequestParams(
        'POST',
        UrlPaths.logoutTrace(orgId: orgId),
        withAuth: true,
        body: {
          "trace": trace.toJson(),
        },
      ),
    );
  }

  /// Attempts to sign in with a dynamic link
  ///
  /// Throws [Message] if an error occurs
  Future<void> trySignIn(PendingDynamicLinkData dynamicLink) async {
    final Uri deepLink = dynamicLink?.link;

    if (deepLink == null) return;

    $signingIn.add(true);
    final email = await localStorage.read(DataKey.signInEmail);
    if (email == null) return Messages.couldNotSignIn;

    final link = deepLink.toString();

    try {
      await FirebaseAuth.instance.signInWithEmailLink(
        email: email,
        emailLink: link,
      );
    } catch (error) {
      throw Messages.outdatedSigninLink;
    }
  }

  Future<EmailStatus> checkEmailInDomain(
    Reference organization,
    String email,
  ) async {
    final response = await http.makeRequest(
      RequestParams(
        'put',
        UrlPaths.domainCheck(orgId: organization.id),
        body: {"email": email},
      ),
      parser: (v) => v['data'] == true,
    );
    return response == true ? EmailStatus.valid : EmailStatus.invalid;
  }

  /// Uses Firebase to send a sign in link to the given email
  ///
  /// Caches the email and organization for future use. Throws an error if
  /// there is an issue sending the email or caching
  Future<Response<void>> sendSignInLink(
    Reference organization,
    String email,
  ) async {
    final trimmedEmail = email.trim().toLowerCase();
    await auth.sendSignInLinkToEmail(
      email: trimmedEmail,
      actionCodeSettings: ActionCodeSettings(
        url: SystemConstants.authenticationLink,
        handleCodeInApp: true,
        androidInstallApp: true,
        iOSBundleId: SystemConstants.bundleID,
        androidPackageName: SystemConstants.bundleID,
        androidMinimumVersion: SystemConstants.minAndroidVersion,
      ),
    );

    await Future.wait([
      localStorage.write(DataKey.signInEmail, email),
      localStorage.write(DataKey.userOrganization, organization.id),
    ]);

    return Response.success();
  }

  /// Uses the Firebase API to sign in with email and password
  ///
  /// May throw an error if something goes wrong
  Future<void> signInWithEmailAndPassword(
    String orgId,
    String email,
    String password,
  ) async {
    await auth.signInWithEmailAndPassword(email: email, password: password);
    await localStorage.write(DataKey.userOrganization, orgId);

    return Response.success();
  }

  Future<void> sendHeartbeat(String orgId) async {
    try {
      final info = await PackageInfo.fromPlatform();

      final hb = Heartbeat(
        clientOs: Platform.isIOS ? 'ios' : 'android',
        clientVersion: info.version,
        clientBuildNumber: info.buildNumber,
      );

      await http.makeRequest(
        RequestParams(
          'POST',
          UrlPaths.heartbeat(orgId: orgId),
          body: {"heartbeat": hb.toJson()},
          withAuth: true,
        ),
      );
    } catch (e) {
      logger.e(e);
    }
  }
}

enum EmailStatus { valid, invalid }
enum AuthStatus { unknown, signedIn, signedOut }
AuthStatus _toAuthStatus(User user) =>
    user == null ? AuthStatus.signedOut : AuthStatus.signedIn;
