import 'package:flutter/material.dart';

class UrlPaths {
  // static const String host = 'http://localhost:3000';
  // static const String host = 'http://192.168.0.15:3000';
  static const String host = 'https://app.mdaware.us';
  // static const String host = 'http://172.20.10.9:3000';

  static const String baseUrl = '$host/api';

  static String org({String orgId = ''}) => '$baseUrl/org/$orgId';

  static String loc({String orgId = '', String locId = ''}) =>
      '$baseUrl/org/$orgId/loc/$locId';

  static String bed(
          {String orgId = '', String locId = '', String bedId = ''}) =>
      '$baseUrl/org/$orgId/loc/$locId/bed/$bedId';

  static String beds({String orgId = '', String locId = ''}) =>
      '$baseUrl/org/$orgId/loc/$locId/beds';

  static String orgs() => '$baseUrl/orgs';

  static String acc({@required String orgId}) => '${org(orgId: orgId)}/acc';

  static String heartbeat({@required String orgId}) =>
      '${acc(orgId: orgId)}/heartbeat';

  static String logoutTrace({@required String orgId}) =>
      '${acc(orgId: orgId)}/logout';

  static String token({@required String orgId}) => '${acc(orgId: orgId)}/token';

  static String tasks({@required String orgId}) => '${org(orgId: orgId)}/tasks';

  static String historicTasks({@required String orgId}) =>
      '${tasks(orgId: orgId)}/historic';

  static String taskStats({@required String orgId}) =>
      '${tasks(orgId: orgId)}/stats';

  static String domainCheck({@required String orgId}) =>
      '$baseUrl/email-in-domain/$orgId';
}
