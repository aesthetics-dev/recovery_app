import 'package:meta/meta.dart';
import 'package:recovery_app/domain/models/data/location_task_average/location_task_average.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/domain/services/url_paths.dart';

import 'auth_service.dart';

class TaskService {
  final HttpController http;
  final AuthService authUtility;

  TaskService({
    @required this.http,
    @required this.authUtility,
  });

  Future<List<Task>> getForAccount(
    Reference orgRef, {
    bool historic = false,
  }) async {
    return http.fetchObjectList(
      historic
          ? UrlPaths.historicTasks(orgId: orgRef.id)
          : UrlPaths.tasks(orgId: orgRef.id),
      (json) => Task.fromJson(json),
      withAuth: true,
    );
  }

  Future<List<LocationTaskAverage>> getStats(Reference orgRef) async {
    return http.fetchObjectList(
      UrlPaths.taskStats(orgId: orgRef.id),
      (json) => LocationTaskAverage.fromJson(json),
      withAuth: true,
    );
  }

  Future<bool> saveTask(Task task) async {
    final response = http.putObject(
      UrlPaths.tasks(orgId: task.orgRef.id),
      task.toJson(),
      withAuth: true,
    );
    return response != null;
  }

  Stream<String> changedLocation() async* {}

  Future<void> removeTask(Task task) async {
    return saveTask(task.copyWith(tags: [], customTag: ''));
  }
}
