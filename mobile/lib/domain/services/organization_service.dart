import 'package:meta/meta.dart';
import 'package:recovery_app/domain/models/data/abridged_organization/abridged_organization.dart';
import 'package:recovery_app/domain/models/data/organization/organization.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/domain/services/url_paths.dart';

import 'auth_service.dart';

class OrganizationService {
  final HttpController http;
  final AuthService authUtility;

  OrganizationService({
    @required this.http,
    @required this.authUtility,
  });

  Future<Organization> getByRef(Reference orgRef) async {
    return http.fetchObject(
      UrlPaths.org(orgId: orgRef.id),
      (json) => Organization.fromJson(json),
      withAuth: true,
    );
  }

  Future<List<AbridgedOrganization>> fetchOrgRefs() async {
    return http.fetchObjectList(
      UrlPaths.orgs(),
      (v) => AbridgedOrganization.fromJson(v),
      withAuth: true,
    );
  }

  Future<void> updateOrganization(Organization org) async {
    return http.putObject(
      UrlPaths.org(orgId: org.ref.id),
      org.toJson(),
      withAuth: true,
    );
  }
}
