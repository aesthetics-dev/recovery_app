import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:recovery_app/domain/models/core/response.dart';

class LocalStorage {
  final _storage = const FlutterSecureStorage();

  Future<String> read(DataKey key) async {
    if (key == null) return null;
    final data = await _storage.read(key: key.toString());
    return data;
  }

  Future<Response<void>> write(DataKey key, String data) async {
    try {
      await _storage.write(key: key.toString(), value: data);
      return Response.success();
    } catch (error) {
      return Response.error();
    }
  }

  Future<Response<void>> clear() async {
    try {
      await _storage.deleteAll();
      return Response.success();
    } catch (error) {
      return Response.error();
    }
  }
}

enum DataKey {
  signInEmail,
  userOrganization,
  lastLocation,
  areVoiceAnnouncementsOn,
  isDisplayAlwaysOn,
}
