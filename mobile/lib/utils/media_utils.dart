import 'package:flutter/material.dart';

class MediaUtils {
  BuildContext context;

  MediaUtils(this.context);

  bool get isBigScreen => MediaQuery.of(context).size.shortestSide > 700;
}
