class SystemConstants {
  static const String bundleID = 'com.aes.mdaware';
  static const String minAndroidVersion = '21';
  static const String authenticationLink = 'https://mdaware.page.link/bAmq';
}
