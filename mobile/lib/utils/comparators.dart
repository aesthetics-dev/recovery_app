import 'package:collection/collection.dart';

bool listsEq(List l1, List l2) {
  return DeepCollectionEquality.unordered().equals(l1, l2);
}
