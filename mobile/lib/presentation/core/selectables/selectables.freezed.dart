// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'selectables.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SelectableTearOff {
  const _$SelectableTearOff();

// ignore: unused_element
  _Selectable<T> call<T>(T data, {bool selected}) {
    return _Selectable<T>(
      data,
      selected: selected,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Selectable = _$SelectableTearOff();

/// @nodoc
mixin _$Selectable<T> {
  T get data;
  bool get selected;

  $SelectableCopyWith<T, Selectable<T>> get copyWith;
}

/// @nodoc
abstract class $SelectableCopyWith<T, $Res> {
  factory $SelectableCopyWith(
          Selectable<T> value, $Res Function(Selectable<T>) then) =
      _$SelectableCopyWithImpl<T, $Res>;
  $Res call({T data, bool selected});
}

/// @nodoc
class _$SelectableCopyWithImpl<T, $Res>
    implements $SelectableCopyWith<T, $Res> {
  _$SelectableCopyWithImpl(this._value, this._then);

  final Selectable<T> _value;
  // ignore: unused_field
  final $Res Function(Selectable<T>) _then;

  @override
  $Res call({
    Object data = freezed,
    Object selected = freezed,
  }) {
    return _then(_value.copyWith(
      data: data == freezed ? _value.data : data as T,
      selected: selected == freezed ? _value.selected : selected as bool,
    ));
  }
}

/// @nodoc
abstract class _$SelectableCopyWith<T, $Res>
    implements $SelectableCopyWith<T, $Res> {
  factory _$SelectableCopyWith(
          _Selectable<T> value, $Res Function(_Selectable<T>) then) =
      __$SelectableCopyWithImpl<T, $Res>;
  @override
  $Res call({T data, bool selected});
}

/// @nodoc
class __$SelectableCopyWithImpl<T, $Res>
    extends _$SelectableCopyWithImpl<T, $Res>
    implements _$SelectableCopyWith<T, $Res> {
  __$SelectableCopyWithImpl(
      _Selectable<T> _value, $Res Function(_Selectable<T>) _then)
      : super(_value, (v) => _then(v as _Selectable<T>));

  @override
  _Selectable<T> get _value => super._value as _Selectable<T>;

  @override
  $Res call({
    Object data = freezed,
    Object selected = freezed,
  }) {
    return _then(_Selectable<T>(
      data == freezed ? _value.data : data as T,
      selected: selected == freezed ? _value.selected : selected as bool,
    ));
  }
}

/// @nodoc
class _$_Selectable<T> extends _Selectable<T> {
  _$_Selectable(this.data, {this.selected})
      : assert(data != null),
        super._();

  @override
  final T data;
  @override
  final bool selected;

  @override
  String toString() {
    return 'Selectable<$T>(data: $data, selected: $selected)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Selectable<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)) &&
            (identical(other.selected, selected) ||
                const DeepCollectionEquality()
                    .equals(other.selected, selected)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(data) ^
      const DeepCollectionEquality().hash(selected);

  @override
  _$SelectableCopyWith<T, _Selectable<T>> get copyWith =>
      __$SelectableCopyWithImpl<T, _Selectable<T>>(this, _$identity);
}

abstract class _Selectable<T> extends Selectable<T> {
  _Selectable._() : super._();
  factory _Selectable(T data, {bool selected}) = _$_Selectable<T>;

  @override
  T get data;
  @override
  bool get selected;
  @override
  _$SelectableCopyWith<T, _Selectable<T>> get copyWith;
}
