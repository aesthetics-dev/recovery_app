import 'package:collection/collection.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'selectables.freezed.dart';

@freezed
abstract class Selectable<T> implements _$Selectable<T> {
  factory Selectable(
    T data, {
    bool selected,
  }) = _Selectable<T>;

  const Selectable._();

  Selectable<T> toggled() => copyWith(selected: !selected);
}

class Selectables<T> {
  final List<Selectable<T>> items;

  const Selectables(this.items);

  factory Selectables.fromData({
    @required List<T> all,
    @required List<T> selected,
    int Function(T a, T b) comparator,
  }) {
    if (comparator != null) {
      all = [...all];
      all.sort(comparator);
    }

    return Selectables(
      UnmodifiableListView(all
          .map(
            (item) => Selectable(
              item,
              selected: selected.any((element) => element == item),
            ),
          )
          .toList()),
    );
  }

  Selectables<T> toggle(T data) => Selectables<T>(
      items.map((item) => item.data == data ? item.toggled() : item).toList());

  Selectables<T> select(T data) => Selectables<T>(items
      .map((item) => Selectable<T>(item.data, selected: item.data == data))
      .toList());

  List<T> get selected =>
      items.where((item) => item.selected).map((item) => item.data).toList();
}
