import 'package:flutter/material.dart';

class MyTheme {
  static final pageTransition = PageTransitionsTheme(builders: const {
    TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder(),
    TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
  });

  static const sideMargin = 20.0;
  static EdgeInsets get wSideMargin =>
      EdgeInsets.symmetric(horizontal: sideMargin);

  static const sidePadding = EdgeInsets.symmetric(horizontal: sideMargin);

  static final modalShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
        topLeft: Radius.circular(15), topRight: Radius.circular(15)),
  );
}

class MyColors {
  static const Color primary = Color.fromARGB(255, 0, 49, 147);
  static const Color offBlack = Colors.black87;

  //* tasks
  static Color taskRecent = Color.fromARGB(206, 132, 228, 177);
  static Color taskOlder = Color.fromARGB(242, 253, 221, 107);
  static Color taskOld = Color.fromARGB(255, 249, 141, 134);

  // popups
  static Color error = Colors.red.shade400;
  static Color success = primary;
  static Color info = primary;

  static Color completeTask = Colors.green.shade400;

  static Color selectedButton = Colors.teal.shade500;
  static Color selectedButtonText = Colors.white;
  static Color notSelectedButton = Colors.teal.shade100;
  static Color notSelectedButtonText = Colors.black;
}

class MyTextStyles {
  static TextStyle capsulePageTitle = TextStyle(
    color: Colors.white,
    fontSize: 23,
    fontWeight: FontWeight.w700,
  );

  static final modalTitle = TextStyle(
    fontSize: 25,
    fontWeight: FontWeight.bold,
  );
}

class MyAssets {
  static const mainLogo = 'assets/logos/logo_no_bg.png';
  static const landscapeLogo = 'assets/logos/landscape_logo_no_bg.png';
  static const fontRobotoMono = 'Roboto_Mono';
}
