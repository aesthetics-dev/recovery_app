import 'package:flutter/material.dart';

abstract class ConfirmationDialog {
  static Future show(
    BuildContext context, {
    final String title = 'Are you sure?',
    final String noButton = 'No',
    final String yesButton = 'Yes',
    final String content = '',
    final Function() onYes,
    final Function() onNo,
  }) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          FlatButton(
            onPressed: () => _dismissAnd(context, onYes),
            child: Text(
              yesButton,
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
          ),
          FlatButton(
              onPressed: () => _dismissAnd(context, onNo),
              child: Text(noButton)),
        ],
      ),
    );
  }

  static void _dismissAnd(BuildContext context, Function() func) {
    Navigator.of(context).maybePop();
    func?.call();
  }
}
