import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/domain/models/data/location_task_average/location_task_average.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

void showTaskStatisticsModal({BuildContext context}) {
  final taskCub = context.read<TaskListCubit>();
  taskCub.updateStats();

  showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    shape: MyTheme.modalShape,
    builder: (context) => ConstrainedBox(
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height * .5),
      child: BlocProvider.value(
        value: taskCub,
        child: SingleChildScrollView(child: TaskStatisticsModal()),
      ),
    ),
  );
}

class TaskStatisticsModal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: MyTheme.sideMargin, vertical: 30),
      child: BlocBuilder<TaskListCubit, TaskListState>(
        builder: (context, state) {
          if (state.locationAverages == null) {
            return Container();
          }

          return Column(
            children: [
              Text(
                '7-Day Task Completion Average',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              ...state.locationAverages.map(_buildRow).toList(),
              _buildRow(_calculateAverage(state.locationAverages)),
              SizedBox(height: 40),
            ],
          );
        },
      ),
    );
  }

  LocationTaskAverage _calculateAverage(List<LocationTaskAverage> averages) {
    final avg =
        averages.fold<int>(0, (sum, avg) => sum + avg.secondsToComplete) /
            averages.length;

    return LocationTaskAverage(
      locRef: Reference(name: 'All Locations'),
      secondsToComplete: avg.toInt(),
    );
  }

  Widget _buildRow(LocationTaskAverage average) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                average.locRef.name,
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
            Text(
              _constructTimeString(average.secondsToComplete),
              style:
                  TextStyle(fontSize: 16, fontFamily: MyAssets.fontRobotoMono),
              textAlign: TextAlign.end,
            ),
          ],
        ),
        Divider(height: 10),
        SizedBox(height: 5),
      ],
    );
  }

  String _constructTimeString(int secondsToComplete) {
    final d = Duration(seconds: secondsToComplete);
    final hours = d.inHours;
    final minutes = d.inMinutes.remainder(60);
    final seconds = d.inSeconds.remainder(60);

    String out = '';
    if (hours > 0) out += '${hours}h ';
    if (out.isNotEmpty || minutes > 0) {
      out += '${minutes.toString().padLeft(2)}m ';
    }
    return out += '${seconds.toString().padLeft(2)}s';
  }
}
