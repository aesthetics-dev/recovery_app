import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/auth_cubit/auth_cubit.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/utilities/nav_utils.dart';
import 'package:recovery_app/domain/utilities/utility_functions.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/cool_button.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';

class EmailVerificationScreen extends StatefulWidget {
  static Key id = Key("EmailVerificationScreen");

  EmailVerificationScreen() : super(key: id);

  @override
  _EmailVerificationScreenState createState() =>
      _EmailVerificationScreenState();
}

class _EmailVerificationScreenState extends State<EmailVerificationScreen> {
  CountdownController _countdownController;
  static const int _coundownSeconds = 15;

  @override
  void initState() {
    super.initState();
    _countdownController = CountdownController();
  }

  @override
  Widget build(BuildContext context) {
    final signInCubit = context.watch<AuthCubit>();
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) => state.stage == AuthStage.infoEnter
          ? NavUtils.popIfCurrent(context)
          : null,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(FontAwesomeIcons.chevronLeft, color: MyColors.primary),
            onPressed: signInCubit.cancelEmailVerification,
          ),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildIcon(),
                _buildInfoLabel(signInCubit),
                CoolButton(
                  label: 'My Mail',
                  icon: Icon(
                    FontAwesomeIcons.solidEnvelope,
                    color: Colors.white,
                  ),
                  onPressed: UtilityFunctions.launchMail,
                ),
                SizedBox(height: 10),
                _buildCountdown(),
                SizedBox(height: 100),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCountdown() {
    return Column(
      children: [
        SizedBox(height: 20),
        Text(
          'Didnt get it? Check spam or resend:',
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
        ),
        SizedBox(height: 10),
        Countdown(
          seconds: _coundownSeconds,
          controller: _countdownController,
          build: (context, seconds) => SizedBox(
            width: 200,
            child: CoolButton(
              active: seconds == 0,
              onPressed: () => _onClickResend(context),
              label: seconds == 0
                  ? 'Resend!'
                  : 'Resend in: ${seconds.toInt().toString().padLeft(2)}',
              containerColor: MyColors.selectedButton,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _onClickResend(BuildContext context) async {
    final authCubit = context.read<AuthCubit>();
    final feedbackCubit = context.read<FeedbackCubit>();
    _countdownController.restart();
    _countdownController.pause();
    await authCubit.submitAuth();
    _countdownController.resume();
    feedbackCubit.addSuccess('Authentication link sent!');
  }

  Widget _buildInfoLabel(AuthCubit signInCubit) {
    final style = TextStyle(
      fontWeight: FontWeight.w500,
      color: Colors.black54,
      fontSize: 17,
    );
    // \n${signInCubit.state.email}
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            'An authentication link has been sent to',
            style: style,
            textAlign: TextAlign.center,
          ),
          Text(
            signInCubit.state.email,
            style: style.copyWith(fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10),
          Text(
            'In order to authenticate, the link must be opened on this device.',
            style: style.copyWith(fontSize: 13, color: Colors.black45),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Icon _buildIcon() {
    return Icon(
      FontAwesomeIcons.envelopeOpenText,
      size: 160,
      color: MyColors.primary.withAlpha(230),
    );
  }
}
