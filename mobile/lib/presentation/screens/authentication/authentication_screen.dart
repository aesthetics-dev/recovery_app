import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/domain/cubits/auth_cubit/auth_cubit.dart';
import 'package:recovery_app/domain/utilities/nav_routes.dart';
import 'package:recovery_app/presentation/widgets/logo.dart';
import 'package:recovery_app/presentation/widgets/cool_button.dart';

import ' widgets/auth_fields.dart';
import ' widgets/organization_selector.dart';

class AuthenticationScreen extends StatefulWidget {
  @override
  _AuthenticationScreenState createState() => _AuthenticationScreenState();
}

class _AuthenticationScreenState extends State<AuthenticationScreen> {
  @override
  void initState() {
    context.read<AuthCubit>().reset();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // signInCubit.add(AuthInitEvent());

    return BlocListener<AuthCubit, AuthState>(
      listener: (BuildContext context, AuthState state) {
        if (state.stage == AuthStage.emailVerification) {
          Navigator.of(context).pushNamed(NavRoutes.emailVerification);
        }
      },
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 350),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Logo(),
                  SizedBox(height: 20),
                  OrganizationSelector(),
                  SizedBox(height: 10),
                  AuthFields(),
                  SizedBox(height: 10),
                  _buildSubmitButton(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSubmitButton(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        return CoolButton(
          onPressed: context.watch<AuthCubit>().submitAuth,
          label: 'GO',
          active: state.isEmailValid,
          icon: Icon(
            FontAwesomeIcons.angleRight,
            color: Colors.white,
            size: 18,
          ),
        );
      },
    );
  }
}
