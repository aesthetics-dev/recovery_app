import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/auth_cubit/auth_cubit.dart';
import 'package:recovery_app/domain/models/data/abridged_organization/abridged_organization.dart';

class OrganizationSelector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final signInCubit = context.watch<AuthCubit>();

    return BlocBuilder<CoreCubit, CoreState>(
      builder: (context, state) {
        final dropdownItems = state.orgs
            ?.map((org) => DropdownMenuItem<AbridgedOrganization>(
                value: org, child: Text(org.ref.name)))
            ?.toList();

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: Material(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            elevation: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: DropdownButton<AbridgedOrganization>(
                isExpanded: true,
                value: signInCubit.state.organization,
                icon: Icon(FontAwesomeIcons.hospital),
                underline: Container(),
                style: TextStyle(color: Colors.black),
                hint: Text("Select an organization"),
                onChanged: signInCubit.selectOrganization,
                items: dropdownItems,
              ),
            ),
          ),
        );
      },
    );
  }
}
