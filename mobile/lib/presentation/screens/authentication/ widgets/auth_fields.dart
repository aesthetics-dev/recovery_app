import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/auth_cubit/auth_cubit.dart';
import 'package:recovery_app/domain/models/data/abridged_organization/abridged_organization.dart';
import 'package:recovery_app/domain/models/util/enums.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/presentation/widgets/cool_input.dart';

class AuthFields extends StatefulWidget {
  const AuthFields({Key key}) : super(key: key);

  @override
  _AuthFieldsState createState() => _AuthFieldsState();
}

class _AuthFieldsState extends State<AuthFields> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        if (state.organization != null) {
          _controller.forward();
        } else {
          _controller.reverse();
          return Container();
        }

        final signInCubit = context.watch<AuthCubit>();

        return SizeTransition(
          sizeFactor: _animation,
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              children: [
                _buildEmailInput(signInCubit),
                if (state.organization.authType == AuthType.password)
                  ..._buildPasswordInput(signInCubit)
              ],
            ),
          ),
        );
      },
    );
  }

  CoolInput _buildEmailInput(AuthCubit signInCubit) {
    return CoolInput(
      icon: Icon(
        FontAwesomeIcons.envelope,
        color: Colors.grey.shade800,
      ),
      iconSide: Side.right,
      inputParams: InputParams(
        onChange: signInCubit.setEmail,
        onSubmit: signInCubit.submitAuth,
        value: signInCubit.state.email,
        placeholder: 'Email',
      ),
      inputType: TextInputType.emailAddress,
    );
  }

  List<Widget> _buildPasswordInput(AuthCubit signInCubit) {
    return [
      SizedBox(height: 15),
      CoolInput(
        icon: Icon(
          FontAwesomeIcons.lock,
          color: Colors.grey.shade800,
        ),
        iconSide: Side.right,
        inputParams: InputParams(
          onChange: signInCubit.setPassword,
          onSubmit: signInCubit.submitAuth,
          value: signInCubit.state.password,
          placeholder: 'Password',
          obscureText: true,
        ),
        inputType: TextInputType.visiblePassword,
      ),
    ];
  }
}
