import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainPageHeaderButton extends StatelessWidget {
  final void Function() onTap;
  final IconData icon;

  const MainPageHeaderButton({
    Key key,
    @required this.onTap,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CoreCubit, CoreState>(
      builder: (BuildContext context, coreState) => Padding(
        padding: const EdgeInsets.only(right: MyTheme.sideMargin),
        child: GestureDetector(
          onTap: onTap,
          child: Icon(icon, size: 30),
        ),
      ),
    );
  }
}
