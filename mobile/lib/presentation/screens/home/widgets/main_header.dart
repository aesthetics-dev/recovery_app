import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/utilities/nav_routes.dart';
import 'package:recovery_app/presentation/screens/historic_tasks_list/historic_tasks_modal.dart';
import 'package:recovery_app/presentation/screens/home/widgets/main_page_header_button.dart';
import 'package:recovery_app/presentation/screens/task_statistics/task_statistics_modal.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'package:recovery_app/presentation/widgets/skeleton.dart';

class MainHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _buildNamePositionSettings(context),
          SizedBox(height: 5),
          _buildFollowedLocations(context),
        ],
      ),
    );
  }

  Row _buildNamePositionSettings(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _buildNameRoleSection(context),
        _buildSettingsHistoryButton(context),
      ],
    );
  }

  Widget _buildSettingsHistoryButton(BuildContext context) {
    return Row(
      children: [
        MainPageHeaderButton(
          icon: FontAwesomeIcons.chartBar,
          onTap: () => showTaskStatisticsModal(context: context),
        ),
        MainPageHeaderButton(
          icon: FontAwesomeIcons.history,
          onTap: () => showHistoricTasksModal(context: context),
        ),
        MainPageHeaderButton(
          icon: FontAwesomeIcons.cog,
          onTap: () => Navigator.of(context).pushNamed(NavRoutes.settings),
        ),
      ],
    );
  }

  Widget _buildNameRoleSection(BuildContext context) {
    return BlocBuilder<CoreCubit, CoreState>(
      builder: (context, state) {
        return CapsuleTitle(
          onShortTap: () => Navigator.of(context).pushNamed(NavRoutes.settings),
          position: CapsulePosition.left,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(state.account.ref.name,
                  style: CapsuleTitle.sectionTextStyle.copyWith(fontSize: 15)),
              Text(state.account?.role?.ref?.name ?? '',
                  style: CapsuleTitle.sectionTextStyle
                      .copyWith(fontSize: 14, fontWeight: FontWeight.w400)),
            ],
          ),
        );
      },
    );
  }

  Widget _buildFollowedLocations(BuildContext context) {
    return BlocBuilder<CoreCubit, CoreState>(
      builder: (context, state) {
        if (state.account == null) {
          return Skeleton(height: 20);
        }
        final locations = state.account.locRefs
            .map((loc) => loc.name)
            .where((loc) => state.organization.locations
                .any((loc2) => loc2.ref.name == loc))
            .join(', ');

        return CapsuleTitle(
          onShortTap: () => Navigator.of(context).pushNamed(NavRoutes.settings),
          position: CapsulePosition.left,
          child: Text(
            locations,
            style: TextStyle(
                color: Colors.white, fontSize: 13, fontWeight: FontWeight.w400),
          ),
        );
      },
    );
  }
}
