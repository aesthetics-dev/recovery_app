import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/controllers/dependency_injector.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/task_modal.dart';
import 'package:recovery_app/presentation/screens/home/widgets/main_header.dart';
import 'package:recovery_app/presentation/screens/task_list/task_list_section.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<NavigatorState> contentNavigator = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TaskListCubit>(
      create: (_) => TaskListCubit(
        taskService: getInstance(),
        notificationService: getInstance(),
        coreCubit: context.read(),
        ttsService: getInstance(),
        feedback: context.read(),
      ),
      // child: TaskList(),
      child: Scaffold(
        floatingActionButton: NewTaskFAB(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                MainHeader(),
                SizedBox(height: 20),
                TaskListSection(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // void _coreCubitListener(BuildContext context, CoreState state) {
  //   if (state.account == null) return;
  //   if (state.account.needsRegistration &&
  //       state.stage == Stage.main &&
  //       !alreadyLaunchedRegistration) {
  //     alreadyLaunchedRegistration = true;
  //     Navigator.of(context).pushNamed(NavRoutes.settings);
  //   }
  // }
}

class NewTaskFAB extends StatelessWidget {
  const NewTaskFAB({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: MyColors.primary,
      onPressed: () => showTaskEditModal(
        context: context,
        modalMode: TaskModalMode.taskEdit,
      ),
      child: Icon(
        FontAwesomeIcons.plus,
        color: Colors.white,
      ),
    );
  }
}
