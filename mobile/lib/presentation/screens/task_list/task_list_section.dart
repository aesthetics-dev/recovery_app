import 'dart:async';
import 'dart:math';

import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/domain/controllers/animated_list_controller.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/presentation/screens/task_list/widgets/slidable_task_list_item.dart';
import 'package:recovery_app/presentation/screens/task_list/widgets/task_list_item.dart';
import 'package:recovery_app/presentation/widgets/loading_indicator.dart';

import 'widgets/task_list_message.dart';

class TaskListSection extends StatefulWidget {
  static Key id = Key('TaskListScreen');

  TaskListSection() : super(key: id);

  @override
  _TaskListSectionState createState() => _TaskListSectionState();
}

class _TaskListSectionState extends State<TaskListSection> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskListCubit, TaskListState>(
      builder: (context, state) {
        if (state.tasks == null) {
          return LoadingIndicator();
        }
        return TaskList();
      },
    );
  }
}

class TaskList extends StatefulWidget {
  const TaskList({Key key}) : super(key: key);

  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> with WidgetsBindingObserver {
  Timer timer1Minute;
  Timer timer30Seconds;
  final confettiController = ConfettiController(duration: Duration(seconds: 1));
  int lastTaskNum = 0;

  final GlobalKey<AnimatedListState> listKey = GlobalKey();
  AnimatedListController<Task> listController;

  @override
  void initState() {
    super.initState();

    final taskCubit = context.read<TaskListCubit>();
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => listController = AnimatedListController(
        listKey: listKey,
        removedItemBuilder: _removedItemBuilder,
        initialItems: taskCubit.state.tasks,
      ),
    );

    timer1Minute = Timer.periodic(
      Duration(minutes: 1),
      (_) => setState(() {}),
    );

    timer30Seconds = Timer.periodic(
      Duration(seconds: 30),
      (_) => every30Seconds(),
    );

    WidgetsBinding.instance.addObserver(this);
  }

  void every30Seconds() {
    final taskCubit = context.read<TaskListCubit>();
    final coreCubit = context.read<CoreCubit>();
    if (coreCubit.state.account.isDisplayAlwaysOn) {
      taskCubit.updateTasks();
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      context.read<TaskListCubit>().updateTasks();
    }
  }

  @override
  void dispose() {
    confettiController.dispose();
    timer1Minute.cancel();
    timer30Seconds.cancel();
    super.dispose();
  }

  Widget _itemBuilder(
    BuildContext context,
    int index,
    Animation<double> animation,
  ) =>
      SlidableTaskListItem(
        task: listController.items[index],
        animation: animation,
      );

  Widget _removedItemBuilder(
          Task task, BuildContext context, Animation<double> animation) =>
      TaskListItem(task: task, animation: animation);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TaskListCubit, TaskListState>(
      listener: _onTaskListChange,
      builder: (context, state) => Column(
        children: <Widget>[
          ConfettiWidget(
            confettiController: confettiController,
            blastDirection: pi / 2,
          ),
          AnimatedList(
            shrinkWrap: true,
            padding: const EdgeInsets.only(bottom: 20),
            itemBuilder: _itemBuilder,
            key: listKey,
            primary: false,
          ),
          EmptyTaskList(),
          SizedBox(height: 100),
        ],
      ),
    );
  }

  void _onTaskListChange(BuildContext context, TaskListState state) {
    final tasks = [...state.tasks];
    tasks.sort(
      (t1, t2) => -t1.created.timestamp.compareTo(t2.created.timestamp),
    );
    listController?.updateList(tasks);
    if (tasks.isEmpty && lastTaskNum != 0) {
      confettiController.play();
    }
    setState(() {
      lastTaskNum = tasks.length;
    });
  }
}

class EmptyTaskList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskListCubit, TaskListState>(
      builder: (context, state) {
        return AnimatedOpacity(
          opacity: state.tasks.isEmpty ? 1 : 0,
          duration: Duration(milliseconds: 200),
          child: TaskListMessage(
            icon: FontAwesomeIcons.clipboardCheck,
            message: 'All tasks completed!',
          ),
        );
      },
    );
  }
}
