import 'package:flutter/material.dart';

class TaskListMessage extends StatelessWidget {
  final IconData icon;
  final String message;

  const TaskListMessage({
    Key key,
    @required this.icon,
    @required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const color = Colors.black54;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 40),
        Icon(
          icon,
          size: 60,
          color: color,
        ),
        SizedBox(height: 10),
        Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w600,
            color: color,
          ),
        ),
      ],
    );
  }
}
