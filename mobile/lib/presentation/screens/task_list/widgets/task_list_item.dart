import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/core/utils/date_time_converters.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';

import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/task_modal.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'package:recovery_app/utils/media_utils.dart';

class TaskListItem extends StatelessWidget {
  final Task task;
  final Animation<double> animation;

  const TaskListItem({
    Key key,
    @required this.task,
    @required this.animation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10),
      child: SlideTransition(
        position: _slideAnimation,
        child: SizeTransition(
          sizeFactor: _sizeAnimation,
          child: IntrinsicHeight(
            child: Row(
              children: <Widget>[
                Expanded(flex: 4, child: _timeBedLocSection(context)),
                SizedBox(width: 8),
                Expanded(
                  flex: MediaUtils(context).isBigScreen ? 10 : 7,
                  child: _tagsSection(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Animation<double> get _sizeAnimation => Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
          curve: Interval(0, .5),
          parent: animation,
        ),
      );

  Animation<Offset> get _slideAnimation =>
      Tween(begin: Offset(1, 0), end: Offset(0, 0)).animate(
        CurvedAnimation(
          curve: Interval(.5, 1),
          parent: animation,
        ),
      );

  Widget _timeBedLocSection(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Expanded(child: _bedName(context)),
        SizedBox(width: 15),
        IntrinsicWidth(child: _locNameAndTime(context)),
      ],
    );
  }

  Column _locNameAndTime(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          task.locRef.name,
          style: TextStyle(
            fontSize: MediaUtils(context).isBigScreen ? 20 : 16,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          DateTimeConverters.dateToJm(task.created.timestamp.toLocal()),
          style: TextStyle(
            color: Colors.black54,
            fontSize: MediaUtils(context).isBigScreen ? 18 : 15,
          ),
        ),
      ],
    );
  }

  Text _bedName(BuildContext context) {
    return Text(
      task.bedRef.name,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: MediaUtils(context).isBigScreen ? 40 : 25,
      ),
      textAlign: TextAlign.right,
    );
  }

  Widget _tagsSection(BuildContext context) {
    void openTaskTags() {
      showTaskEditModal(
        context: context,
        task: task,
        modalMode: TaskModalMode.taskEdit,
      );
    }

    void openTaskComments() {
      showTaskEditModal(
        context: context,
        task: task,
        modalMode: TaskModalMode.taskComment,
      );
    }

    final isNurse = context.watch<CoreCubit>().state.account.isNurse;

    return CapsuleTitle(
      color: _color(context),
      onShortTap: isNurse ? openTaskTags : openTaskComments,
      onLongTap: !isNurse ? openTaskTags : openTaskComments,
      position: CapsulePosition.right,
      child: _buildTaskCapsuleContent(context),
    );
  }

  Widget _buildTaskCapsuleContent(BuildContext context) {
    final tagText = task.allTags.join(' | ');
    final mdTagText = task.allMdTags.join(' | ');

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          tagText,
          style: TextStyle(
            fontSize: MediaUtils(context).isBigScreen ? 20 : 15,
            fontWeight: FontWeight.w500,
          ),
        ),
        Divider(
          height: MediaUtils(context).isBigScreen ? 12 : 10,
          color: Colors.black.withOpacity(.9),
        ),
        if (mdTagText.isNotEmpty)
          Text(
            mdTagText,
            style: TextStyle(
              fontSize: MediaUtils(context).isBigScreen ? 18 : 15,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
          ),
      ],
    );
  }

  Color _color(BuildContext context) {
    // final tz = context.watch<CoreCubit>().state.organization.utcOffset;
    final timestamp = task.created.timestamp.toLocal();

    DateTime time = DateTime.now();

    time = time.subtract(Duration(minutes: 10));
    if (timestamp.isAfter(time)) {
      return MyColors.taskRecent;
    }

    time = time.subtract(Duration(minutes: 10));
    if (timestamp.isAfter(time)) {
      return MyColors.taskOlder;
    }

    return MyColors.taskOld;
  }
}
