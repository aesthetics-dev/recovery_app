import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';

import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/task_list/widgets/task_list_item.dart';

class SlidableTaskListItem extends StatelessWidget {
  final Task task;
  final Animation<double> animation;

  const SlidableTaskListItem({
    Key key,
    this.task,
    this.animation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final taskBloc = context.watch<TaskListCubit>();
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        secondaryActions: <Widget>[
          BlocBuilder<CoreCubit, CoreState>(
            builder: (context, state) {
              return SlideAction(
                onTap: () {
                  HapticFeedback.lightImpact();
                  return taskBloc.removeTask(task);
                },
                color: state.account.isNurse
                    ? MyColors.error
                    : MyColors.completeTask,
                child: Icon(
                    state.account.isNurse
                        ? FontAwesomeIcons.trash
                        : FontAwesomeIcons.check,
                    color: Colors.white),
              );
            },
          ),
        ],
        child: TaskListItem(task: task, animation: animation),
      ),
    );
  }
}

class CircleButton extends StatelessWidget {
  final Widget child;
  final void Function() onTap;
  final Color color;
  final Widget icon;

  const CircleButton({
    Key key,
    this.child,
    this.onTap,
    this.color = Colors.blue,
    this.icon,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
      child: _content,
    );
  }

  Widget get _content => IconButton(
        icon: icon,
        onPressed: onTap,
        iconSize: 15,
      );
}
