import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/edit_task/task_tag_screen.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/task_modal.dart';

import 'task_bed_screen.dart';

class TaskEditModal extends StatefulWidget {
  final Task task;
  final CoreCubit coreCubit;
  final TaskModalMode modalMode;
  const TaskEditModal({
    Key key,
    @required this.task,
    @required this.coreCubit,
    @required this.modalMode,
  }) : super(key: key);

  @override
  _TaskEditModalState createState() => _TaskEditModalState();
}

class _TaskEditModalState extends State<TaskEditModal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .8,
      padding: EdgeInsets.symmetric(horizontal: MyTheme.sideMargin),
      child: BlocBuilder<TaskEditCubit, TaskEditState>(
        builder: (context, state) =>
            state.task?.bedRef == null ? TaskBedScreen() : TaskTagScreen(),
      ),
    );
  }
}
