import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/custom_tag_input.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/tag_list.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/task_modal_title.dart';

class TaskTagScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<TaskEditCubit>();

    return BlocBuilder<TaskEditCubit, TaskEditState>(builder: (context, state) {
      final bedName = state.task.bedRef?.name;

      return state.task == null
          ? Container()
          : Column(
              children: <Widget>[
                TaskModalTitle(
                  title: '${state.task.locRef.name} - $bedName',
                  onBack: () => cubit.setTaskBed(null),
                ),
                SizedBox(height: 10),
                CustomTagInput(tagType: TagType.nurse),
                SizedBox(height: 10),
                TagList(tagType: TagType.nurse),
              ],
            );
    });
  }
}
