import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/controllers/dependency_injector.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/edit_task/task_bed_screen.dart';
import 'package:recovery_app/presentation/screens/edit_task/task_tag_screen.dart';

import '../task_comment_screen.dart';

enum TaskModalMode { taskEdit, taskComment }

void showTaskEditModal({
  BuildContext context,
  Task task,
  TaskModalMode modalMode,
}) {
  final coreCubit = context.read<CoreCubit>();
  final taskListCubit = context.read<TaskListCubit>();
  final feedbackCubit = context.read<FeedbackCubit>();

  showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    shape: MyTheme.modalShape,
    builder: (context) => MultiProvider(
      providers: [
        BlocProvider.value(value: coreCubit),
        BlocProvider.value(value: taskListCubit),
        BlocProvider(
            create: (context) => TaskEditCubit(
                  coreCubit: coreCubit,
                  localStorage: getInstance(),
                  taskService: getInstance(),
                  taskListCubit: taskListCubit,
                  feedback: feedbackCubit,
                )..setTask(task)),
      ],
      child: Container(
        height: MediaQuery.of(context).size.height *
            (modalMode == TaskModalMode.taskComment ? .6 : .8),
        padding: EdgeInsets.symmetric(
          horizontal: MyTheme.sideMargin,
          vertical: 20,
        ),
        child: TaskModal(mode: modalMode),
      ),
    ),
  );
}

class TaskModal extends StatelessWidget {
  final TaskModalMode mode;

  const TaskModal({Key key, this.mode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskEditCubit, TaskEditState>(
      builder: (context, state) {
        if (state.task == null) {
          return Container();
        }

        if (state.task.bedRef == null) {
          return TaskBedScreen();
        }
        if (mode == TaskModalMode.taskEdit) {
          return TaskTagScreen();
        }

        return TaskCommentScreen();
      },
    );
  }
}
