import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

class TaskModalTitle extends StatelessWidget {
  final String title;
  final void Function() onBack;
  static const _size = 30.0;

  const TaskModalTitle({
    Key key,
    this.title,
    this.onBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildBackButton(),
              Text(title, style: MyTextStyles.modalTitle),
              _buildContainer(Container()),
            ],
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget _buildContainer(Widget child) => Container(
        height: _size,
        width: _size,
        child: child ?? Container(),
      );

  Widget _buildBackButton() {
    return _buildContainer(
      onBack == null
          ? Container()
          : InkWell(
              onTap: onBack,
              child: Icon(
                FontAwesomeIcons.chevronLeft,
                size: _size,
                color: MyColors.primary,
              ),
            ),
    );
  }
}
