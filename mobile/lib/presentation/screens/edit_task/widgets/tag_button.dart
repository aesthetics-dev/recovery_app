import 'dart:math';

import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/core/selectables/selectables.dart';

class TagButton extends StatelessWidget {
  final Function() onTap;
  final Selectable<String> tag;

  const TagButton({
    Key key,
    @required this.onTap,
    @required this.tag,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: min(160.0, MediaQuery.of(context).size.width / 2.5),
      height: 70,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Material(
          elevation: 2,
          borderRadius: BorderRadius.circular(15),
          color: tag.selected
              ? MyColors.selectedButton
              : MyColors.notSelectedButton,
          clipBehavior: Clip.hardEdge,
          child: InkWell(
            onTap: onTap,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Center(
                child: Text(
                  tag.data,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: tag.selected ? Colors.white : Colors.black,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
