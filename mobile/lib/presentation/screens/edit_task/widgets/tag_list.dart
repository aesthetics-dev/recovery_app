import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/presentation/core/selectables/selectables.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/tag_button.dart';

class TagList extends StatelessWidget {
  final TagType tagType;

  const TagList({
    Key key,
    @required this.tagType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskEditCubit, TaskEditState>(
      builder: (context, taskState) => BlocBuilder<CoreCubit, CoreState>(
        builder: (context, coreState) {
          final task = taskState.task;
          final location = coreState.organization.findLocation(task.locRef.id);

          final isNurse = tagType == TagType.nurse;
          final tags = Selectables<String>.fromData(
            all: isNurse ? location.tags : location.mdTags,
            selected: isNurse ? task.tags : task.mdTags,
          );

          final editBloc = context.watch<TaskEditCubit>();
          final buttons = tags.items
              .map((tag) => TagButton(
                    tag: tag,
                    onTap: () => editBloc.setTaskTags(
                      tagType,
                      tags.toggle(tag.data).selected,
                    ),
                  ))
              .toList();

          return Expanded(
            child: SingleChildScrollView(
              child: Wrap(
                children: buttons,
              ),
            ),
          );
        },
      ),
    );
  }
}
