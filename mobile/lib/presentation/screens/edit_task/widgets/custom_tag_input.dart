import 'package:flutter/material.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/domain/cubits/task_list_cubit/task_list_cubit.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/presentation/widgets/cool_button_input.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class CustomTagInput extends StatelessWidget {
  final TagType tagType;

  const CustomTagInput({
    Key key,
    @required this.tagType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final taskBloc = context.watch<TaskEditCubit>();

    return BlocBuilder<TaskEditCubit, TaskEditState>(
      builder: (context, state) => BlocBuilder<CoreCubit, CoreState>(
        builder: (context, taskListState) =>
            BlocBuilder<TaskListCubit, TaskListState>(
          builder: (context, taskListState) => Row(
            children: [
              _buildInput(taskBloc, context, state),
            ],
          ),
        ),
      ),
    );
  }

  Expanded _buildInput(
    TaskEditCubit taskBloc,
    BuildContext context,
    TaskEditState state,
  ) {
    return Expanded(
      child: CoolButtonInput(
        inputParams: InputParams(
          placeholder: 'Custom Tag',
          onChange: (val) => taskBloc.setCustomTag(tagType, val),
          onSubmit: () => onSubmit(context, taskBloc),
          initialValue: tagType == TagType.nurse
              ? state.task.customTag
              : state.task.customMDTag,
          canSubmit: state.task.allTags.isNotEmpty,
        ),
        // submitIcon: FontAwesomeIcons.check,
      ),
    );
  }

  void onSubmit(BuildContext context, TaskEditCubit taskBloc) =>
      taskBloc.saveTask(tagType).then((_) => Navigator.of(context).pop());
}
