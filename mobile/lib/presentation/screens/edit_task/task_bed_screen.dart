import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/domain/models/data/bed/bed.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/task_modal_title.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/settings_styles.dart';
import 'package:recovery_app/presentation/widgets/cool_button_input.dart';

class TaskBedScreen extends StatefulWidget {
  @override
  _TaskBedScreenState createState() => _TaskBedScreenState();
}

class _TaskBedScreenState extends State<TaskBedScreen> {
  @override
  void initState() {
    super.initState();
    context.read<TaskEditCubit>().setCustomTaskBedName('');
  }

  @override
  Widget build(BuildContext context) {
    final taskEditCubit = context.watch<TaskEditCubit>();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        TaskModalTitle(title: 'Pick Task Bed'),
        SizedBox(height: 2),
        LocationSelector(),
        SizedBox(height: 10),
        CoolButtonInput(
          inputParams: InputParams(
            placeholder: "Custom Location",
            onChange: taskEditCubit.setCustomTaskBedName,
            onSubmit: taskEditCubit.submitCustomTaskBedName,
            canSubmit: taskEditCubit.state.customBedName.isNotEmpty,
          ),
          icon: Icon(FontAwesomeIcons.chevronRight, color: Colors.white),
        ),
        SizedBox(height: 10),
        BedList(),
      ],
    );
  }
}

class LocationSelector extends StatelessWidget {
  const LocationSelector({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final taskBloc = context.watch<TaskEditCubit>();

    return BlocBuilder<CoreCubit, CoreState>(
      builder: (context, coreState) =>
          BlocBuilder<TaskEditCubit, TaskEditState>(
        builder: (context, state) {
          final locations = coreState.account.locRefs;
          if (state.task == null || locations == null) {
            return Container();
          }

          if (locations.length < 2) {
            taskBloc.setTaskLocation(locations[0]);
            return Center(
                child: Text(locations[0].name, style: kSettingsTextFieldTitle));
          }

          return CupertinoSegmentedControl(
            children: locations.map(_buildLocation).toList().asMap(),
            selectedColor: MyColors.primary,
            borderColor: MyColors.primary,
            onValueChanged: (i) =>
                taskBloc.setTaskLocation(locations[i as int]),
            groupValue: locations.indexWhere(
              (loc) => loc == taskBloc.state.task.locRef,
            ),
          );
        },
      ),
    );
  }

  Widget _buildLocation(Reference location) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(location.name, textAlign: TextAlign.center),
      );
}

class BedList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskEditCubit, TaskEditState>(
      builder: (context, taskState) => BlocBuilder<CoreCubit, CoreState>(
        builder: (context, coreState) {
          final task = taskState.task;
          final org = coreState.organization;
          final location = org.findLocation(task?.locRef?.id);
          final beds = location?.beds ?? [];

          beds.sort(
            ([bed1, bed2]) => compareNatural(bed1.ref.name, bed2.ref.name),
          );
          final bedButtons = beds
              .map((bed) =>
                  _buildBedButton(context, bed, context.watch<TaskEditCubit>()))
              .toList();

          return Flexible(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 20),
                child: Wrap(
                  alignment: WrapAlignment.spaceEvenly,
                  runSpacing: 15,
                  spacing: 15,
                  children: bedButtons,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildBedButton(
    BuildContext context,
    Bed bed,
    TaskEditCubit taskBloc,
  ) =>
      InkWell(
        onTap: () {
          taskBloc.setTaskBed(bed.ref);
        },
        child: Material(
          elevation: 3,
          color: MyColors.notSelectedButton,
          borderRadius: BorderRadius.circular(15),
          child: SizedBox(
            height: 70,
            width: min(80.0, MediaQuery.of(context).size.width / 2.5),
            child: Center(
              child: Text(
                bed.ref.name,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ),
      );
}
