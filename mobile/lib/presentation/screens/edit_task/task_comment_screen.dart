import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/custom_tag_input.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/tag_list.dart';
import 'package:recovery_app/presentation/screens/edit_task/widgets/task_modal_title.dart';

class TaskCommentScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskEditCubit, TaskEditState>(
      builder: (context, state) {
        final task = state.task;

        if (task == null) {
          return Container();
        }

        final title = '${task.locRef.name} ${task.bedRef.name}';
        return Column(
          children: <Widget>[
            TaskModalTitle(title: title),
            CustomTagInput(tagType: TagType.md),
            TagList(tagType: TagType.md)
          ],
        );
      },
    );
  }
}
