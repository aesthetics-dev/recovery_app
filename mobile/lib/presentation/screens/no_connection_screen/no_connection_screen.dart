import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/controllers/dependency_injector.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/services/http_controller.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/info_section.dart';
import 'package:recovery_app/presentation/widgets/cool_button.dart';
import 'package:provider/provider.dart';

class NoConnectionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ConnectionStatus>(
        stream: getInstance<HttpController>().$connection,
        builder: (context, snapshot) {
          return Directionality(
            textDirection: TextDirection.ltr,
            child: Container(
              color: MyColors.primary,
              width: double.infinity,
              child: _buildContent(context),
            ),
          );
        });
  }

  Column _buildContent(BuildContext context) {
    final coreCubit = context.watch<CoreCubit>();

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InfoSection(
          icon: FontAwesomeIcons.wifi,
          text: coreCubit.organizationService.http.connection ==
                  ConnectionStatus.noInternet
              ? "Oops... We're having trouble connecting to the internet."
              : "Oops... Can't seem to connect to our servers.",
          topBold: true,
        ),
        SizedBox(height: 20),
        CoolButton(
          label: 'Try again',
          onPressed: coreCubit.tryReconnect,
          containerColor: Colors.white,
          textColor: MyColors.offBlack,
        ),
      ],
    );
  }
}
