import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/domain/cubits/historic_task_list_cubit/historic_task_list_cubit.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/historic_tasks_list/widgets/historic_task_list.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/settings_styles.dart';

void showHistoricTasksModal({
  BuildContext context,
}) {
  final taskCub = HistoricTaskListCubit(taskListCubit: context.read());
  taskCub.update();

  showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    shape: MyTheme.modalShape,
    builder: (context) => SizedBox(
      height: MediaQuery.of(context).size.height * .8,
      child: BlocProvider<HistoricTaskListCubit>.value(
        value: taskCub,
        child: HistoricTasksModal(),
      ),
    ),
  );
}

class HistoricTasksModal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HistoricTaskListCubit, HistoricTaskListState>(
        builder: (context, state) {
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Text(
                'Recently Completed',
                style: kSettingsTextFieldTitle.copyWith(fontSize: 22),
              ),
              SizedBox(height: 10),
              HistoricTaskList(state: state),
            ],
          ),
        ),
      );
    });
  }
}
