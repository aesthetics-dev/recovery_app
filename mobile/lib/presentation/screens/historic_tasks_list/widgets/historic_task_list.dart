import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/core/utils/date_time_converters.dart';
import 'package:recovery_app/domain/cubits/historic_task_list_cubit/historic_task_list_cubit.dart';
import 'package:recovery_app/domain/models/data/footprint/footprint.dart';
import 'package:recovery_app/domain/models/data/task/task.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/loading_indicator.dart';

class HistoricTaskList extends StatelessWidget {
  final HistoricTaskListState state;

  const HistoricTaskList({Key key, this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (state.tasks == null) {
      return LoadingIndicator();
    }

    return Column(
      children: state.tasks
          .map<Widget>((t) => HistoricTaskListItem(task: t))
          .toList(),
    );
  }
}

class HistoricTaskListItem extends StatelessWidget {
  final Task task;

  const HistoricTaskListItem({Key key, this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20),
      child: Material(
        color: MyColors.notSelectedButton,
        elevation: 3,
        borderRadius: BorderRadius.circular(15),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: _buildContent(context),
        ),
      ),
    );
  }

  Column _buildContent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildTags(context),
            _buildTimestamps(context),
          ],
        ),
      ],
    );
  }

  Widget _buildTags(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${task.locRef.name} - ${task.bedRef.name}',
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
          _buildTagList(task.customTag, task.tags, true),
          _buildTagList(task.customMDTag, task.mdTags, false)
        ],
      ),
    );
  }

  Widget _buildTagList(String custom, List<String> tags, bool bold) {
    List<String> outTags = tags;
    if (custom.isNotEmpty) {
      outTags = [custom, ...outTags];
    }
    outTags = outTags.isEmpty ? ['-'] : outTags;
    return Text(
      outTags.join(", "),
      style: TextStyle(fontWeight: bold ? FontWeight.w500 : null),
    );
  }

  Widget _buildTimestamps(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        _timeStamp(task.created, FontAwesomeIcons.plus),
        _timeStamp(task.removed, FontAwesomeIcons.check),
      ],
    );
  }

  Widget _timeStamp(Footprint fp, IconData icon) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          DateTimeConverters.dateToJm(fp.timestamp.toLocal()),
          textAlign: TextAlign.end,
        ),
        SizedBox(width: 10),
        Icon(icon, size: 15),
      ],
    );
  }
}
