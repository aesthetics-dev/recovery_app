import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/admin_cubit/admin_cubit.dart';
import 'package:recovery_app/domain/models/data/role/role.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/domain/models/util/value/value.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/cool_button_input.dart';
import 'package:recovery_app/domain/core/extensions/list_extensions.dart';

class RoleEditor extends HookWidget {
  final vContext = Value<BuildContext>();

  @override
  Widget build(BuildContext context) {
    final adminCubit = context.watch<AdminCubit>();
    final role = useState('');
    vContext.v = context;

    return Padding(
      padding: const EdgeInsets.only(
        top: 20.0,
        left: MyTheme.sideMargin,
        right: MyTheme.sideMargin,
      ),
      child: Container(
        width: double.infinity,
        child: BlocBuilder<AdminCubit, AdminState>(builder: (context, state) {
          final roles = state.organization.roles.indexedMap(_buildChip);

          final canSubmitRole = role.value.isNotEmpty &&
              !state.organization.roles.any((r) => r.ref.name == role.value);

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Roles',
                textAlign: TextAlign.left,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              SizedBox(height: 10),
              CoolButtonInput(
                inputParams: InputParams(
                  value: role.value,
                  onChange: (v) => role.value = v,
                  canSubmit: canSubmitRole,
                  onSubmit: () => adminCubit.addRole(role.value),
                  clearOnSubmit: true,
                  placeholder: 'New role',
                ),
                icon: Icon(FontAwesomeIcons.plus, color: Colors.white),
              ),
              SizedBox(height: 10),
              Wrap(
                spacing: 10,
                runSpacing: 10,
                children: roles.toList(),
              ),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildChip(Role r, int index) {
    final adminCubit = vContext.v.read<AdminCubit>();

    return Chip(
      label: GestureDetector(
        onTap: r.editable ? () => adminCubit.toggleRoleType(index) : null,
        child: Column(
          children: [
            Text(
              r.ref.name,
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
              ),
            ),
            Text(
              r.type == RoleTypes.poster ? 'poster' : 'completer',
              style: TextStyle(
                color: Colors.white.withOpacity(.7),
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
      onDeleted: r.editable ? () => adminCubit.removeRole(index) : null,
      backgroundColor: MyColors.selectedButton,
      deleteIconColor: Colors.white,
      padding: EdgeInsets.all(8),
    );
  }
}
