import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/admin_cubit/admin_cubit.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/role_editor.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/whitelisted_email_editor.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';

class OrganizationEditor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final adminCubit = context.watch<AdminCubit>();
    return BlocBuilder<AdminCubit, AdminState>(
      cubit: adminCubit,
      builder: (context, state) {
        return Column(
          children: [
            CapsuleTitle(
              position: CapsulePosition.left,
              child: Text(
                'Organization Settings',
                style: CapsuleTitle.subsectionTextStyle,
              ),
            ),
            Padding(
              padding: MyTheme.wSideMargin,
              child: _buildNameEditor(state, adminCubit),
            ),
            RoleEditor(),
            SizedBox(height: 10),
            WhitelistedEmailEditor(),
          ],
        );
      },
    );
  }

  TextFormField _buildNameEditor(AdminState state, AdminCubit adminCubit) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      initialValue: state.organization.ref.name,
      onChanged: (val) => adminCubit.changeOrganizationName(val),
    );
  }
}
