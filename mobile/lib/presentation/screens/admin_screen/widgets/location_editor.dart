import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/cubits/admin_cubit/admin_cubit.dart';
import 'package:recovery_app/domain/cubits/task_edit_cubit/task_edit_cubit.dart';
import 'package:recovery_app/domain/models/data/organization/organization.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/admin_location.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/list_edit_modal.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/location_button.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'package:recovery_app/domain/core/extensions/list_extensions.dart';
import 'package:recovery_app/presentation/widgets/cool_button_input.dart';

class LocationEditor extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final hCustomLocation = useState('');

    final _adminCubit = context.watch<AdminCubit>();
    return BlocBuilder<AdminCubit, AdminState>(
      builder: (context, state) {
        return Column(
          children: [
            _sectionTitle,
            SizedBox(height: 20),
            _buildNewLocationInput(_adminCubit, hCustomLocation),
            SizedBox(height: 5),
            ...state.organization.locations
                .indexedMap((l, i) => AdminLocation(loc: l, index: i))
                .toList(),
          ],
        );
      },
    );
  }

  Padding _buildNewLocationInput(
    AdminCubit _adminCubit,
    ValueNotifier<String> hCustomLocation,
  ) {
    return Padding(
      padding: MyTheme.wSideMargin,
      child: BlocBuilder<AdminCubit, AdminState>(
        builder: (context, state) {
          return CoolButtonInput(
            inputParams: InputParams(
              placeholder: 'New location...',
              clearOnSubmit: true,
              onSubmit: () => _adminCubit.addLocation(hCustomLocation.value),
              canSubmit: hCustomLocation.value.isNotEmpty,
              onChange: (v) => hCustomLocation.value = v,
              value: hCustomLocation.value,
            ),
            icon: Icon(FontAwesomeIcons.plus, color: Colors.white),
          );
        },
      ),
    );
  }

  final Widget _sectionTitle = CapsuleTitle(
    position: CapsulePosition.left,
    child: Text(
      'Locations',
      style: CapsuleTitle.subsectionTextStyle,
    ),
  );
}

void openListEditModal(
  BuildContext context,
  AdminCubit adminCubit,
  Organization org,
  Reference locRef,
  LocationEditButtonType type,
) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    enableDrag: false,
    shape: MyTheme.modalShape,
    builder: (context) => BlocProvider.value(
      value: adminCubit,
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Container(
            height: MediaQuery.of(context).size.height * .8,
            child: _getModal(adminCubit, locRef, type)),
      ),
    ),
  );

  // modal.then((_) => _onCloseModal(type));
}

Widget _getModal(
  AdminCubit adminCubit,
  Reference locRef,
  LocationEditButtonType type,
) {
  return BlocBuilder<AdminCubit, AdminState>(
    builder: (context, state) {
      final org = state.organization;
      final loc = org.findLocation(locRef.id);
      final locName = loc.ref.name;
      if (type == LocationEditButtonType.beds) {
        return ListEditModal(
          title: "$locName Beds",
          items: loc.beds.map((b) => b.ref.name).toList(),
          onSubmit: (name) => adminCubit.addBed(locRef, name),
          onDelete: (name) => adminCubit.removeBed(locRef, name),
          // onReorder: (from, to) => adminCubit.reorderBeds(locRef, from, to),
        );
      }

      final areNurseTags = type == LocationEditButtonType.tags;
      final tagType = areNurseTags ? TagType.nurse : TagType.md;

      return ListEditModal(
        title: areNurseTags ? "$locName Tags" : "$locName MD-Tags",
        items: areNurseTags ? [...loc.tags] : [...loc.mdTags],
        onSubmit: (input) => adminCubit.addTag(locRef, input, tagType),
        onDelete: (name) => adminCubit.removeTag(locRef, name, tagType),
        onReorder: (tags) =>
            adminCubit.changeTags(locRef, (list) => tags, tagType),
      );
    },
  );
}
