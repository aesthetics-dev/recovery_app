import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:recovery_app/domain/cubits/admin_cubit/admin_cubit.dart';
import 'package:recovery_app/domain/models/data/location/location.dart';
import 'package:recovery_app/presentation/core/confirmation_dialog.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/location_button.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/location_editor.dart';

class AdminLocation extends HookWidget {
  final Location loc;
  final int index;

  const AdminLocation({
    Key key,
    @required this.loc,
    @required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final adminCubit = context.watch<AdminCubit>();
    final _controller = useTextEditingController();

    if (_controller.text != loc.ref.name) {
      _controller.text = loc.ref.name;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: MyTheme.sideMargin),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Divider(height: 40, color: Colors.black),
          TextFormField(
            controller: _controller,
            onChanged: (v) => adminCubit.changeLocationName(index, v),
            decoration: InputDecoration(
              labelText: 'Location Name',
              border: OutlineInputBorder(),
              errorText: loc.ref.name.isEmpty ? 'Cannot be empty' : '',
            ),
          ),
          _buildLocationButtons(context, loc),
        ],
      ),
    );
  }

  Widget _buildLocationButtons(BuildContext context, Location loc) {
    final adminCubit = context.watch<AdminCubit>();
    return BlocBuilder<AdminCubit, AdminState>(
      builder: (context, state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            LocationEditButton.get(
              LocationEditButtonType.beds,
              () => openListEditModal(
                context,
                adminCubit,
                state.organization,
                loc.ref,
                LocationEditButtonType.beds,
              ),
            ),
            LocationEditButton.get(
              LocationEditButtonType.tags,
              () => openListEditModal(
                context,
                adminCubit,
                state.organization,
                loc.ref,
                LocationEditButtonType.tags,
              ),
            ),
            LocationEditButton.get(
              LocationEditButtonType.mdTags,
              () => openListEditModal(
                context,
                adminCubit,
                state.organization,
                loc.ref,
                LocationEditButtonType.mdTags,
              ),
            ),
            LocationEditButton.get(
              LocationEditButtonType.remove,
              () => ConfirmationDialog.show(
                context,
                title: 'Remove ${loc.ref.name}?',
                content: 'This action may be irreversible.',
                onYes: () => adminCubit.removeLocation(loc.ref),
              ),
            ),
          ],
        );
      },
    );
  }
}
