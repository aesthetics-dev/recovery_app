import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

class LocationEditButton extends StatelessWidget {
  final Function() onTap;
  final _LocationButton buttonData;

  const LocationEditButton({
    Key key,
    @required this.buttonData,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: Column(
        children: [
          IconButton(
            icon: Icon(
              buttonData.icon,
              color: MyColors.primary,
            ),
            onPressed: onTap,
          ),
          Text(
            buttonData.title,
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  static final Map<LocationEditButtonType, _LocationButton> _typeToButton = {
    LocationEditButtonType.beds:
        _LocationButton(icon: FontAwesomeIcons.bed, title: 'Beds'),
    LocationEditButtonType.tags:
        _LocationButton(icon: FontAwesomeIcons.thermometer, title: 'Tags'),
    LocationEditButtonType.mdTags:
        _LocationButton(icon: FontAwesomeIcons.tags, title: 'MD-Tags'),
    LocationEditButtonType.remove:
        _LocationButton(icon: FontAwesomeIcons.trash, title: 'Remove'),
  };

  static Widget get(LocationEditButtonType type, Function() onTap) =>
      LocationEditButton(
        buttonData: _typeToButton[type],
        onTap: onTap,
      );
}

class _LocationButton {
  final Color color;
  final IconData icon;
  final String title;

  _LocationButton({
    this.icon,
    this.title,
    this.color = Colors.black,
  });
}

enum LocationEditButtonType { beds, tags, mdTags, remove, roles }
