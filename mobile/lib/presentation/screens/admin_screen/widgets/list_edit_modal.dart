import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:implicitly_animated_reorderable_list/implicitly_animated_reorderable_list.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/presentation/core/confirmation_dialog.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/cool_button_input.dart';

class ListEditModal extends HookWidget {
  final List<String> items;
  final Function(String val) onSubmit;
  final Function(String val) onDelete;
  final Function(List<String>) onReorder;
  final String title;
  final String placeholder;
  final bool shouldPad;
  final bool loading;

  const ListEditModal({
    Key key,
    @required this.items,
    @required this.onSubmit,
    @required this.onDelete,
    this.onReorder,
    this.loading = false,
    this.placeholder = 'New Value...',
    this.shouldPad = true,
    this.title = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final hInput = useState('');
    if (onReorder == null) items.sort(([a, b]) => compareNatural(a, b));
    return Padding(
      padding: MyTheme.sidePadding,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Text(
              title,
              style: MyTextStyles.modalTitle,
            ),
            SizedBox(height: 10),
            CoolButtonInput(
              inputParams: InputParams(
                placeholder: placeholder,
                onSubmit: () => onSubmit(hInput.value),
                onChange: (v) => hInput.value = v,
                canSubmit: hInput.value.isNotEmpty,
                value: hInput.value,
                clearOnSubmit: true,
              ),
            ),
            SizedBox(height: 10),
            buildList(context),
            SizedBox(height: 60),
          ],
        ),
      ),
    );
  }

  Widget buildList(BuildContext context) {
    return ImplicitlyAnimatedReorderableList<String>(
      items: items,
      itemBuilder: (context, animation, item, i) => buildItem(context, item),
      areItemsTheSame: (a, b) => a == b,
      onReorderFinished: (item, from, to, newItems) => onReorder(newItems),
      shrinkWrap: true,
    );
  }

  Reorderable buildItem(BuildContext context, String item) {
    return Reorderable(
      key: ValueKey(item),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                if (onReorder != null)
                  Handle(
                    delay: const Duration(milliseconds: 100),
                    child: Icon(
                      Icons.list,
                      color: Colors.grey,
                    ),
                  ),
                SizedBox(width: 20),
                Text(item),
              ],
            ),
            IconButton(
                icon: Icon(
                  FontAwesomeIcons.trash,
                  size: 24,
                ),
                onPressed: () => ConfirmationDialog.show(context,
                    onYes: () => onDelete(item),
                    title: 'Remove $item?',
                    content: 'This action may be irreversible.')),
          ],
        ),
      ),
    );
  }
}

class ListEditModalData {
  final Function(String val) onSubmit;
  final List<String> items;
  final String title;
  final Function(Reference ref) onDelete;

  ListEditModalData({
    this.onSubmit,
    this.items,
    this.title,
    this.onDelete,
  });
}
