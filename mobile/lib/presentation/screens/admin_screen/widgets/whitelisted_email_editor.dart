import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recovery_app/domain/cubits/admin_cubit/admin_cubit.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/list_edit_modal.dart';
import 'package:recovery_app/presentation/widgets/cool_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WhitelistedEmailEditor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CoolButton(
      label: 'Edit Emails',
      onPressed: () => _onEditButtonClicked(context),
    );
  }

  void _onEditButtonClicked(BuildContext context) {
    final adminCubit = context.read<AdminCubit>();
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: false,
      shape: MyTheme.modalShape,
      builder: (context) => BlocProvider.value(
        value: adminCubit,
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
              height: MediaQuery.of(context).size.height * .8,
              child: _getModal(adminCubit)),
        ),
      ),
    );
  }

  Widget _getModal(AdminCubit adminCubit) {
    return BlocBuilder<AdminCubit, AdminState>(
      builder: (context, state) {
        return ListEditModal(
          title: 'Organization Emails',
          items: state.organization.emails,
          onSubmit: adminCubit.addEmail,
          onDelete: adminCubit.removeEmail,
        );
      },
    );
  }
}
