import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/controllers/dependency_injector.dart';
import 'package:recovery_app/domain/cubits/admin_cubit/admin_cubit.dart';
import 'package:recovery_app/domain/utilities/nav_utils.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/location_editor.dart';
import 'package:recovery_app/presentation/screens/admin_screen/widgets/organization_editor.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'package:recovery_app/presentation/widgets/cool_button.dart';

class AdminScreen extends StatefulWidget {
  @override
  _AdminScreenState createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<AdminCubit>(
      create: (_) => AdminCubit(
          coreCubit: context.read(),
          organizationService: getInstance(),
          feedback: context.read()),
      child: Scaffold(
        floatingActionButton: SaveButton(),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniCenterFloat,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _pageTitle,
                _sectionSpace,
                OrganizationEditor(),
                _sectionSpace,
                LocationEditor(),
                SizedBox(height: 60),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get _sectionSpace => SizedBox(height: 20);

  Widget get _pageTitle => CapsuleTitle(
        child: Text(
          "Admin Panel",
          style: MyTextStyles.capsulePageTitle,
        ),
      );
}

class SaveButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final keyboardIsOpen = MediaQuery.of(context).viewInsets.bottom != 0;
    final cubit = context.watch<AdminCubit>();
    return SizedBox(
      width: 100,
      height: 50,
      child: Visibility(
        visible: !keyboardIsOpen,
        child: CoolButton(
          onPressed: () => cubit.submitOrganization().then(
              (success) => success ? NavUtils.popIfCurrent(context) : null),
          containerColor: MyColors.selectedButton,
          label: 'SAVE',
        ),
      ),
    );
  }
}
