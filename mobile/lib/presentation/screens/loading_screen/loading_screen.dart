import 'package:flutter/material.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/loading_indicator.dart';
import 'package:recovery_app/presentation/widgets/logo.dart';
import 'package:provider/provider.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  FeedbackCubit feedback;

  @override
  void initState() {
    try {
      feedback = context.read<FeedbackCubit>();
      feedback.blockLoading();
    } catch (_) {}
    super.initState();
  }

  @override
  void dispose() {
    feedback?.unblockLoading();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: MyTheme.sideMargin),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Logo(),
          SizedBox(height: 30),
          LoadingIndicator(),
        ],
      ),
    );
  }
}
