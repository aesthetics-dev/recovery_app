import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/controllers/dependency_injector.dart';
import 'package:recovery_app/domain/cubits/settings_cubit/settings_cubit.dart';
import 'package:recovery_app/domain/utilities/nav_routes.dart';
import 'package:recovery_app/domain/utilities/nav_utils.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/location_selector.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/role_name_editor.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/settings_switches.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SettingsCubit>(
      create: (context) => SettingsCubit(
          notificationService: getInstance(),
          userService: getInstance(),
          feedback: context.read(),
          coreCubit: context.read()),
      child: Builder(
        builder: (context) {
          return BlocListener<CoreCubit, CoreState>(
            listenWhen: (p, c) => p.account != c.account,
            listener: (context, state) => context.read<SettingsCubit>().reset(),
            child: Scaffold(
              floatingActionButton: SettingsFAB(),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              body: SafeArea(
                child: SettingsContent(),
              ),
            ),
          );
        },
      ),
    );
  }
}

class SettingsFAB extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();

    return FloatingActionButton(
      backgroundColor: MyColors.primary,
      onPressed: () => settingsCubit
          .save()
          .then((success) => success ? NavUtils.popIfCurrent(context) : null),
      child: Icon(FontAwesomeIcons.check, color: Colors.white),
    );
  }
}

class SettingsContent extends StatelessWidget {
  const SettingsContent({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const sectionSpace = SizedBox(height: 20);
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildTitle(),
          SizedBox(height: 5),
          RoleNameEditor(),
          sectionSpace,
          LocationSection(),
          sectionSpace,
          SettingsSwitches(),
          sectionSpace,
          _buildAdminButton(context),
          sectionSpace,
          _buildSignOutButton(context),
          SizedBox(height: 90),
        ],
      ),
    );
  }

  CapsuleTitle _buildTitle() {
    return CapsuleTitle(
      child: Text(
        'Settings',
        style: MyTextStyles.capsulePageTitle,
      ),
    );
  }

  Widget _buildSignOutButton(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();
    return CapsuleTitle(
      position: CapsulePosition.right,
      onShortTap: settingsCubit.signOut,
      color: MyColors.selectedButton,
      child: Text(
        'Sign out',
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget _buildAdminButton(BuildContext context) {
    return BlocBuilder<CoreCubit, CoreState>(
      builder: (context, state) => state.account.admin
          ? CapsuleTitle(
              position: CapsulePosition.right,
              onShortTap: () =>
                  Navigator.of(context).pushNamed(NavRoutes.adminScreen),
              color: MyColors.selectedButton,
              child: Text(
                'Admin Settings',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
            )
          : Container(),
    );
  }
}
