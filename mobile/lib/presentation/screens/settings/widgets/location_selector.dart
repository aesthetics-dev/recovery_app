import 'package:flutter/material.dart';
import 'package:recovery_app/domain/cubits/settings_cubit/settings_cubit.dart';
import 'package:recovery_app/domain/models/data/reference/reference.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/core/selectables/selectables.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'package:recovery_app/presentation/widgets/outer_padding.dart';

class LocationSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        CapsuleTitle(
          position: CapsulePosition.left,
          child: Text('I want to see tasks from...',
              style: CapsuleTitle.sectionTextStyle),
        ),
        OuterPadding(child: BlocBuilder<CoreCubit, CoreState>(
          builder: (context, state) {
            final settingsCubit = context.watch<SettingsCubit>();
            final locs =
                state.organization.locations.map((loc) => loc.ref).toList();

            final selLocs = Selectables<Reference>.fromData(
              all: locs,
              selected: settingsCubit.state.account.locRefs,
            );

            final selectableButtons = selLocs.items
                .map((location) =>
                    LocationOption(location: location, locations: selLocs))
                .toList();

            return Wrap(children: selectableButtons);
          },
        )),
      ],
    );
  }
}

class LocationOption extends StatelessWidget {
  final Selectable<Reference> location;
  final Selectables<Reference> locations;

  const LocationOption({
    Key key,
    this.location,
    this.locations,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();
    return Padding(
      padding: const EdgeInsets.only(top: 20, right: 15),
      child: Material(
        elevation: 3,
        borderRadius: BorderRadius.circular(15),
        clipBehavior: Clip.hardEdge,
        child: AnimatedContainer(
          color: _getColor(context),
          duration: Duration(milliseconds: 100),
          child: InkWell(
            onTap: () => _onTap(settingsCubit),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Text(location.data.name, style: _nameStyle),
            ),
          ),
        ),
      ),
    );
  }

  TextStyle get _nameStyle => TextStyle(
        color: location.selected ? Colors.white : Colors.black,
        fontWeight: FontWeight.w600,
        fontSize: 16,
      );

  void _onTap(SettingsCubit settingsCubit) {
    return settingsCubit.setFollowed(locations.toggle(location.data).selected);
  }

  Color _getColor(BuildContext context) =>
      location.selected ? MyColors.selectedButton : MyColors.notSelectedButton;
}
