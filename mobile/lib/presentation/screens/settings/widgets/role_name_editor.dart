import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/settings_cubit/settings_cubit.dart';
import 'package:recovery_app/domain/models/data/role/role.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/core/selectables/selectables.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/settings_styles.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'package:recovery_app/presentation/widgets/outer_padding.dart';

class RoleNameEditor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _title,
        SizedBox(height: 20),
        _mainContent,
      ],
    );
  }

  Widget get _mainContent => OuterPadding(
        child: Column(
          children: <Widget>[
            NameInput(),
            SizedBox(height: 20),
            _roleChoice,
          ],
        ),
      );

  Widget get _title => CapsuleTitle(
        position: CapsulePosition.left,
        child: Text(
          'Profile',
          style: CapsuleTitle.sectionTextStyle,
        ),
      );

  Widget get _roleChoice => BlocBuilder<CoreCubit, CoreState>(
        builder: (context, coreState) {
          return BlocBuilder<SettingsCubit, SettingsState>(
              builder: (context, state) {
            final settingsCubit = context.watch<SettingsCubit>();

            final selRoles = Selectables<Role>.fromData(
              all: coreState.organization.roles,
              selected: [state.account.role],
            );

            return Row(
              children: [
                Text('Role:', style: kSettingsTextFieldTitle),
                SizedBox(width: 20),
                Flexible(
                    child: Wrap(
                        spacing: 15,
                        runSpacing: 15,
                        children: selRoles.items
                            .map((role) =>
                                _roleButton(role, settingsCubit.roleChanged))
                            .toList()))
              ],
            );
          });
        },
      );

  Widget _roleButton(Selectable<Role> role, Function(Role role) onTap) {
    // final iconData = role.data == 'Nurse'
    //     ? FontAwesomeIcons.userNurse
    //     : FontAwesomeIcons.userMd;

    final Color _containerColor =
        role.selected ? MyColors.selectedButton : MyColors.notSelectedButton;
    final Color _textColor = role.selected
        ? MyColors.selectedButtonText
        : MyColors.notSelectedButtonText;

    return SizedBox(
      width: 90,
      // height: 90,
      child: Material(
        elevation: 1,
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(15),
        color: _containerColor,
        child: InkWell(
          onTap: () => onTap(role.data),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                // Icon(iconData, size: 40, color: Colors.white),
                Text(
                  role.data.ref.name,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: _textColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class NameInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();
    return Row(
      children: <Widget>[
        Text('Name:', style: kSettingsTextFieldTitle),
        Flexible(
          child: BlocBuilder<CoreCubit, CoreState>(
            builder: (context, state) => OuterPadding(
              child: TextFormField(
                onChanged: settingsCubit.nameChanged,
                initialValue: state.account.ref.name,
                decoration: InputDecoration(hintText: "Name"),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
