import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/settings_cubit/settings_cubit.dart';
import 'package:recovery_app/presentation/widgets/capsule_title.dart';
import 'named_switch.dart';

class SettingsSwitches extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CapsuleTitle(
          position: CapsulePosition.left,
          child: Text('Other Settings', style: CapsuleTitle.sectionTextStyle),
        ),
        _buildNotificationSwitch(context),
        _buildDisplayAlwaysOnSwitch(context),
        _buildVoiceAnnouncementsSwitch(context),
      ],
    );
  }

  Widget _buildNotificationSwitch(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();
    return BlocBuilder<SettingsCubit, SettingsState>(
      builder: (context, state) {
        return NamedSwitch(
          text: 'Notifications',
          onToggle: () =>
              settingsCubit.toggleField(SettingsSwitch.notificationsOn),
          switchValue: state.account.notificationsOn,
        );
      },
    );
  }

  Widget _buildVoiceAnnouncementsSwitch(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();

    return BlocBuilder<SettingsCubit, SettingsState>(
      builder: (context, state) {
        return NamedSwitch(
          text: 'Voice Announcements',
          onToggle: () =>
              settingsCubit.toggleField(SettingsSwitch.voiceAnnouncementsOn),
          switchValue: state.account.areVoiceAnnouncementsOn,
        );
      },
    );
  }

  Widget _buildDisplayAlwaysOnSwitch(BuildContext context) {
    final settingsCubit = context.watch<SettingsCubit>();

    return BlocBuilder<SettingsCubit, SettingsState>(
      builder: (context, state) {
        return NamedSwitch(
          text: 'Keep device awake',
          onToggle: () =>
              settingsCubit.toggleField(SettingsSwitch.displayAlwaysOn),
          switchValue: state.account.isDisplayAlwaysOn,
        );
      },
    );
  }
}
