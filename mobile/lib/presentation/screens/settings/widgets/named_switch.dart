import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/settings/widgets/settings_styles.dart';

class NamedSwitch extends StatelessWidget {
  final String text;
  final Function onToggle;
  final bool switchValue;

  const NamedSwitch({
    Key key,
    this.text,
    this.onToggle,
    this.switchValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: MyTheme.sideMargin),
      child: Container(
        constraints: BoxConstraints(maxWidth: 400),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(text, style: kSettingsTextFieldTitle),
            Switch.adaptive(
              value: switchValue,
              activeColor: MyColors.selectedButton,
              onChanged: (_) => onToggle(),
            ),
          ],
        ),
      ),
    );
  }
}
