import 'package:flutter/material.dart';

final TextStyle kSettingsTextFieldTitle = TextStyle(
  fontWeight: FontWeight.w600,
  fontSize: 16,
);
