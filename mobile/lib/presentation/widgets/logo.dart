import 'dart:math';

import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

class Logo extends StatelessWidget {
  const Logo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      MyAssets.landscapeLogo,
      width: min(MediaQuery.of(context).size.width * .8, 400),
    );
  }
}
