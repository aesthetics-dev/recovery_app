import 'package:flutter/material.dart';

class CoolCapsule extends StatelessWidget {
  final Widget child;
  final Color bgColor;
  final Color shadowColor;
  final double elevation;
  final EdgeInsets padding;
  final EdgeInsets innerPadding;

  const CoolCapsule({
    Key key,
    @required this.child,
    this.bgColor = Colors.white,
    this.shadowColor = Colors.black,
    this.padding = const EdgeInsets.only(top: 5, right: 5),
    this.innerPadding = const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
    this.elevation = 2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Material(
        elevation: elevation,
        color: bgColor,
        shadowColor: shadowColor,
        borderRadius: BorderRadius.circular(25),
        child: Padding(
          padding: innerPadding,
          child: child,
        ),
      ),
    );
  }
}
