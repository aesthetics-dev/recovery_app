import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

class CapsuleTitle extends StatelessWidget {
  final Widget child;
  final Function() onLongTap;
  final Function() onShortTap;
  final Color color;
  final CapsulePosition position;

  const CapsuleTitle({
    Key key,
    this.child,
    this.onLongTap,
    this.onShortTap,
    this.color,
    this.position = CapsulePosition.center,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: _alignment,
      child: Material(
        borderRadius: _borderRadius,
        clipBehavior: Clip.hardEdge,
        color: _color(context),
        child: InkWell(
          splashColor: _splashColor,
          highlightColor: _splashColor,
          onTap: onShortTap,
          onLongPress: onLongTap,
          child: Padding(
            padding: _padding,
            child: child,
          ),
        ),
      ),
    );
  }

  Color get _splashColor => onLongTap != null ? null : Colors.transparent;

  Color _color(BuildContext context) => color ?? MyColors.primary;

  EdgeInsets get _padding {
    return EdgeInsets.symmetric(
      horizontal: MyTheme.sideMargin,
      vertical: 10,
    );
  }

  Alignment get _alignment {
    final Map<CapsulePosition, Alignment> map = {
      CapsulePosition.left: Alignment.centerLeft,
      CapsulePosition.center: Alignment.center,
      CapsulePosition.right: Alignment.centerRight,
    };
    return map[position];
  }

  BorderRadius get _borderRadius {
    const curve = 20.0;
    double left = curve;
    double right = curve;

    if (position == CapsulePosition.left) {
      left = 0;
    } else if (position == CapsulePosition.right) {
      right = 0;
    }
    Radius toRadius(double radius) => Radius.circular(radius);
    return BorderRadius.only(
      bottomLeft: toRadius(left),
      topLeft: toRadius(left),
      bottomRight: toRadius(right),
      topRight: toRadius(right),
    );
  }

  static TextStyle get sectionTextStyle => TextStyle(
        color: Colors.white,
        fontSize: 18,
        fontWeight: FontWeight.w600,
      );

  static TextStyle get subsectionTextStyle => TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontWeight: FontWeight.w600,
      );
}

enum CapsulePosition { left, right, center }
