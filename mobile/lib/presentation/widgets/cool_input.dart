import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:recovery_app/domain/models/util/enums.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';

class CoolInput extends HookWidget {
  final InputParams inputParams;
  final Icon icon;
  final Side iconSide;
  final TextInputType inputType;

  const CoolInput({
    Key key,
    this.inputParams,
    this.icon,
    this.inputType,
    this.iconSide = Side.left,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _controller = useTextEditingController(
      text: inputParams.initialValue,
    );

    if (inputParams.value != null && inputParams.value != _controller.text) {
      _controller.text = inputParams.value;
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(child: _buildInputField(context, _controller)),
        // if (onSubmit != null) _buildSubmitButton(context)
      ],
    );
  }

  Widget _buildInputField(
      BuildContext context, TextEditingController controller) {
    List<Widget> content = [
      if (icon != null) ...[icon, SizedBox(width: 10)],
      Expanded(
        child: TextField(
          obscureText: inputParams.obscureText,
          controller: controller,
          onChanged: inputParams.onChange,
          decoration: InputDecoration(
            hintText: inputParams.placeholder,
            border: InputBorder.none,
          ),
          keyboardType: inputType,
          onSubmitted: (_) => _submit(context, controller.text),
        ),
      ),
    ];

    if (iconSide == Side.right) content = content.reversed.toList();

    return Material(
      elevation: 3,
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Row(
          children: content,
        ),
      ),
    );
  }

  void _submit(BuildContext context, String value) {
    if (!inputParams.canSubmit) {
      return;
    }

    FocusScope.of(context).unfocus();
    inputParams?.onSubmit();

    if (inputParams.clearOnSubmit) {
      inputParams.onChange('');
    }
  }
}
