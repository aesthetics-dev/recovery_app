import 'package:division/division.dart';
import 'package:flutter/material.dart';

typedef VoidFunc = void Function();

class ElevatedContainer extends StatelessWidget {
  final Widget child;
  const ElevatedContainer({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: kElevatedCointainerStyle,
      child: child,
    );
  }
}

final ParentStyle kElevatedCointainerStyle = ParentStyle()
  ..elevation(2)
  ..background.color(Colors.white)
  ..borderRadius(all: 25)
  ..padding(vertical: 5, horizontal: 15);
