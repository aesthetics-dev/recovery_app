import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/presentation/widgets/standalone_scaffold.dart';

import 'loading_overlay.dart';

class FeedbackListener extends StatelessWidget {
  final Widget child;

  const FeedbackListener({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StandaloneScaffold(
      child: Stack(
        children: [
          child ?? Container(),
          BlocBuilder<FeedbackCubit, FeedbackState>(
            buildWhen: (pState, nState) => pState.loading != nState.loading,
            builder: (context, state) {
              FocusScope.of(context).unfocus();
              return LoadingOverlay(shown: state.loading);
            },
          ),
        ],
      ),
    );
  }
}
