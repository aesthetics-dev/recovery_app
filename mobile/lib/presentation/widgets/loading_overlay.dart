import 'package:flutter/material.dart';

import 'loading_indicator.dart';

class LoadingOverlay extends StatelessWidget {
  final bool shown;

  const LoadingOverlay({
    Key key,
    @required this.shown,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !shown,
      child: AnimatedOpacity(
        opacity: shown ? 1 : 0,
        duration: Duration(milliseconds: 400),
        child: Container(
          color: Colors.white.withOpacity(.3),
          child: LoadingIndicator(),
        ),
      ),
    );
  }
}
