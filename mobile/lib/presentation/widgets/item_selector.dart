import 'package:flutter/material.dart';

import 'package:recovery_app/presentation/core/selectables/selectables.dart';

class ItemSelector<T> extends StatelessWidget {
  final Widget Function(Selectable<T> item) widgetBuilder;
  final List<T> all;
  final List<T> selected;
  final int Function(T i1, T i2) comparator;
  final WrapAlignment alignment;

  const ItemSelector({
    Key key,
    @required this.widgetBuilder,
    @required this.all,
    @required this.selected,
    this.comparator,
    this.alignment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (comparator != null) {
      all.sort(comparator);
    }

    final selectables = Selectables<T>.fromData(all: all, selected: selected);
    return Wrap(
      alignment: alignment,
      children: selectables.items.map(widgetBuilder).toList(),
    );
  }
}
