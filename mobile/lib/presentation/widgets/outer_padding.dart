import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

class OuterPadding extends StatelessWidget {
  final Widget child;

  const OuterPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: MyTheme.sideMargin),
      child: child,
    );
  }
}
