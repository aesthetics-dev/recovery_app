import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:recovery_app/domain/models/util/input_prams/input_params.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/widgets/cool_input.dart';

class CoolButtonInput extends HookWidget {
  final InputParams inputParams;
  final Icon icon;

  const CoolButtonInput({
    Key key,
    this.inputParams,
    this.icon = const Icon(FontAwesomeIcons.check, color: Colors.white),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _controller = useAnimationController(
      duration: Duration(
        milliseconds: 300,
      ),
    );

    final animation = useState(CurvedAnimation(
      curve: Curves.easeIn,
      parent: _controller,
    ));

    if (inputParams.canSubmit) {
      _controller.forward();
    } else {
      _controller.reverse();
    }

    return Row(
      children: [
        Expanded(
          child: CoolInput(
            inputParams: inputParams.copyWith(onChange: inputParams.onChange),
          ),
        ),
        SizedBox(width: 10),
        _buildSubmitButton(animation.value),
      ],
    );
  }

  Widget _buildSubmitButton(Animation<double> animation) {
    final duration = Duration(milliseconds: 300);

    return AnimatedOpacity(
      duration: duration,
      opacity: inputParams.canSubmit ? 1 : .7,
      child: ScaleTransition(
        scale: Tween<double>(begin: .95, end: 1).animate(animation),
        child: Material(
          color: MyColors.primary,
          elevation: 3,
          borderRadius: BorderRadius.circular(500),
          child: IconButton(
            icon: icon,
            onPressed: _onSubmit,
          ),
        ),
      ),
    );
  }

  void _onSubmit() {
    if (inputParams.onSubmit == null || !inputParams.canSubmit) {
      return;
    }

    inputParams.onSubmit();
    if (inputParams.clearOnSubmit) inputParams.onChange('');
  }
}
