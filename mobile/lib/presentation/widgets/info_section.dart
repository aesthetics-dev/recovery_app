import 'package:flutter/material.dart';

import 'cool_icon.dart';

class InfoSection extends StatelessWidget {
  final String text;
  final String subtext;

  final IconData icon;

  final bool topBold;
  final bool bottomBold;

  final String separator;

  const InfoSection({
    Key key,
    this.icon,
    this.text = '',
    this.subtext = '',
    this.separator = ' ',
    this.topBold = false,
    this.bottomBold = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(color: Colors.white, fontSize: 20);

    return SizedBox(
      width: 300,
      child: Column(
        children: [
          if (icon != null) CoolIcon(icon, size: 128, color: Colors.white),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: text + separator,
              style: textStyle,
              children: [
                TextSpan(
                  text: subtext,
                  style: textStyle.copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
