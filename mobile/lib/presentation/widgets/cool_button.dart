import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';

typedef VoidFunc = void Function();

class CoolButton extends StatelessWidget {
  final String label;
  final VoidFunc onPressed;
  final bool active;
  final bool isTransparent;
  final Color containerColor;
  final Color textColor;
  final Icon icon;

  const CoolButton({
    Key key,
    @required this.onPressed,
    this.label,
    this.active = true,
    this.isTransparent = false,
    this.containerColor,
    this.textColor,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: active ? 1 : .8,
      duration: Duration(milliseconds: 200),
      child: Material(
        borderRadius: BorderRadius.circular(50),
        clipBehavior: Clip.hardEdge,
        elevation: isTransparent ? 0 : 4,
        color: bgColor,
        child: InkWell(
          onTap: pressAction,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: IntrinsicWidth(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildLabel(),
                  if (icon != null) ...[
                    SizedBox(width: 10),
                    icon,
                  ]
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Text _buildLabel() {
    return Text(
      label,
      style: TextStyle(
          color: tColor,
          fontWeight: FontWeight.w600,
          fontSize: 18,
          fontFeatures: const [FontFeature.tabularFigures()]),
    );
  }

  void Function() get pressAction => active ? onPressed : () {};

  Color get tColor =>
      textColor ?? (isTransparent ? Colors.black : Colors.white);

  Color get bgColor =>
      containerColor ?? (isTransparent ? Colors.transparent : MyColors.primary);
}
