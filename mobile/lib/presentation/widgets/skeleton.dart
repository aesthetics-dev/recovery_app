import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class Skeleton extends StatelessWidget {
  final double height;
  final double width;

  const Skeleton({Key key, this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonAnimation(
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(color: Colors.grey[300]),
      ),
    );
  }
}
