import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recovery_app/domain/cubits/feedback_cubit/feedback_cubit.dart';
import 'package:recovery_app/domain/utilities/nav_routes.dart';
import 'package:recovery_app/domain/cubits/core_cubit/core_cubit.dart';
import 'package:recovery_app/domain/controllers/dependency_injector.dart' as di;
import 'package:provider/provider.dart';
import 'package:recovery_app/presentation/core/my_theme.dart';
import 'package:recovery_app/presentation/screens/loading_screen/loading_screen.dart';
import 'package:recovery_app/presentation/widgets/feedback_listener.dart';

import 'domain/cubits/auth_cubit/auth_cubit.dart';

void main() {
  runApp(App());
  WidgetsFlutterBinding.ensureInitialized();
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  bool ready = false;
  final navKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    _init();
  }

  ThemeData get themeData => ThemeData(
        pageTransitionsTheme: MyTheme.pageTransition,
      );

  @override
  Widget build(BuildContext context) {
    if (!ready) {
      return MaterialApp(
        home: LoadingScreen(),
      );
    }
    return BlocProvider(
      create: (context) => FeedbackCubit(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthCubit>(create: _buildSignInCubit),
          BlocProvider<CoreCubit>(create: _buildCoreCubit, lazy: false),
        ],
        child: GestureDetector(
          onTap: () {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus.unfocus();
            }
          },
          child: FeedbackListener(
            child: MaterialApp(
              theme: themeData,
              builder: BotToastInit(),
              navigatorObservers: [BotToastNavigatorObserver()],
              initialRoute: NavRoutes.loading,
              onGenerateRoute: NavRoutes.onGenerateRoute,
              navigatorKey: navKey,
            ),
          ),
        ),
      ),
    );
  }

  CoreCubit _buildCoreCubit(BuildContext context) => CoreCubit(
      navKey: navKey,
      userService: di.getInstance(),
      organizationService: di.getInstance(),
      localStorage: di.getInstance(),
      notificationService: di.getInstance(),
      feedback: context.read());

  AuthCubit _buildSignInCubit(BuildContext context) => AuthCubit(
        userService: di.getInstance(),
        organizationService: di.getInstance(),
        dynamicLinks: di.getInstance(),
        feedback: context.read(),
      );

  Future<void> _init() async {
    await di.init();
    setState(() => ready = true);
  }
}
