const Organization = ({ ref, utcDelta = -4, locations = [] }) => ({
  ref, utcDelta, locations,
});

module.exports = Organization;
