const Reference = ({ name, id }) => ({
  name, id,
});

const ShortRef = (name) => Reference({ name, id: `${name} | ${Math.random()}` });

module.exports = { Reference, ShortRef };
