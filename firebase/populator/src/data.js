const Organization = require('./models/organization');
const Location = require('./models/location');
const Bed = require('./models/bed');
const User = require('./models/user');
const { ShortRef, Reference } = require('./models/reference');

const generateBeds = (from, to) => Array.from(Array(to - from).keys()).map((n) => Bed({
  ref: Reference({
    name: `${(n + from)}`,
    id: `${(n + from)} | ${Math.random()}`,
  }),
}));

const tags = [
  'Stomach Ache',
  'Bleeding',
  'Head Ache',
  'Low BP',
  'High BP',
  'Bruises',
  'ECG',
  'Nausea',
  'Itching',
  'Drowsiness',
  'Trouble Breathing',
];

const mdTags = [
  'Resident Aware',
  'MD Aware',
];

const org1 = Organization({
  ref: ShortRef('Brigham and Women\'s Hospital'),
  utcDelta: -4,
  locations: [
    Location({
      ref: ShortRef('PACU'),
      beds: generateBeds(101, 120),
      tags,
      mdTags,
    }),
    Location({
      ref: ShortRef('DSU'),
      beds: generateBeds(1, 40),
      tags,
      mdTags,
    }),
    Location({
      ref: ShortRef('L&D'),
      beds: generateBeds(1, 68),
      tags,
      mdTags,
    }),
    Location({
      ref: ShortRef('OR'),
      beds: generateBeds(100, 150),
      tags,
      mdTags,
    }),
    Location({
      ref: ShortRef('Triage'),
      beds: generateBeds(1, 43),
      tags,
      mdTags,
    }),
  ],
});

const org2 = Organization({
  ref: ShortRef('New England Baptist Hospital'),
  utcDelta: -4,
  locations: [],
});

const user1 = User({
  ref: Reference({ name: 'Volodymyr Bobyr', id: 'vkbobyr@outlook.com' }),
  orgRef: org1.ref,
  notificationsOn: false,
  role: 'Nurse',
  followedLocations: [],
});

const user2 = User({
  ref: Reference({ name: 'David Preiss', id: 'dpreiss@bwh.harvard.edu' }),
  orgRef: org1.ref,
  notificationsOn: false,
  role: 'Physician',
  followedLocations: [],
});

const user3 = User({
  ref: Reference({ name: 'David Preiss', id: 'dpreiss@gmail.com' }),
  orgRef: org1.ref,
  notificationsOn: false,
  role: 'Nurse',
  followedLocations: [],
});

const user4 = User({
  ref: Reference({ name: 'Jimin Kim', id: 'jiminkim@gmail.com' }),
  orgRef: org1.ref,
  notificationsOn: false,
  role: 'Nurse',
  followedLocations: [org1.locations[0].ref],
});

const orgRefs = [org1.ref, org2.ref];
module.exports = {
  org1, org2, user1, user2, user3, user4, orgRefs,
};
