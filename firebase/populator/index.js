const admin = require('firebase-admin');
const creds = require('../functions/src/service-account.json');
const {
  org1, org2, user1, user2, user3, orgRefs, user4,
} = require('./src/data');

admin.initializeApp({
  credential: admin.credential.cert(creds),
});

const db = admin.firestore();

// console.log(org1);
const populate = async () => {
  const orgs = await db.collection('Organizations').get();
  const removes = orgs.docs.map((doc) => doc.ref.delete());
  await Promise.all(removes);

  await db.doc(`Organizations/${org1.ref.id}`).set(org1);
  await db.doc(`Organizations/${org2.ref.id}`).set(org2);
  await Promise.all([user1, user2, user3, user4].map((user) => db.doc(`Organizations/${user.orgRef.id}/Users/${user.ref.id}`).set(user)));
  await db.doc('PublicData/SignInData').set({ orgRefs });
};

populate();
