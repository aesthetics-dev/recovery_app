// import { generateRef } from "./data_utils";
// import { Footprint, Account, Reference, Task } from "../../src/models";

// export class TestData {
//   org1Ref: Reference = generateRef('org1');

//   nurse: User = {
//     ref: generateRef('nurse'),
//     orgRef: this.org1Ref,
//     role: 'Nurse'
//   };

//   fpNurse: Footprint = {
//     ref: this.nurse.ref,
//     timestamp: 't1',
//   };

//   invalidTaskObj = {
//     orgRef: generateRef('org'),
//     bedRef: generateRef('bed'),
//     locRef: generateRef('loc'),
//     tags: ['tag1', 'tag2'],
//     mdTags: ['tag1', 'tag2'],
//     customMDTag: 't',
//     customTag: 't',
//   };

//   validTaskObj = {
//     ...this.invalidTaskObj,
//     created: this.fpNurse
//   };

//   validTask: Task = {
//     orgRef: generateRef('org'),
//     bedRef: generateRef('bed'),
//     locRef: generateRef('loc'),
//     tags: ['tag1', 'tag2'],
//     mdTags: ['tag1', 'tag2'],
//     customMDTag: 't',
//     customTag: 't',
//     created: this.fpNurse,
//     mdETA: undefined,
//     removed: undefined,
//   };
// }