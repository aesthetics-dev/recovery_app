import { Reference } from "../../src/models";

export const generateRef = (prefix = ''): Reference => ({
  id: `${prefix} - ${Math.random().toPrecision(5)} - id`,
  name: `${prefix} - ${Math.random().toPrecision(5)}`
});