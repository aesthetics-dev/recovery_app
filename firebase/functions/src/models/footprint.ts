import * as t from 'io-ts';
import { Reference } from './reference';

export const Footprint = t.exact(t.type({
  ref: Reference,
  timestamp: t.string,
}));

export type Footprint = t.TypeOf<typeof Footprint>;