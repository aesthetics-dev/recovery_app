import * as t from 'io-ts';
import { Reference } from './reference';
import { Footprint } from './footprint';
const optional = (val: t.Mixed): t.Mixed => t.union([val, t.undefined, t.null]);

export const Task = t.exact(t.type({
  bedRef: Reference,
  locRef: Reference,
  orgRef: Reference,
  tags: optional(t.array(t.string)),
  mdTags: optional(t.array(t.string)),
  customTag: optional(t.string),
  customMDTag: optional(t.string),
  mdETA: optional(Footprint),
  created: optional(Footprint),
  removed: optional(Footprint),
}));


export type Task = t.TypeOf<typeof Task>;
