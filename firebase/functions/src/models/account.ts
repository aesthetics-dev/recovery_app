import * as t from 'io-ts';
import { Reference } from './reference';

const RequiredAccount = t.interface({
  ref: Reference,
  orgRef: Reference,
  role: t.string,
});

const OptionalAccount = t.partial({
  notificationsOn: t.boolean,
  followedLocations: t.array(Reference),
});

export const Account = t.intersection([RequiredAccount, OptionalAccount]);

export type Account = t.TypeOf<typeof Account>;
