import * as t from "io-ts/lib/index";
import { isLeft } from "fp-ts/lib/Either";

export function cast<T>(obj: any, decoder: t.Any): T | null {
  const decoded = decoder.decode(obj);

  if (isLeft(decoded)) {
    return null;
  }

  return decoded.right;
}