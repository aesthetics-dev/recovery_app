import * as t from 'io-ts';

export const Reference = t.exact(t.type({
  id: t.string,
  name: t.string,
}));

export type Reference = t.TypeOf<typeof Reference>;
