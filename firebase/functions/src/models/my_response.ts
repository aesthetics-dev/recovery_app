import * as t from 'io-ts';

const MyResponseRequired = t.interface({
  success: t.boolean,
});

const MyResponseOptional = t.partial({
  message: t.string,
  data: t.any,
});

const MyResponse = t.intersection([MyResponseRequired, MyResponseOptional]);

export type MyResponse = t.TypeOf<typeof MyResponse>;
