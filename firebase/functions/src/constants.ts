export const Colls = {
  organizations: 'Organizations',
  users: "Users",
  tasks: "Tasks",
  taskArchive: "TaskArchive",
  organizationData: "OrganizationData",
  userData: "UserData",
}

export const Docs = {
  account: 'Account',
  organization: 'Organization',
}