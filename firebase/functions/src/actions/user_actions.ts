import { Colls } from "../constants";
import { CallableContext } from "firebase-functions/lib/providers/https";
import admin = require("firebase-admin");
import { UserRecord } from "firebase-functions/lib/providers/auth";
import { Reference, Account as Account, Footprint } from "../models";
import { isLeft } from "fp-ts/lib/Either";
import moment = require("moment");


type UserActionsSignature = {
  db: FirebaseFirestore.Firestore;
  auth: admin.auth.Auth;
};

export class UserActions {
  db: FirebaseFirestore.Firestore;
  auth: admin.auth.Auth;

  getTaskCol = (orgID: String) => this.db.collection(`${Colls.organizations}/${orgID}/${Colls.tasks}`);
  getArchCol = (orgID: String) => this.db.collection(`${Colls.organizations}/${orgID}/${Colls.taskArchive}`);

  constructor({ db, auth }: UserActionsSignature) {
    this.db = db;
    this.auth = auth;
  }

  getUserRecord = async (context: CallableContext): Promise<UserRecord | null> => {
    const uid = context.auth?.uid;
    if (!uid) {
      return null;
    }
    return this.auth.getUser(uid);
  };

  castAccount(obj: object | undefined | null) {
    const userCast = Account.decode(obj);
    if (isLeft(userCast)) {
      return null;
    }
    return userCast.right;
  }

  findAccountByEmail = async (orgRef: Reference, email: string): Promise<Account | null> => {
    const accQuery = await this.db.doc(`${Colls.organizations}/${orgRef.id}/${Colls.users}/${email}`).get();
    return this.castAccount(accQuery.data());
  };

  findAccountByContext = async (orgRef: Reference, context: CallableContext): Promise<Account | null> => {
    const userRecord = await this.getUserRecord(context);
    if (!userRecord || !userRecord.email) {
      return null;
    }

    return this.findAccountByEmail(orgRef, userRecord.email);
  };

  createFootprint = (account: Account): Footprint => {
    return {
      ref: account.ref,
      timestamp: moment().utc().format(),
    };
  };

}
