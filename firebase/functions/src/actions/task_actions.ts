import { Task, Reference } from "../models";
import { Colls } from "../constants";
import { cast } from "../models/model_utils";


type GetTask = {
  orgRef: Reference;
  bedRef: Reference;
};

export class TaskActions {
  db: FirebaseFirestore.Firestore;

  getTaskCol = (orgID: String) => this.db.collection(`${Colls.organizations}/${orgID}/${Colls.tasks}`);
  getArchCol = (orgID: String) => this.db.collection(`${Colls.organizations}/${orgID}/${Colls.taskArchive}`);

  constructor(db: FirebaseFirestore.Firestore) {
    this.db = db;
  }

  setTask = async (task: Task) => {
    return this.getTaskCol(task.orgRef.id).doc(task.bedRef.id).set(task);
  };

  removeTask = async ({ orgRef, bedRef }: GetTask) => {
    return this.getTaskCol(orgRef.id).doc(bedRef.id).delete();
  };

  addTaskToArchive = async (task: Task) => {
    return this.getArchCol(task.orgRef.id).add(task);
  };

  getTask = async ({ orgRef, bedRef }: GetTask): Promise<Task | null> => {
    const query = await this.getTaskCol(orgRef.id).doc(bedRef.id).get();
    return cast<Task>(query.data(), Task);
  };

  isEmpty(task: Task) {
    return !task.customTag && (!task.tags || !task.tags.length);
  }
}
