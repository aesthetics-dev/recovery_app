
export class Testy {
  classy: SomeClass;

  constructor(classy: SomeClass) {
    this.classy = classy;
  }
}

export class SomeClass {
  returnClass(): SomeClass {
    return new SomeClass();
  }

  hello(): number {
    return 10;
  }
}