import { Left } from "fp-ts/lib/Either";
import { Errors } from "io-ts";

export const concatErr = (val: Left<Errors>) => val.left.map(d => d.value).concat(' | ');

