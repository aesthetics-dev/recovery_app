import * as functions from 'firebase-functions';
import { MyResponse, Task } from '../models';
import { TaskActions, UserActions } from '../actions';
import { cast } from '../models/model_utils';

type TaskControllerSignature = {
  taskActions: TaskActions;
  userActions: UserActions;
};

export class TaskController {
  taskActions: TaskActions;
  userActions: UserActions;

  constructor({ taskActions, userActions }: TaskControllerSignature) {
    this.taskActions = taskActions;
    this.userActions = userActions;
  }

  addTask = functions.https.onCall(async (data, context): Promise<MyResponse> => {
    const task = cast<Task>(data, Task);
    if (!task) {
      return {
        success: false,
        message: 'invalid task format',
      };
    }

    const account = await this.userActions.findAccountByContext(task.orgRef, context);
    if (!account) {
      return {
        success: false,
        message: 'Could not verify user'
      };
    }

    const footprint = this.userActions.createFootprint(account);
    const activeTask = await this.taskActions.getTask(task);

    const promises: Array<Promise<any>> = [];
    if (activeTask) {
      activeTask.removed = footprint;
      promises.push(this.taskActions.addTaskToArchive(activeTask));
    }

    if (this.taskActions.isEmpty(task)) {
      promises.push(this.taskActions.removeTask(task));
    } else {
      task.created = footprint;
      promises.push(this.taskActions.setTask(task));
    }

    await Promise.all(promises);
    return { success: true };
  });
}