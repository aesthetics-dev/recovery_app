import { UserActions } from '../actions';
import { cast } from '../models/model_utils';
import { Reference, MyResponse } from '../models';
import * as functions from 'firebase-functions';

type UserControllerSignature = {
  userActions: UserActions;
};

export class UserController {
  userActions: UserActions;

  constructor({ userActions }: UserControllerSignature) {
    this.userActions = userActions;
  }

  userExists = functions.https.onCall(async (data, context): Promise<MyResponse> => {
    const orgRef = cast<Reference>(data?.orgRef, Reference);
    const email = data?.email;
    if (!orgRef || !email) {
      return {
        success: false,
        message: 'Invalid organization reference or email format.',
      };
    }
    const user = await this.userActions.findAccountByEmail(orgRef, email);

    return {
      success: true,
      data: user !== null,
    };
  });
}