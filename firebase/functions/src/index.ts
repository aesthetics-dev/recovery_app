import * as admin from 'firebase-admin';
import { TaskController } from './controllers/task_controller';
import { TaskActions, UserActions } from './actions';
import { UserController } from './controllers/user_controller';

admin.initializeApp();
const db = admin.firestore();
const auth = admin.auth();

const taskActions = new TaskActions(db);
const userActions = new UserActions({ db, auth });

const taskController = new TaskController({ taskActions, userActions });
const userController = new UserController({ userActions });

export const addTask = taskController.addTask;
export const userExists = userController.userExists;